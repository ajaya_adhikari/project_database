CREATE DOMAIN ShortString as VARCHAR(128);

-- Stores website content per language, allowing lookup by unique identifier
-- string.
-- If you wish to add a new language column, please name it according to the
-- ISO 639-1 standard.
CREATE TABLE Contents (
    id ShortString PRIMARY KEY,
    description TEXT,
    en TEXT NOT NULL,
    nl TEXT
);


-- A user of the system.
CREATE TABLE Users (
    id SERIAL PRIMARY KEY,
    name ShortString UNIQUE NOT NULL,
    password ShortString NOT NULL,
    time TIMESTAMP DEFAULT 'now',
    credits INT DEFAULT 0
);
    CREATE INDEX User_name_index ON Users(name);
    CREATE INDEX User_time_index ON Users(time);

-- A series of exercises.
CREATE TABLE ExerciseSeries (
    id SERIAL PRIMARY KEY,
    name ShortString NOT NULL,
    author_id INTEGER NOT NULL REFERENCES Users(id) ON DELETE CASCADE,
    description ShortString,
    difficulty INTEGER,

    programming_language ShortString,
    language CHAR(2),
    visible BOOLEAN,

    times_made INTEGER, -- Number of users who made this series.
    avg_rating FLOAT,
    rating_count INTEGER, -- Number of users who rated this series.

    time TIMESTAMP DEFAULT 'now',
    credits INT DEFAULT 0,

    UNIQUE(name, author_id)
);
    CREATE INDEX ExerciseSeries_name_index ON ExerciseSeries(name);
    CREATE INDEX ExerciseSeries_author_index ON ExerciseSeries(author_id);

-- A single exercise.
CREATE TABLE Exercises (
    id SERIAL PRIMARY KEY,
    author_id INTEGER REFERENCES Users(id),
    title Text,
    contents Text,
    time TIMESTAMP DEFAULT 'now'
);
CREATE INDEX Exercises_time_index ON Exercises(time);

-- Relationship between ExerciseSeries and Exercises.
CREATE TABLE ExercisesInSeries (
    series_id INTEGER REFERENCES ExerciseSeries,
    exercise_id INTEGER REFERENCES Exercises,
    num INTEGER NOT NULL,
    PRIMARY KEY(series_id, exercise_id, num)
);

CREATE TABLE Tags (
    series_id INTEGER REFERENCES ExerciseSeries(id) ON DELETE CASCADE,
    tag ShortString,
    PRIMARY KEY(series_id, tag)
);

-- User's solutions to exercises.
CREATE TABLE Solutions (
    exercise_id INTEGER REFERENCES Exercises(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES Users(id) ON DELETE CASCADE,
    solution Text,
    correct BOOLEAN,
    attempt_count INTEGER,
    time TIMESTAMP DEFAULT 'now',
    PRIMARY KEY(exercise_id, user_id)
);
CREATE INDEX Solutions_time_index ON Solutions(time);

CREATE TABLE Ratings (
    series_id INTEGER REFERENCES ExerciseSeries(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES Users(id) ON DELETE CASCADE,
    score FLOAT NOT NULL,
    time TIMESTAMP DEFAULT 'now',
    PRIMARY KEY (series_id, user_id)
);


CREATE INDEX Ratings_time_index ON Ratings(time);
-- The many-to-many relation between users and exercise series, where users
-- are treated as the consumers of the exercises.
--CREATE TABLE UsersAsConsumersAndSeries (
    --user_id INTEGER REFERENCES Users(id),
    --series_id INTEGER REFERENCES ExerciseSeries(id),
    --has_made BOOLEAN DEFAULT FALSE,
    --rating INTEGER DEFAULT NULL,
    --PRIMARY KEY (user_id, series_id)
--);



-- Pair of subsriber and subsribee
-- id1 is the person who sends the request
-- id2 is the person to which person1 subscribes to
-- (old version: Pairs of users who are friends.)
CREATE TABLE Friends (
    id1 INTEGER REFERENCES Users(id) ON DELETE CASCADE,
    id2 INTEGER REFERENCES Users(id) ON DELETE CASCADE,
    time TIMESTAMP DEFAULT 'now',
    PRIMARY KEY (id1, id2)
    --CONSTRAINT id1_smaller_than_id2 CHECK (id1 < id2) --not used any more beacause friends are now subscribers
);
CREATE INDEX Friends_time_index ON Friends(time);
CREATE INDEX Friends_id1_index ON Friends(id1);

-- A many-to-many relationship containing the friendrequests
--CREATE TABLE FriendRequests (
--    sender INTEGER REFERENCES Users(id) ON DELETE CASCADE,
--    receiver INTEGER REFERENCES Users(id) ON DELETE CASCADE,
--    time TIMESTAMP DEFAULT 'now',
--    PRIMARY KEY (sender, receiver)
--);

--    CREATE INDEX FriendRequests_receiver_index on FriendRequests(receiver);

-- A group of users. Not sure if we're going to use this.
CREATE TABLE Groups (
    id SERIAL PRIMARY KEY,
    name ShortString UNIQUE NOT NULL,
    time TIMESTAMP DEFAULT 'now'
);
    CREATE INDEX Group_name_index ON Groups(name);
    CREATE INDEX Group_time_index ON Groups(time);


-- The many-to-many relationship associating users with the groups they
-- have joined.
CREATE TABLE GroupMembers (
    group_id INTEGER REFERENCES Groups(id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES Users(id) ON DELETE CASCADE,
    time TIMESTAMP DEFAULT 'now',
    PRIMARY KEY (group_id, user_id)
);
    CREATE INDEX GroupMembers_time_index ON GroupMembers(time);

-- This table contains the similarity between two series.
CREATE TABLE SeriesNeighbours (
    series_id INTEGER REFERENCES ExerciseSeries(id) ON DELETE CASCADE,
    neighbours_id INTEGER REFERENCES ExerciseSeries(id) ON DELETE CASCADE,
    similarity FLOAT NOT NULL,
    PRIMARY KEY (series_id, neighbours_id)
);

    CREATE INDEX SeriesNeighbours_series_id_index ON SeriesNeighbours(series_id);
