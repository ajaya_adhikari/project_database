import os
import sys
import inspect
abs_path = os.path.dirname(
            os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0,abs_path + '/../src/database/')
import database

db = database.Database('pdb','panjandrum','panjandrum')

ut = db.users_table()
et = db.exercises_table()
est = db.exercise_series_table()
eist = db.exercises_in_series_table()
rt = db.ratings_table()
tt = db.tag_table()
ft = db.friends_table()
frt = db.friendrequests_table()
sn = db.series_neighbours_table()

#Insert user
author = ut.insert('author','password')

user1 = ut.insert('user1','password')
user2 = ut.insert('user2','password')
user3 = ut.insert('user3','password')
user4 = ut.insert('user4','password')
user5 = ut.insert('user5','password')
user6 = ut.insert('user6','password')
user7 = ut.insert('user7','password')
user8 = ut.insert('user8','password')
user9 = ut.insert('user9','password')
user10 = ut.insert('user10','password')
user11 = ut.insert('user11','password')
user12 = ut.insert('user12','password')
user13 = ut.insert('user13','password')
user14 = ut.insert('user14','password')
user15 = ut.insert('user15','password')
user16 = ut.insert('user16','password')
user17 = ut.insert('user17','password')
user18 = ut.insert('user18','password')
user19 = ut.insert('user19','password')
user20 = ut.insert('user20','password')


# Insert exercise series.
serie1 = est.insert('serie1', author, 'discription',  1, 'python', 'en', True)
serie2 = est.insert('serie2', author, 'discription',  1, 'python', 'en', True)
serie3 = est.insert('serie3', author, 'discription',  1, 'python', 'en', True)
serie4 = est.insert('serie4', author, 'discription',  1, 'python', 'en', True)
serie5 = est.insert('serie5', author, 'discription',  1, 'python', 'en', True)
serie6 = est.insert('serie6', author, 'discription',  1, 'python', 'en', True)
serie7 = est.insert('serie7', author, 'discription',  1, 'python', 'en', True)
serie8 = est.insert('serie8', author, 'discription',  1, 'python', 'en', True)
serie9 = est.insert('serie9', author, 'discription',  1, 'python', 'en', True)
serie10 = est.insert('serie10', author, 'discription',  1, 'python', 'en', True)
serie11 = est.insert('serie11', author, 'discription',  1, 'python', 'en', True)
serie12 = est.insert('serie12', author, 'discription',  1, 'python', 'en', True)
serie13 = est.insert('serie13', author, 'discription',  1, 'python', 'en', True)
serie14 = est.insert('serie14', author, 'discription',  1, 'python', 'en', True)
serie15 = est.insert('serie15', author, 'discription',  1, 'python', 'en', True)
serie16 = est.insert('serie16', author, 'discription',  1, 'python', 'en', True)
serie17 = est.insert('serie17', author, 'discription',  1, 'python', 'en', True)
serie18 = est.insert('serie18', author, 'discription',  1, 'python', 'en', True)
serie19 = est.insert('serie19', author, 'discription',  1, 'python', 'en', True)
serie20 = est.insert('serie20', author, 'discription',  1, 'python', 'en', True)

#rating serie1
rt.insert(serie1,user1,4)
rt.insert(serie1,user2,3)
rt.insert(serie1,user4,3)
rt.insert(serie1,user5,2)
rt.insert(serie1,user6,1)


#rating serie2
rt.insert(serie2,user1,4)
rt.insert(serie2,user2,5)
rt.insert(serie2,user4,3)
rt.insert(serie2,user5,4)
rt.insert(serie2,user6,2)

rt.insert(serie2,user7,4)
rt.insert(serie2,user8,3)
rt.insert(serie2,user9,1)
rt.insert(serie2,user10,2)


#rating serie3
rt.insert(serie3,user1,4)
rt.insert(serie3,user4,4)
rt.insert(serie3,user5,3)
rt.insert(serie3,user6,2)



#rating serie8
rt.insert(serie8,user7,4)
rt.insert(serie8,user8,4)
rt.insert(serie8,user9,4)
rt.insert(serie8,user10,2)
