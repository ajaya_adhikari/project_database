import os
import sys
import inspect
abs_path = os.path.dirname(
            os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0,abs_path + '/../../src/database')


import users_data
import friends_data
import exerciseSeries_data
import exercises_data
import solutions_rating_data

