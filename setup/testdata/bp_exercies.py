contents = """[large_header]Definition[/large_header]
    In Python, a function is a named sequence of statements that belong together. Their primary purpose is to help us organize programs into chunks that match how we think about the problem.

    [large_header]Syntax[/large_header]
    The syntax for a function definition is:

    [code_block]def NAME( PARAMETERS ):
        STATEMENTS[/code_block]
    We can make up any names we want for the functions we create, except that we can’t use a name that is a Python keyword, and the names must follow the rules for legal identifiers.

    There can be any number of statements inside the function, but they have to be indented from the def. In the examples in this book, we will use the standard indentation of four spaces. Function definitions are the second of several compound statements we will see, all of which have the same pattern:

    A header line which begins with a keyword and ends with a colon.
    A body consisting of one or more Python statements, each indented the same amount — the Python style guide recommends 4 spaces — from the header line.

    [large_header]Example[/large_header]
    So looking again at the function definition, the keyword in the header is def, which is followed by the name of the function and some parameters enclosed in parentheses.
    The parameter list may be empty, or it may contain any number of parameters separated from one another by commas. In either case, the parentheses are required.
    The parameters specifies what information, if any, we have to provide in order to use the new function.

    [small_header]Now that you know all of this, adapt the function below such that it print's 'Hello World'[/small_header]
    [user_input]
    def HelloWorld( ): #takes no parameters
        ___
    [regex]def HelloWorld( ): #takes no parameters
        print("Hello World")[/regex]
    [/user_input]
    """
    ex = et.insert(bp, 'Functions Introduction', contents)
    eist.insert_exercise_in_series(functions_series.id, ex.id)


    contents = """Functions that require arguments
Most functions require arguments: the arguments provide for generalization. For example, if we want to find the absolute value of a number, we have to indicate what the number is. Python has a built-in function for computing the absolute value:

[code_block]>>> abs(5)
5
>>> abs(-5)
5[/code_block]
In this example, the arguments to the abs function are 5 and -5.

Some functions take more than one argument. For example the built-in function pow takes two arguments, the base and the exponent. Inside the function, the values that are passed get assigned to variables called parameters.

[code_block]>>> pow(2, 3)
8
>>> pow(7, 4)
2401[/code_block]
Another built-in function that takes more than one argument is max.

[code_block]>>> max(7, 11)
11
>>> max(4, 1, 17, 2, 12)
17
>>> max(3 * 11, 5**3, 512 - 9, 1024**0)
503[/code_block]
max can be passed any number of arguments, separated by commas, and will return the largest value passed. The arguments can be either simple values or expressions. In the last example, 503 is returned, since it is larger than 33, 125, and 1.

[small_header]Exercise[/small_header]
Adapt the function below so that it takes 2 parameters called 'a' and 'b' and prints the sum (of a and b).

[user_input]
def add(___):
    print(___)
[evaluation_code]
def add(a,b):
    print(a+b
[/evaluation_code]
[/user_input]
        """
    ex = et.insert(bp, 'Functions with arguments', contents)
    eist.insert_exercise_in_series(functions_series.id, ex.id)


    contents = """All the functions in the previous section return values. Furthermore, functions like range, int, abs all return values that can be used to build more complex expressions.
So an important difference between these functions and one like
[code_block]
def add(___):
    print(___)
[/code_block]

[highlight]If you dont return values you can't use what you computed in your function![/highlight]


A function that returns a value is called a fruitful function in this book. The opposite of a fruitful function is void function — one that is not executed for its resulting value, but is executed because it does something useful. (Languages like Java, C#, C and C++ use the term “void function”, other languages like Pascal call it a procedure.) Even though void functions are not executed for their resulting value, Python always wants to return something. So if the programmer doesn’t arrange to return a value, Python will automatically return the value None.

[user_input]
[regex]Hello World[/regex]
[/user_input]
    """
    ex = et.insert(bp, 'Function with return values', contents)
    eist.insert_exercise_in_series(functions_series.id, ex.id)

