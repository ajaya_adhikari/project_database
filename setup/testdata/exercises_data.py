import database
# NOTE: Adding or modifying messages requires a server restart.
# -*- coding: utf-8 -*-
db = database.Database('pdb','panjandrum','panjandrum')
et = db.exercises_table()
est = db.exercise_series_table()
eist = db.exercises_in_series_table()
ut = db.users_table()


# Insert Exercises.
try:
    panjandrum = ut.get_by_name('panjandrum')
    arek = ut.get_by_name('arek')
    ajaya = ut.get_by_name('ajaya')
    koen = ut.get_by_name('koen')
    sdemeyer = ut.get_by_name('sdemeyer')
    bp = ut.get_by_name('bp')


    if_series = est.get_by_author_and_name(ajaya, 'If-statements')
    loops_series = est.get_by_author_and_name(panjandrum, 'Programming with Loops')
    functions_series = est.get_by_author_and_name(bp, 'Functions')
    recursion_series = est.get_by_author_and_name(koen, 'recursion')
    classes_series = est.get_by_author_and_name(koen, 'Classes')



########################################## If-statements ##########################################

    contents = """Run this example program, suitcase.py. Try it at least twice, with inputs: 30 and then 55. As you an see, you get an extra result, depending on the input. The main code is:
    [code_block]
        weight = float(input("How many pounds does your suitcase weigh? "))
        if weight > 50:
            print("There is a $25 charge for luggage that heavy.")
        print("Thank you for your business.")
    [/code_block]
    The middle two line are an if statement. It reads pretty much like English. If it is true that the weight is greater than 50, then print the statement about an extra charge. If it is not true that the weight is greater than 50, then don't do the indented part: skip printing the extra luggage charge. In any event, when you have finished with the if statement (whether it actually does anything or not), go on to the next statement that is not indented under the if. In this case that is the statement printing "Thank you".
    The general Python syntax for a simple if statement is
    [code_block]
        if condition :
        indentedStatementBlock
    [/code_block]
    If the condition is true, then do the indented statements. If the condition is not true, then skip the indented statements.

    Another fragment as an example:
    [code_block]
    if balance < 0:
        transfer = -balance
        # transfer enough from the backup account:
        backupAccount = backupAccount - transfer
        balance = balance + transfer
    [/code_block]
    <p>As with other kinds of statements with a heading and an indented block, the block can have more than one statement. The assumption in the example above is that if an account goes negative, it is brought back to 0 by transferring money from a backup account in several steps.
    In the examples above the choice is between doing something (if the condition is True) or nothing (if the condition is False). Often there is a choice of two possibilities, only one of which will be done, depending on the truth of a condition.</p>
    <p>Write an if statement that prints "Hello world" if 2<5.</p>
    [user_input]
    print("Hello world")
    [regex]
    Hello world
    [/regex]
    [/user_input]"""
    ex = et.insert(ajaya, 'Simple If Statement', contents)
    eist.insert_exercise_in_series(if_series.id, ex.id)

    contents = """Run the example program, clothes.py. Try it at least twice, with inputs 50 and then 80. As you can see, you get different results, depending on the input. The main code of clothes.py is:
    [code_block]
        temperature = float(input("What is the temperature? "))
        if temperature > 70:
            print("Wear shorts.")
        else:
            print("Wear long pants.")
        print("Get some exercise outside.")
    [/code_block]
    The middle four lines are an if-else statement. Again it is close to English, though you might say "otherwise" instead of "else" (but else is shorter!). There are two indented blocks: One, like in the simple if statement, comes right after the if heading and is executed when the condition in the if heading is true. In the if-else form this is followed by an else: line, followed by another indented block that is only executed when the original condition is false. In an if-else statement exactly one of two possible indented blocks is executed.

    A line is also shown outdented next, about getting exercise. Since it is outdented, it is not a part of the if-else statement: It is always executed in the normal forward flow of statements, after the if-else statement (whichever block is selected).

    The general Python if-else syntax is
    [code_block]
        if condition :
        indentedStatementBlockForTrueCondition
        else:
        indentedStatementBlockForFalseCondition
    [/code_block]
    These statement blocks can have any number of statements, and can include about any kind of statement.
    <p>Write a if-else statement. If 2<1 print "Lol 2<1" else print "Hello world".
    [user_input]
    if 2<1:
        print("Lol 2<1")
    else:
        #complete the code
    [regex]
    Hello world
    [/regex]
    [/user_input]"""
    ex = et.insert(ajaya, 'if-else statement', contents)
    eist.insert_exercise_in_series(if_series.id, ex.id)

    contents = """The statements introduced in this chapter will involve tests or conditions.
    More syntax for conditions will be introduced later, but for now consider simple arithmetic comparisons that directly translate from math into Python.
    Try each line separately in the Shell
    [code_block]
    2 < 5
    3 > 7
    x = 11
    x > 10
    2 * x < x
    type(True)
    [/code_block]
    You see that conditions are either True or False (with no quotes!).
    These are the only possible Boolean values (named after 19th century mathematician George Boole). In Python the name Boolean is shortened to the type bool.
    It is the type of the results of true-false conditions or tests.
    <p>Write try print(2<5) </p>
    [user_input]
    #You can do this
    [regex]
    True
    [/regex]
    [/user_input]"""
    ex_simple_conditions = et.insert(ajaya, 'Simple Conditions', contents)
    eist.insert_exercise_in_series(if_series.id, ex_simple_conditions.id, 1)


############################################ Programming with Loops ############################################

    contents = """There are two types of loops in Python, for and while.
[small_header]The "for" loop[/small_header]
For loops [highlight]iterate[/highlight] over a given sequence. Here is an example:
[code_block]
primes = [2, 3, 5, 7]
for prime in primes:
    print prime
[/code_block]
For loops can iterate over a sequence of numbers using the "range" and "xrange" functions. The difference between range and xrange is that the range function returns a new list with numbers of that specified range, whereas xrange returns an iterator, which is more efficient. (Python 3 uses the range function, which acts like xrange). Note that the xrange function is zero based.
[code_block]
# Prints out the numbers 0,1,2,3,4
for x in xrange(5): # or range(5)
    print x

# Prints out 3,4,5
for x in xrange(3, 6): # or range(3, 6)
    print x

# Prints out 3,5,7
for x in xrange(3, 8, 2): # or range(3, 8, 2)
    print x

[/code_block]

Write a for-loop that prints 1 till 10.
[user_input]
# now it's your turn
[regex]
1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n
[/regex]
[/user_input]"""
    ex = et.insert(ajaya, 'For-loops', contents)
    eist.insert_exercise_in_series(loops_series.id, ex.id, 1)

    contents = """While loops repeat as long as a certain boolean condition is met. For example:
[code_block]
# Prints out 0,1,2,3,4

count = 0
while count < 5:
    print count
    count += 1  # This is the same as count = count + 1
[/code_block]
[large_header]"break" and "continue" statements[/large_header]

break is used to exit a for loop or a while loop, whereas continue is used to skip the current block, and return to the "for" or "while" statement. A few examples:
[code_block]
# Prints out 0,1,2,3,4
count = 0
while True:
    print count
    count += 1
    if count >= 5:
        break
# Prints out only odd numbers - 1,3,5,7,9
for x in xrange(10):
    # Check if x is even
    if x % 2 == 0:
        continue
    print x
[/code_block]

Write a while loop that prints even numbers from 1 till 10.
[user_input]
counter = 1
while counter <= 10:
    #complete the body of the loop
[regex]
2\n4\n6\n8\n10\n
[/regex]
[/user_input]"""

    ex = et.insert(ajaya, 'While loops', contents)
    eist.insert_exercise_in_series(loops_series.id, ex.id)

################################################## Functions ##################################################

    contents = """We can create a function that writes the Fibonacci series to an arbitrary boundary:
[code_block]
def fib(n):    # write Fibonacci series up to n
    \"\"\"Print a Fibonacci series up to n.\"\"\"
    a, b = 0, 1
    while a < n:
        print a,
        a, b = b, a+b
# Now call the function we just defined:
fib(2000)
[/code_block]
Output:
[code_block]
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597
[/code_block]
<p>The keyword def introduces a function definition. It must be followed by the function name and the parenthesized list of formal parameters.
The statements that form the body of the function start at the next line, and must be indented.</p>
<p>Write a funtion add that takes 2 integers as argument and prints their sum.</p>
[user_input]
#Write your function here



#Lets see if your funtion works
add(5,5)
[regex]
10
[/regex]
[/user_input]"""

    ex = et.insert(bp, 'Defining functions', contents)
    eist.insert_exercise_in_series(functions_series.id, ex.id)

    contents = """The most useful form is to specify a default value for one or more arguments. This creates a function that can be called with fewer arguments than it is defined to allow. For example:
[code_block]
def ask_ok(prompt, retries=4, complaint='Yes or no, please!'):
    while True:
        ok = raw_input(prompt)
        if ok in ('y', 'ye', 'yes'):
            return True
        if ok in ('n', 'no', 'nop', 'nope'):
            return False
        retries = retries - 1
        if retries < 0:
            raise IOError('refusenik user')
        print complaint
[/code_block]
This function can be called in several ways:

    <p>giving only the mandatory argument: ask_ok('Do you really want to quit?')</p>
    <p>giving one of the optional arguments: ask_ok('OK to overwrite the file?', 2)</p>
    <p>or even giving all arguments: ask_ok('OK to overwrite the file?', 2, 'Come on, only yes or no!')</p>

    <p> Write a function sum which takes 3 integers as argument and prints the result. The second and the third arguments have a default
        value of 0.</p>
[user_input]
#Write your function here



#Lets see if your funtion works
add(5)
add(5,5)
add(5,5,5)
[regex]
5\n10\n15\n
[/regex]
[/user_input]"""

    ex = et.insert(bp, 'Default values', contents)
    eist.insert_exercise_in_series(functions_series.id, ex.id)

    contents = """Python supports an interesting syntax that lets you define one-line mini-functions on the fly.
Borrowed from Lisp, these so-called lambda functions can be used anywhere a function is required.
Introducing lambda Functions:
[code_block]
def f(x):
     return x*2

f(3)                #output 6

#First option using lambda functions
g = lambda x: x*2
g(3)                #output 6

#Second option using lambda functions
(lambda x: x*2)(3)  #output 6
[/code_block]

<p>1 	This is a lambda function that accomplishes the same thing as the normal function above it. Note the abbreviated syntax here: there are no parentheses around the argument list, and the return keyword is missing (it is implied, since the entire function can only be one expression). Also, the function has no name, but it can be called through the variable it is assigned to.</p>
<p>2 	You can use a lambda function without even assigning it to a variable. This may not be the most useful thing in the world, but it just goes to show that a lambda is just an in-line function.</p>

<p> Write a lambda funtion inc that increments a integer with 14.</p>
[user_input]
#Write your function here
inc =

#Lets see if your funtion works
print(inc(5))
[regex]
19
[/regex]
[/user_input]"""
    ex = et.insert(bp, 'Lambda functions', contents)
    eist.insert_exercise_in_series(functions_series.id, ex.id)
    eist.insert_exercise_in_series(functions_series.id, ex_simple_conditions.id,1)


################################################## Recursion ##################################################
    contents = """Write a function 'fac' that returns the factorial of n.

[user_input]
def fac(n):
    # write your code here
[evaluation_code]
def fac(n):
    if n == 0:
        return 1
    else:
        return n * fac(n-1)
        
def evaluate():
    success = True
    for i in [0, 1, 5, 20]:
        expected = fac(i)
        result = solution.fac(i)
        
        print('fac({})'.format(i))
        print('\tExpected:', expected)
        print('\tResult:', result)
        if expected == result:
            print('\tOK!')
        else:
            print('\tWRONG!')
            success = False
    return success
[/evaluation_code]
[/user_input]
"""
    ex = et.insert(koen, 'Factorial', contents)
    eist.insert_exercise_in_series(recursion_series.id, ex.id)


################################################## Classes ##################################################

    contents = """
Write a class named 'Queue', with methods 'append' and 'pop'.

append should accept one argument, append the value of the argument to the queue and return nothing.

pop should accept no arguments, delete the element that was inserted first from the queue, and return it. It should return None if there are no elements left in the queue.


[user_input]
# Write your solution here.
[evaluation_code]
import types

def evaluate():
    if 'Queue' not in dir(solution) or type(solution.Queue) != type:
        print("Your solution should contain a class named 'Queue'.")
        return False
    try:
        q = solution.Queue()
    except:
        print("You should correctly implement the Queue.__init__ method.")
        return False
        
    if 'append' not in dir(solution.Queue) or type(q.append) != types.MethodType:
        print("The 'Queue' class should implement an 'append' method")
        return False
        
    if 'pop' not in dir(solution.Queue) or type(q.pop) != types.MethodType:
        print("The 'Queue' class should implement a 'pop' method")
        return False
    
    testqueue = [5,2,7,9,1,5,3,6,6,9]
    expected_queue_contents = []
    def a():
        nonlocal testqueue
        x, testqueue = testqueue[0], testqueue[1:]
        print("Appending", x)
        q.append(x)
        expected_queue_contents.append(x)
        return True
    
    def p():
        nonlocal expected_queue_contents
        if len(expected_queue_contents) == 0:
            y = None
        else:
            y, expected_queue_contents = expected_queue_contents[0], expected_queue_contents[1:]
        print("Popping an element, expecting", y, end='. ')
        
        x = q.pop()
        print("Result:", x, end='. ')
        
        if x != y:
            print("This is wrong.")
            return False
        else:
            print("Correct!")
            return True
    
    actions = [a,a,p,a,p,p,p,p,a,a,a,a,a,a,a,p,p,p,p,p,p,p,p]
    
    try:
        for act in actions:
            if not act():
                return False
            print("Your queue should now contain", expected_queue_contents)
    except Exception as e:
        print("\\nYour code threw an exception...", e)
        return False
    
    return True
    
[/evaluation_code]
[/user_input]
"""

    ex = et.insert(koen, 'Queue class', contents)
    eist.insert_exercise_in_series(classes_series.id, ex.id)

    print "\tExercises added."

except database.psycopg2.Error as e:
    print(e)


