import database
import random

db = database.Database('pdb','panjandrum','panjandrum')
rt = db.ratings_table()
st = db.solutions_table()
est = db.exercise_series_table()
eist = db.exercises_in_series_table()
ut = db.users_table()

#insert2 userSolutions
try:
    #all users
    panjandrum = ut.get_by_name('panjandrum')
    arek = ut.get_by_name('arek')
    ajaya = ut.get_by_name('ajaya')
    koen = ut.get_by_name('koen')
    sdemeyer = ut.get_by_name('sdemeyer')
    bp = ut.get_by_name('bp')


    #all series
    if_series = est.get_by_author_and_name(ajaya, 'If-statements')
    loops_series = est.get_by_author_and_name(panjandrum, 'Programming with Loops')
    lussen = est.get_by_author_and_name(panjandrum, 'lussen')
    proper_init = est.get_by_author_and_name(sdemeyer, 'Proper Initialization')
    learn_c = est.get_by_author_and_name(ajaya, 'Learning C++ met Ajay')
    software_eng = est.get_by_author_and_name(sdemeyer, 'Software Engineering Best Practices')
    recursion = est.get_by_author_and_name(koen, 'recursion')
    functions_series = est.get_by_author_and_name(bp, 'Functions')
    stringss = est.get_by_author_and_name(bp, 'Strings')

    all_series = [if_series, loops_series, lussen, proper_init, learn_c, software_eng, recursion, functions_series, stringss]

    #some exercises per series
    if1 = eist.get_exercise_by_series_id_and_num(if_series, 1)
    if2 = eist.get_exercise_by_series_id_and_num(if_series, 2)
    if3 = eist.get_exercise_by_series_id_and_num(if_series, 3)

    loops1 = eist.get_exercise_by_series_id_and_num(loops_series, 1)
    loops2 = eist.get_exercise_by_series_id_and_num(loops_series, 2)


    function1 = eist.get_exercise_by_series_id_and_num(functions_series, 1)
    function2 = eist.get_exercise_by_series_id_and_num(functions_series, 2)
    function3 = eist.get_exercise_by_series_id_and_num(functions_series, 3)
    function4 = eist.get_exercise_by_series_id_and_num(functions_series, 4)

##################################### user: Ajaya solution: function_series #####################################
    st.insert_empty(function2, ajaya)
    st.register_attempt(function2, ajaya, """#Write your function here
def add(x,y):
    print x+y
#Lets see if your funtion works
add(5,5)""" , True)

    st.insert_empty(function3, ajaya)
    st.register_attempt(function3, ajaya, """#Write your function here
def add(x, y=0 ,z=0 ):
    print x+y+z

#Lets see if your funtion works
add(5)
add(5,5)
add(5,5,5)""" , True)

    st.insert_empty(function4, ajaya)
    st.register_attempt(function4, ajaya, """#Write your function here
inc = lambda x: x+14

#Lets see if your funtion works
print(inc(5))""" , True)

    st.insert_empty(function1, ajaya)
    st.register_attempt(function1, ajaya, """#You can do this
print(2<5)""" , True)


    print "\tUserSolutions added."
except database.psycopg2.Error as e:
    print(e)


#insert2 ratings
try:
    #ratings if_series
    rt.insert2(if_series,ajaya, 1)
    rt.insert2(if_series,arek,1)
    rt.insert2(if_series,koen,1)

    #ratings loops_series
    rt.insert2(loops_series,ajaya,5)
    rt.insert2(loops_series,koen,5)
    rt.insert2(loops_series,bp,5)

    #ratings lussen
    rt.insert2(lussen,koen,5)
    rt.insert2(lussen,sdemeyer,5)
    rt.insert2(lussen,panjandrum,5)

    #ratings proper_init
    rt.insert2(proper_init,sdemeyer,5)
    rt.insert2(proper_init,arek,3)
    rt.insert2(proper_init,bp,4)

    #ratings learn_c++
    rt.insert2(learn_c,sdemeyer,1)
    rt.insert2(learn_c,panjandrum,2)

    #ratings software_eng
    rt.insert2(software_eng, ajaya, 5)
    rt.insert2(software_eng,arek,2)


    #ratings recursion
    rt.insert2(recursion, sdemeyer,3)
    rt.insert2(recursion,bp,3)

    #ratings functions_series
    rt.insert2(functions_series,arek,5)
    rt.insert(functions_series,ajaya,5)
    rt.insert(functions_series,bp,5)

    #ratings strings
    rt.insert2(stringss,arek,1)
    rt.insert2(stringss,panjandrum,2)
    #random ratings
    for i in range(15):
        sett = set([random.randrange(0,9) for j in range(4)])
        for index in sett:
            rt.insert2(all_series[index], ut.get_by_name("user"+str(i)), random.randrange(1,6))



    print "\tUserRatings added."
except database.psycopg2.Error as e:
    print(e)
