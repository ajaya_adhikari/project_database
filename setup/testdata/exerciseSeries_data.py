import database
import datetime
import random

db = database.Database('pdb','panjandrum','panjandrum')

est = db.exercise_series_table()
ut = db.users_table()
tt = db.tag_table()

# Insert exercise series.
try:
    panjandrum = ut.get_by_name('panjandrum')
    arek = ut.get_by_name('arek')
    ajaya = ut.get_by_name('ajaya')
    koen = ut.get_by_name('koen')
    sdemeyer = ut.get_by_name('sdemeyer')
    bp = ut.get_by_name('bp')

    est.insert('Programming with Loops', panjandrum, 'all about loops',
               1, 'python', 'en', True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('If-statements', ajaya, 'Learn about if', 1,
               'python', 'en', True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('lussen', panjandrum, 'alles over lussen', 3,
               'python', 'nl', True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('Proper Initialization', sdemeyer,
                'How to properly initialize your C++ objects.', 3, 'cpp', 'en',
                 True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('Learning C++ met Ajay', ajaya, 'Introduction tot C++', 2, 'cpp',
                'en', True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('Software Engineering Best Practices', sdemeyer,
                'More on how to properly initialize your C++ objects.', 3,
                 'cpp', 'en', True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('recursion', koen, 'all about recursion', 1, 'python', 'en',
                True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('Functions', bp, 'Everything you want to know about functions',
                1, 'python', 'en', True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('Strings', bp,
                'Everything you want to know about python strings', 1, 'python',
                'en', True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(20,40))))

    est.insert('Classes', koen, 
                'An introduction to classes and object-oriented programming', 1, 'python',
                'en', True, str(datetime.datetime.now()))

    print "\tExerciseSeries added."

except database.psycopg2.Error as e:
    print(e)



# Insert tags.
try:
    if_series = est.get_by_author_and_name(ajaya, 'If-statements')
    loops_series = est.get_by_author_and_name(panjandrum, 'Programming with Loops')
    functions_series = est.get_by_author_and_name(bp, 'Functions')
    strings_series = est.get_by_author_and_name(bp, 'Strings')

    tt.insert(loops_series, 'iteration')
    tt.insert(loops_series, 'for')
    tt.insert(loops_series, 'while')
    tt.insert(loops_series, 'python')
    tt.insert(if_series, 'conditionals')
    tt.insert(if_series, 'if-else')
    tt.insert(if_series, 'python')
    tt.insert(functions_series, 'python')
    tt.insert(functions_series, 'functions')
    tt.insert(functions_series, 'arguments')
    tt.insert(functions_series, 'return-value')
    tt.insert(strings_series, 'python')
    tt.insert(strings_series, 'parts')
    tt.insert(strings_series, 'slices')
    tt.insert(strings_series, 'length')
    tt.insert(strings_series, 'stringcomparison')
    tt.insert(strings_series, 'immutable')
    tt.insert(strings_series, 'find-function')
    tt.insert(strings_series, 'split')

    print "\tTags added."

except database.psycopg2.Error as e:
    print(e)
