import database
import datetime
import random

db = database.Database('pdb','panjandrum','panjandrum')
ut = db.users_table()


# Insert users.
try:
    panjandrum = ut.insert('panjandrum','wazniak', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    arek = ut.insert('arek','arekt', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    admin = ut.insert('admin','admin', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    administrator = ut.insert('administrator','administrator', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    vreg = ut.insert('vreg','narusimu', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    ajaya = ut.insert('ajaya','ajaya', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    koen = ut.insert('koen','meloen', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    sdemeyer = ut.insert('sdemeyer','pascal4life', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))
    bp = ut.insert('bp', 'wachtwoord', str(datetime.datetime.now() - datetime.timedelta(random.randrange(40,60))))

    #add more random users
    name = "user"
    for i in range(15):
        ut.insert(name+str(i),name+str(i))

    print "\tUsers added."
except database.psycopg2.Error as e:
    print(e)
