# Starts the development server.
#
# This server automatically reloads templates when they are modified 
# and prints extra debugging information.
(cd src; python main.py) # launch from src dir, bottle.py doesn't obey sys.path when reloading is enabled
