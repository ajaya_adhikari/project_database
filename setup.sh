#!/bin/bash

# Colors
# Black        0;30     Dark Gray     1;30
# Blue         0;34     Light Blue    1;34
# Green        0;32     Light Green   1;32
# Cyan         0;36     Light Cyan    1;36
# Red          0;31     Light Red     1;31
# Purple       0;35     Light Purple  1;35
# Brown/Orange 0;33     Yellow        1;33
# Light Gray   0;37     White         1;37
black='\e[0;30m'
red='\e[0;31m'
green='\e[0;32m'
orange='\e[0;33m'
blue='\e[0;34m'
purple='\e[0;35m'
cyan='\e[0;36m'
lightgrey='\e[0;37m'
darkgrey='\e[1;30m'
lightred='\e[1;31m'
lightgreen='\e[1;32m'
yellow='\e[1;33m'
lightblue='\e[1;34m'
lightpurple='\e[1;35m'
lightcyan='\e[1;36m'
white='\e[1;37m'
default='\e[0m'

error=0

# Functions
##################################################################################################

# Fill line with character(s)
# fillRow "{CHAR}"
fillRow () {
    echo -en "${darkgrey}"
    i=0
    columns=$(tput cols)
    while [ $i -lt $columns ]; do
        echo -en "${1}"
        let i=$i+1
    done
    echo -e "${default}"
}

# Check if the item is in the list
# contains "{LIST}" "{ITEM}"
# contains "{LIST}" "{LIST}"
contains () {
    for arg in ${1}; do
        for item in ${2}; do
            if [ "$arg" == "$item" ]; then
                echo 1
                return
            fi
        done
    done
    echo 0
}

# Print succes message depending on exit code
# printStatus $? "Cleaning"
printStatus () {
    if [ $1 -eq 0 ]; then
        echo -en "$2: ${green}Successful"
    else
        echo -en "$2: ${red}Unsuccessful" >&2
    fi
    echo -e "${default}."
}

progress () {
    columns=$(tput cols)
    columns=$(($columns - 7))
    done=$(($1 * $columns / 100))
    remaining=$(($columns - $done))
    printf "%03d%% [" $1
    i=0
    while [ $i -lt $done ]; do
        echo -en "="
        let i=$i+1
    done
    i=0
    while [ $i -lt $remaining ]; do
        echo -en "."
        let i=$i+1
    done
    printf "]\r"
    if [ $1 -eq 100 ]; then
        printf "\n"
    fi
}

# Main
##################################################################################################

fillRow "="

# Check argument count
if [ $# -eq 0 ]; then
    echo -e "${red}"
    echo -e "Usage: ${0} [options]"
    echo -e ""
    echo -e "Options: -c, --clean -  Remove temporary files."
    echo -e "         -s, --sync  -  Sync apache and project files."
    echo -e "         --install   -  Install requirements and configure project."
    echo -e ""
    echo -e "Example: ${0} --clean --sync"
    echo -e "${default}"
fi

# Progresstest
if [ $(contains "$*" "-m") -eq 1 ]; then
    for i in {0..100}
    do
        sleep 0.01
        progress $i
    done
    printStatus $? "Test"
fi

# Clean
if [ $(contains "$*" "-c --clean") -eq 1 ]; then
    status=0
    find . -name "*~" -type f -delete
    status=$(($status + $?))
    find . -name "*.pyc" -type f -delete
    status=$(($status + $?))
    rm -r src/.session/*
    status=$(($status + $?))
    printStatus $status "Temporary file removal"
fi

# Sync website files to apache
if [ $(contains "$*" "-s --sync") -eq 1 ]; then
    status=0
    sudo -S rsync -r src/ /var/www/python/src
    status=$(($status + $?))
    sudo -S rsync -r lib/ /var/www/python/lib
    status=$(($status + $?))
    printStatus $status "Apache Sync"
fi

# Restart the apache service
if [ $(contains "$*" "-r --restart") -eq 1 ]; then
    sudo service apache2 restart
    printStatus $? "Apache restart"
fi

# Install the website
if [ $(contains "$*" "--install") -eq 1 ]; then
    status=0

    mkdir src/tmp

    # Install the required packages
    sudo apt-get -y install postgresql postgresql-contrib
    status=$(($status + $?))
    sudo apt-get -y install python python-dev python-psycopg2
    status=$(($status + $?))
    sudo apt-get -y install apache2 apache2-dev libapache2-mod-wsgi
    status=$(($status + $?))

    # Copy the apache site settings
    sudo mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.backup
    status=$(($status + $?))
    sudo cp setup/000-default.conf /etc/apache2/sites-available/000-default.conf
    status=$(($status + $?))

    # Prepare directory structure for copying
    sudo mkdir /var/www/python
    status=$(($status + $?))

    # Copy files from project to apache website location
    sudo -S rsync -r src/ /var/www/python/src
    status=$(($status + $?))

    sudo -S rsync -r lib/ /var/www/python/lib
    status=$(($status + $?))

    # Postgres
    # Copy postgres password settings to allow trusted authentication
    sudo mv /etc/postgresql/9.3/main/pg_hba.conf /etc/postgresql/9.3/main/pg_hba.conf.backup
    status=$(($status + $?))
    sudo cp setup/pg_hba.conf /etc/postgresql/9.3/main/pg_hba.conf
    status=$(($status + $?))

    # Reload/restart postgres to load new settings
    sudo service postgresql reload
    status=$(($status + $?))
    sudo service postgresql start
    status=$(($status + $?))

    # Create a new user 'panjandrum' for the site
    sudo -u postgres createuser -d -s -r -h localhost -U postgres panjandrum
    status=$(($status + $?))

    # Create the required database as panjandrum
    sudo psql -U postgres -h localhost -c "CREATE DATABASE panjandrum;"
    status=$(($status + $?))

    # Create the website database
    sudo psql -U panjandrum -h localhost -c "CREATE DATABASE pdb;"
    status=$(($status + $?))

    # Initialize database structure
    cat setup/init.sql | sudo -u postgres psql -d pdb
    status=$(($status + $?))

    # Reload/restart postgres to load new settings
    sudo service postgresql reload
    status=$(($status + $?))
    sudo service postgresql restart
    status=$(($status + $?))

    # Add module to apache to prevent it from crying about localhost server
    echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/fqdn.conf
    status=$(($status + $?))
    sudo a2enconf fqdn
    status=$(($status + $?))

    # Prevent apache from complaining about session write permissions
    # Could change this to only include .session directory
    sudo chown -R www-data /var/www/python
    status=$(($status + $?))

    sudo service apache2 restart
    status=$(($status + $?))


    echo -e "${blue}Installation done.${default}"
    if [ $status -gt 0 ]; then
        echo -e "${red}$status errors. Please check manually!${default}"
    fi
fi

# Dump postgres database
if [ $(contains "$*" "--dumpdb") -eq 1 ]; then
    pg_dump pdb > setup/DB_DUMP
    printStatus $? "Postgres data dump"
fi

# Restore postgres database
if [ $(contains "$*" "--restoredb") -eq 1 ]; then
    psql pdb < setup/DB_DUMP
    printStatus $? "Postgres data restore"
fi

# Empty the apache website directory
if [ $(contains "$*" "--empty") -eq 1 ]; then
    sudo rm -r /var/www/python
    printStatus $? "Emptying of /var/www/python"
fi

# Purge postgres from the system
if [ $(contains "$*" "--remove-postgres") -eq 1 ]; then
    status=0
    # Make sure to stop postgres
    sudo service postgresql stop
    status=$(($status + $?))

    # Remove all postgres related packages
    sudo apt-get -y --purge remove postgres\*
    status=$(($status + $?))

    # Postgres is a meta package
    # Remove remaining files
    rm -r /etc/postgresql/
    status=$(($status + $?))
    rm -r /etc/postgresql-common/
    status=$(($status + $?))
    rm -r /var/lib/postgresql/
    status=$(($status + $?))

    # Remove postgres user and group
    sudo deluser postgres
    status=$(($status + $?))
    sudo delgroup postgres
    status=$(($status + $?))
    printStatus $status "Removing postgresql"
fi

# Purge postgres from the system
if [ $(contains "$*" "--reset-db") -eq 1 ]; then
    status=0
    # Close connections from apache
    sudo service apache2 stop
    status=$(($status + $?))
    # Drop the old database
    echo "Dropping old database.."
    sudo PGPASSWORD=panjandrum psql -U panjandrum -d template1 -h localhost -c "DROP DATABASE IF EXISTS pdb;"
    status=$(($status + $?))
    # Create the website database
    echo "Creating new database.."
    sudo PGPASSWORD=panjandrum psql -U panjandrum -d template1 -h localhost -c "CREATE DATABASE pdb;"
    status=$(($status + $?))
    # Initialise the database
    cat setup/init.sql | sudo -u postgres psql -d pdb
    status=$(($status + $?))
    sudo service apache2 start
    status=$(($status + $?))
    if [ $(contains "$*" "-i --import") -eq 1 ]; then
        echo "Inserting dummy data.."
        python setup/testdata/testdata.py
        status=$(($status + $?))
    fi
    if [ -d "./src/.session" ]; then
      # Remove sessions to prevent logged in errors
      rm -r ./src/.session
      status=$(($status + $?))
    fi
    printStatus $status "Resetting database"
fi

fillRow "="

exit 0
