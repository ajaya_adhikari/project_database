import os
import sys

srcdir = os.path.dirname(os.path.realpath(__file__))
libdir = os.path.join(srcdir, "../lib")
sys.path = [libdir] + sys.path
os.chdir(srcdir)

######################################################################

import bottle

from beaker.middleware import SessionMiddleware
from controllers       import *

recommendation.set_series_neighbours()

@bottle.route("/static/<filepath:path>")
def serve_static(filepath):
    return bottle.static_file(filepath, root="static")

@bottle.hook('before_request')
def setup_request():
    bottle.request.session = bottle.request.environ['beaker.session']
    if "preferences" not in bottle.request.session:
        bottle.request.session["preferences"] = {}

session_opts = {
    "session.type": "file",
    "session.cookie_expires": False,
    "session.data_dir": "./.session",
    "session.auto": True
}
app = SessionMiddleware(bottle.app(), session_opts)

if __name__ == "__main__":
    bottle.run(app=app, debug=True, reloader=True)
