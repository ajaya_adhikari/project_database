/********************************************************************/
/*                          MINIFIED.JS SETUP                       */
/********************************************************************/

var MINI = require('minified');
var _=MINI._, $=MINI.$, $$=MINI.$$, EE=MINI.EE, HTML=MINI.HTML;


/********************************************************************/
/*                         EDITOR CONFIGURATION                     */
/********************************************************************/

// Replaces tabs with spaces
function cm_tab_replacement(cm) {
  if (cm.somethingSelected()) {
    cm.indentSelection("add");
  } else {
    cm.replaceSelection(Array(cm.getOption("indentUnit") + 1).join(" "));
  }
}
// Unlike default, does not de-indent to start of line. Also allows de-indenting
// beyond a block's base indentation level.
function cm_shifttab_replacement(cm) {
  if (cm.somethingSelected()) {
    cm.indentSelection("subtract");
  } else {
    var pos = cm.getCursor();
    cm.setSelection({line: pos.line, ch: 0}, {line: pos.line, ch: cm.getOption("indentUnit")});
    var sel = cm.getSelection();
    var len = sel.length - sel.trimLeft().length;
    cm.replaceSelection(sel.trimLeft());
    cm.setCursor({line: pos.line, ch: pos.ch-len});
  }
}
function submit_user_input() {
  $$("#submit").click();
}
CodeMirror.defaults.mode            =  "python";
CodeMirror.defaults.extraKeys       = { "Tab":        cm_tab_replacement, 
                                        "Shift-Tab":  cm_shifttab_replacement,
                                        "Ctrl-Enter": submit_user_input, };
CodeMirror.defaults.theme           = "solarized dark";
CodeMirror.defaults.vimMode         = false;
CodeMirror.defaults.styleActiveLine = false;
CodeMirror.defaults.indentUnit      = 4;  // indent of 4 is baked into rust mode.. let's roll with it
CodeMirror.defaults.tabSize         = 4;
CodeMirror.defaults.lineNumbers     = false;
CodeMirror.defaults.dragDrop        = false;
CodeMirror.defaults.cursorBlinkRate = 1000;
CodeMirror.defaults.cursorHeight    = 0.85;
CodeMirror.defaults.matchBrackets   = true;


/********************************************************************/
/*                   HOME EXERCISE CODE EVALUATION                  */
/********************************************************************/

function evaluate_home_exercise() {
  var user_input   = $$("#txt-code-python").value;  // TODO: support different languages
  var language     = active_language
  var btn_submit   = $$("#submit");
  var div_result   = $("#result");

  function onsuccess(txt) {
    var data = $.parseJSON(txt);
    div_result.set("$display", "block");
    if( data.success ) {
        div_result.set("+userinput-correct -userinput-incorrect");
        setTimeout(function() { window.location.href = "/signup"; }, 1500);
    } else {
        div_result.set("-userinput-correct +userinput-incorrect");
    }
    if( data.output != "" ) {
        div_result.set("innerHTML", "<pre>" + data.output.replace(/\n/g, "<br/>") + "</pre>");
    } else {
        div_result.set("innerHTML", "<pre>no output or execution timed out.</pre>");
    }
  }

  function onfailure(status, statusText, responseText) {
    alert("Server could not be reached. Please try again later.");
  }

  btn_submit.disabled = true;
  $.request("post", "/home/eval", {
      user_input:   user_input,
      language:     language,
  }).then(onsuccess, onfailure).then(function() { 
      btn_submit.disabled = false; 
  });
}


/********************************************************************/
/*                CHALLENGE EXERCISE CODE EVALUATION                */
/********************************************************************/

function evaluate_challenge_exercise() {
  if(!$$("#user_input")) { console.log("no input window"); return; }

  var user_input   = $$("#user_input").value;
  var language     = $$("#language").value;
  var challenge_id = $$("#challenge_id").value;
  var exercise_id  = $$("#exercise_id").value;
  var btn_submit   = $$("#submit");
  var btn_continue = $$("#continue");
  var div_result   = $("#result");

  function onsuccess(txt) {
    var data = $.parseJSON(txt);
    div_result.set("$display", "block");
    if( data.success ) {
        div_result.set("+userinput-correct -userinput-incorrect");
    } else {
        div_result.set("-userinput-correct +userinput-incorrect");
    }
    if( data.output != "" ) {
        div_result.set("innerHTML", "<pre>" + data.output.replace(/\n/g, "<br/>") + "</pre>");
    } else {
        div_result.set("innerHTML", "<pre>no output or execution timed out.</pre>");
    }
    if( data.success || data.past_success ) {
        btn_continue.disabled = false;
    }
  }

  function onfailure(status, statusText, responseText) {
    alert("Server could not be reached. Please try again later.");
  }

  btn_submit.disabled = true;
  $.request("post", "/challenge/eval", {
      user_input:   user_input,
      language:     language,
      challenge_id: challenge_id,
      exercise_id:  exercise_id,
  }).then(onsuccess, onfailure).then(function() { 
      btn_submit.disabled = false; 
  });
}
