# NOTE: Adding or modifying messages requires a server restart.
msg = {}

# Header / Footer
msg["header.search"]        = "Search"
msg["header.browse"]        = "Browse"
msg["header.login"]         = "Log In"
msg["header.signup"]        = "Sign Up"
msg["header.loggedin"]      = "You are logged in. "
msg["header.logout"]        = "Log out"
msg["footer.language"]      = "LANGUAGE"

# Home page, not logged in
msg["home.splashtitle"]     = "Fame and fortune await you."
msg["home.splashsubtitle1"] = "Solve"
msg["home.splashsubtitle2"] = "coding challenges"
msg["home.splashsubtitle3"] = ", earn riches and worldwide fame."
msg["home.splashlearnmore"] = "Learn more"
msg["home.username"]        = "User Name"
msg["home.password"]        = "Password"
msg["home.startcoding"]     = "START CODING"

msg["home.headertry1"]      = "Try one of the challenges and earn your "
msg["home.headertry2"]      = "first coins"
msg["home.challengeinfo1"]  = "# Calculates the greatest common divisor"
msg["home.challengeinfo2"]  = "# But it gives the wrong result.. can you fix it?"
msg["home.challengepython"] = "The perfect language for a beginner. Python is a great tool " +\
                           "for website development."
msg["home.challengecpp"]    = "Use at your own risk."
msg["home.challengerust"]   = "Truly the language of The Grand Panjandrum himself."
msg["home.browseall"]       = "BROWSE ALL CHALLENGES"

msg["home.headerabout1"]    = "What's"
msg["home.headerabout2"]    = "Panjandrum"
msg["home.headerabout3"]    = "about"
msg["home.about1"]          = "At Panjandrum, you can solve coding challenges in several languages to learn the basics of programming or to expand your knowledge later on. As you solve challenges you earn coins to display your coding prowess. Or you can help build this community by writing your own challenges for others to solve."
msg["home.about2"]          = "We believe that coding is an essential skill for the 21st century, and we hope Panjandrum will help unlock your potential as a master programmer. Have fun!"

# Log in
msg["login.title"]       = "Welcome Back!"
msg["login.info"]        = "Sign in to solve challenges and get wealthy."
msg["login.username"]    = "User Name"
msg["login.password"]    = "Password"
msg["login.login"]       = "LOG IN"
msg["login.signupinfo"]  = "Don't have an account? "
msg["login.signup"]      = "Sign up"

msg["login.error.username"]      = "The username does not exist!"
msg["login.error.password"]      = "Password incorrect!"

# Sign up
msg["signup.title"]         = "Well Met!"
msg["signup.subtitle"]      = "Sign up now to complete challenges and earn fabulous prizes."
msg["signup.rewardtitle"]   = "Great Job!"
msg["signup.rewardinfo1"]   = "Sign up to claim your"
msg["signup.rewardinfo2"]   = " and solve more challenges."
msg["signup.rewardcoins"]   = "100 coins"
msg["signup.username"]      = "User Name"
msg["signup.password"]      = "Password"
msg["signup.confirmation"]  = "Password Confrimation"
msg["signup.signup"]        = "SIGN UP"
msg["signup.logininfo"]     = "Already have an account?"
msg["signup.login"]         = "Log in"

msg["signup.error1"]      = "Invalid name!"
msg["signup.error2"]      = "The username must contain at least 3 characters!"
msg["signup.error3"]      = "The username may conain at most 15 characters!"
msg["signup.error4"]      = "The passwords don't match!"
msg["signup.error5"]      = "The password must contain at least 5 characters!"

# Browse page
msg["browse.mychallenges"] = "My challenges"
msg["browse.showall"]      = "Show all"

msg["browse.anydifficulty"]     = "Any difficulty"
msg["browse.easy"]              = "Easy"
msg["browse.medium"]            = "Medium"
msg["browse.hard"]              = "Hard"

msg["browse.and"]   = "and"

msg["browse.english"]   = "English"
msg["browse.dutch"]     = "Dutch"

msg["browse.date"]          = "DATE"
msg["browse.title"]         = "TITLE"
msg["browse.popularity"]    = "POPULARITY"

msg["browse.by"]        = "by"
msg["browse.day"]       = "day"
msg["browse.days"]      = "days"
msg["browse.hour"]      = "hour"
msg["browse.hours"]     = "hours"
msg["browse.minute"]    = "minute"
msg["browse.minutes"]   = "minutes"
msg["browse.second"]    = "second"
msg["browse.seconds"]   = "seconds"
msg["browse.ago"]       = "ago"
msg["browse.stars"]     = "stars"

# Home page, logged in
msg["loggedinhome.browsemychallenges"]     = "BROWSE MY CHALLENGES"
msg["loggedinhome.createnewchallenge"]     = "CREATE NEW CHALLENGE"

msg["loggedinhome.recentactivity"]     = "Recent activity"
msg["loggedinhome.friendactivity"]     = "Friend activity"
msg["loggedinhome.recommendedchallenges"]      = "Recommended challenges"

msg["loggedinhome.ra.profile3"]             = "%s created his profile."
msg["loggedinhome.ra.profile2"]             = "You created your profile."
msg["loggedinhome.ra.challenge3"]           = "%s created a challenge called %s."
msg["loggedinhome.ra.challenge2"]           = "You created a challenge called %s."
msg["loggedinhome.ra.exercise3"]            = "%s created an exercise called %s."
msg["loggedinhome.ra.exercise2"]            = "You created an exercise called %s."
msg["loggedinhome.ra.solution3"]            = "%s solved an exercise called %s."
msg["loggedinhome.ra.solution2"]            = "You solved an exercise called %s ."
msg["loggedinhome.ra.rate3"]                = "%s rated an exercise called %s."
msg["loggedinhome.ra.rate2"]                = "You rated an exercise called %s"
msg["loggedinhome.ra.friend3"]              = "%s added a friend called %s."
msg["loggedinhome.ra.friend2"]              = "You added a friend called %s."

msg["loggedinhome.browseallchallenges"]     = "BROWSE ALL CHALLENGES"

# Search page
msg["search.resultsfor"]     = "Results for"
msg["search.challenges"]     = "Challenges"
msg["search.users"]          = "Users"
msg["search.nomatch"]        = "No match found."
msg["search.membersince"]    = "member since "

# Profile page
msg["profile.exercisescreated"]     = "Exercises created"
msg["profile.exercisessolved"]      = "Exercises solved"
#msg["profile.accept"]     = "ACCEPT FRIENDREQEST"
#msg["profile.reject"]     = "REJECT FRIENDREQUEST"
#msg["profile.cancel"]     = "CANCEL FRIENDREQUEST"
msg["profile.add"]        = "SUBSCRIBE"
msg["profile.remove"]     = "UNSUBSCRIBE"

# Create new challenge page
msg["newchallenge.challengeinfo"]     = "Challenge info"
msg["newchallenge.info"]              = "This is the information users see when browsing through the challenges."
msg["newchallenge.title"]             = "Challenge title"
msg["newchallenge.difficulty"]        = "Difficulty"
msg["newchallenge.easy"]              = "Easy"
msg["newchallenge.medium"]            = "Medium"
msg["newchallenge.hard"]              = "Hard"
msg["newchallenge.language"]          = "Language"
msg["newchallenge.english"]           = "English"
msg["newchallenge.dutch"]             = "Dutch"
msg["newchallenge.proglanguage"]      = "Programming language"
msg["newchallenge.reward"]            = "Reward"
msg["newchallenge.description"]       = "Description"
msg["newchallenge.tags"]              = "Tags"
msg["newchallenge.taginfo"]           = "use commas to separate multiple tags"
msg["newchallenge.makepublic"]        = "Make challenge public"
msg["newchallenge.savechallenge"]     = "SAVE CHALLENGE"

msg["newchallenge.exercises"]          = "Exercises"
msg["newchallenge.exerciseinfo"]       = "A challenge is made up of one or more exercises. Each exercise contains a code problem for the user to solve."
msg["newchallenge.edit"]               = "EDIT"
msg["newchallenge.delete"]             = "DELETE"
msg["newchallenge.createexercise"]     = "CREATE EXERCISE"
msg["newchallenge.findexercise"]       = "FIND EXERCISE"

# Create new exercise page
msg["newexercise.title"]     = "Exercise title"

msg["newexercise.markup"]          = "Markup"
msg["newexercise.largeheader"]     = "Large header"
msg["newexercise.smallheader"]     = "Small header"
msg["newexercise.highlight"]       = "Highlight"
msg["newexercise.hint"]            = "Hint"

msg["newexercise.codeblock"]       = "Code block"
msg["newexercise.userinput"]       = "User input"

msg["newexercise.expectedresult"]   = "Expected result"
msg["newexercise.expectregex"]      = "Expect regex"
msg["newexercise.evaluationcode"]   = "Evaluation code"

msg["newexercise.preview"]          = "PREVIEW"
msg["newexercise.save"]             = "SAVE EXERCISE"
msg["newexercise.saveandreturn"]    = "SAVE & RETURN"
msg["newexercise.update"]           = "UPDATE EXERCISE"
msg["newexercise.updateandreturn"]  = "UPDATE & RETURN"

msg["newexercise.sample"] = """This is a sample exercise that requires the user to print out "Hello World"."""

#Challenge completed page
msg["challengecompleted.congratulation"]        = "Congratulations! You have completed"
msg["challengecompleted.earning"]               = "earning"
msg["challengecompleted.exercises"]             = "EXERCISES"
msg["challengecompleted.rateinfo"]              = "Would you like to rate this challenge?"
msg["challengecompleted.rate"]                  = "rate"
msg["challengecompleted.abysmal"]               = "Abysmal"
msg["challengecompleted.poor"]                  = "Poor"
msg["challengecompleted.adequate"]              = "Adequate"
msg["challengecompleted.good"]                  = "Good"
msg["challengecompleted.sublime"]               = "Sublime"

# challengelandingpage
msg["challengelandingpage.overview"]        = "OVERVIEW"
msg["challengelandingpage.statistics"]      = "STATISTICS"
msg["challengelandingpage.startchallenge"]  = "START CHALLENGE"
msg["challengelandingpage.editchallenge"]   = "EDIT CHALLENGE"

# challengestatistics page
msg["challengestatistics.nrofsolvers"] = "of users have solved this challenge."
msg["challengestatistics.attempts"] = "Attempts"
msg["challengestatistics.ratings"] = "Ratings"
msg["challengestatistics.completion"] = "Completion over time"


# listedexercise page
msg["listedexercise.addref"]  = "ADD REF"
msg["listedexercise.addcopy"] = "ADD COPY"

# taginfo page
msg["taginfo.title"]         = "Tag info"
msg["taginfo.info1"]         = "Various tags can be used to style your exercise and specify where code should be placed."
msg["taginfo.display"]       = "Display tags"
msg["taginfo.text"]          = "TEXT"
msg["taginfo.textinfo1"]     = """Displays "Text" using a large header font."""
msg["taginfo.textinfo2"]     = """Displays "Text" using a small header font."""
msg["taginfo.textinfo3"]     = """Highlights "Text" with a color."""
msg["taginfo.codetags"]      = "Code tags"
msg["taginfo.code"]          = "Code"
msg["taginfo.codeinfo"]      = """Places "Code" in a read-only text field with syntax highlighting."""
msg["taginfo.regex"]         = "Regex"
msg["taginfo.regexinfo"]     = """Places "Code" in a text editor, modifyable by the user. The user's output must match the "Regex" regular expression."""
msg["taginfo.textinfo4"]     = """A hint button will be created. If you press the button "Text" will appear."""

# exercise page
msg["exercise.continue"] = "CONTINUE"
msg["exercise.complete"] = "COMPLETE"
msg["exercise.challenge"] = "Challenge"
msg["exercise.hasnoexercises"] = "doesn't contain exercises."
