import nl
import en

def translate(id):
    """Translates a message based on the 'language' set in the current user's preferences"""
    from bottle import request
    lang = request.session["preferences"].get("language", "en")
    if   lang == "en": table = en.msg
    elif lang == "nl": table = nl.msg
    else             : raise ValueError("unknown language " + lang)

    return table.get(id, "NO TRANSLATION (lang: %s, id: %s)" % (lang, id))
