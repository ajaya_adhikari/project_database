# NOTE: Adding or modifying messages requires a server restart.
# -*- coding: utf-8 -*-
msg = {}

# Header / Footer
msg["header.search"]        = "Zoek"
msg["header.browse"]        = "Verken"
msg["header.login"]         = "Aanmelden"
msg["header.signup"]        = "Inschijven"
msg["header.loggedin"]      = "Je bent aangemeld. "
msg["header.logout"]        = "Afmelden"

msg["footer.language"]      = "TAAL"

# Home page, not logged in
msg["home.splashtitle"]     = "Roem en rijkdom wachten op u."
msg["home.splashsubtitle1"] = "Los"
msg["home.splashsubtitle2"] = "code challenges"
msg["home.splashsubtitle3"] = " op, verwerf een fortuin en wereldwijde roem."
msg["home.splashlearnmore"] = "Leer meer"
msg["home.username"]        = "Gebruikersnaam"
msg["home.password"]        = "Wachtwoord"
msg["home.startcoding"]     = "BEGIN NU"

msg["home.headertry1"]      = "Probeer een van de challenges en verdien je "
msg["home.headertry2"]      = "eerste centjes"
msg["home.challengeinfo1"]  = "# Berekent de grootste gemene deler"
msg["home.challengeinfo2"]  = "# Maar het resultaat is fout.. kan jij het fixen?"
msg["home.challengepython"] = "De perfecte taal voor een beginner. Python is een uitstekende taal " +\
                              "voor web development."
msg["home.challengecpp"]    = "Gebruiken op eigen risico."
msg["home.challengerust"]   = "Waarlijk de taal van De Grote Panjandrum hemzelf."
msg["home.browseall"]       = "ALLE CHALLENGES VERKENNEN"

msg["home.headerabout1"]    = "Waar draait"
msg["home.headerabout2"]    = "Panjandrum"
msg["home.headerabout3"]    = "om"
msg["home.about1"]          = "Met Panjandrum kan je programmeeruitdagingen in verschillende talen oplossen om de beginselen van programmeren te leren of je kennis uit te breiden. Je kan muntjes verdienen door uitdagingen op te lossen. Of je kan de gemeenschap helpen opbouwen door je eigen uitdagingen te schrijven, die anderen vervolgens kunnen oplossen."
msg["home.about2"]          = "We geloven dat programmeren een essentiële vaardigheid is voor de 21ste eeuw, en we hopen dat Panjandrum je kan helpen om je potentieel als meesterprogrammeur waar te maken. Veel plezier!"

# Log in
msg["login.title"]       = "Welkom Terug!"
msg["login.info"]        = "Meld je aan om uitdagingen op te lossen en rijk te worden."
msg["login.username"]    = "Gebruikersnaam"
msg["login.password"]    = "Wachtwoord"
msg["login.login"]       = "AANMELDEN"
msg["login.signupinfo"]  = "Nog geen profiel?"
msg["login.signup"]      = "Registreer"

msg["login.error.username"]      = "De gebruikersnaam is ongeldig!"
msg["login.error.password"]      = "Het wachtwoord is ongeldig!"

# Sign up
msg["signup.title"]         = "Welkom, vriend!"
msg["signup.subtitle"]      = "Registreer nu om uitdagingen op te lossen en roem te verwerven."
msg["signup.rewardtitle"]   = "Goed Gedaan!"
msg["signup.rewardinfo1"]   = "Registreer om je "
msg["signup.rewardinfo2"]   = " op te eisen en meer uitdagingen op te lossen."
msg["signup.rewardcoins"]   = "100 munten"
msg["signup.username"]      = "Gebruikersnaam"
msg["signup.password"]      = "Wachtwoord"
msg["signup.confirmation"]  = "Wachtwoord-Bevestiging"
msg["signup.signup"]        = "REGISTREER"
msg["signup.logininfo"]     = "Al een profiel?"
msg["signup.login"]         = "Aanmelden"

msg["signup.error1"]      = "Ongeldige naam!"
msg["signup.error2"]      = "De gebruikersnaam moet minstens 3 tekens bevatten!"
msg["signup.error3"]      = "De gebruikersnaam mag maximaal 15 tekens bevatten!"
msg["signup.error4"]      = "De wachtwoorden komen niet overeen!"
msg["signup.error5"]      = "Het wachtwoord moet minstens 5 tekens bevatten!"

# Browse page
msg["browse.mychallenges"] = "Mijn uitdagingen"
msg["browse.showall"]      = "Alles"

msg["browse.anydifficulty"]     = "Moeilijkheidsgraad"
msg["browse.easy"]              = "Gemakkelijk"
msg["browse.medium"]            = "Gemiddeld"
msg["browse.hard"]              = "Moeilijk"

msg["browse.english"]   = "Engels"
msg["browse.dutch"]     = "Nederlands"

msg["browse.and"]   = "en"

msg["browse.date"]          = "DATUM"
msg["browse.title"]         = "TITEL"
msg["browse.popularity"]    = "POPULARITEIT"

msg["browse.by"]        = "door"
msg["browse.day"]       = "dag"
msg["browse.days"]      = "dagen"
msg["browse.hour"]      = "uur"
msg["browse.hours"]     = "uren"
msg["browse.minute"]    = "minuut"
msg["browse.minutes"]   = "minuten"
msg["browse.second"]    = "seconde"
msg["browse.seconds"]   = "seconden"
msg["browse.ago"]       = "geleden"
msg["browse.stars"]     = "sterren"

# Home page, logged in
msg["loggedinhome.browsemychallenges"]     = "VERKEN MIJN UITDAGINGEN"
msg["loggedinhome.createnewchallenge"]     = "CREËER NIEUWE UITDAGING"

msg["loggedinhome.recentactivity"]     = "Recente gebeurtenissen"
msg["loggedinhome.friendactivity"]     = "Gebeurtenissen van vrienden"
msg["loggedinhome.recommendedchallenges"]      = "Aanbevolen uitdagingen"

msg["loggedinhome.ra.profile3"]             = "%s heeft zijn profiel aangemaakt."
msg["loggedinhome.ra.profile2"]             = "Je hebt je profiel aangemaakt."
msg["loggedinhome.ra.challenge3"]           = "%s heeft een uitdaging aangemaakt genaamd %s."
msg["loggedinhome.ra.challenge2"]           = "Je hebt een uitdaging aangemaakt genaamd %s."
msg["loggedinhome.ra.exercise3"]            = "%s heeft een oefening aangemaakt genaamd %s."
msg["loggedinhome.ra.exercise2"]            = "Je hebt een oefening aangemaakt genaamd %s."
msg["loggedinhome.ra.solution3"]            = "%s heeft een oefening opelost genaamd %s."
msg["loggedinhome.ra.solution2"]            = "Je hebt een oefening opgelost genaamd %s."
msg["loggedinhome.ra.rate3"]                = "%s heeft een oefening beoordeeld genaamd %s."
msg["loggedinhome.ra.rate2"]                = "Je hebt een oefening beoordeeld genaamd %s."
msg["loggedinhome.ra.friend3"]              = "%s heeft een vriend toegevoegd genaamd %s."
msg["loggedinhome.ra.friend2"]              = "Je hebt een vriend toegevoegd genaaamd %s."

msg["loggedinhome.browseallchallenges"]     = "VERKEN ALLE UITDAGINGEN"

# Search page
msg["search.resultsfor"]     = "Resultaten voor"
msg["search.challenges"]     = "Uitdagingen"
msg["search.users"]          = "Gebruikers"
msg["search.nomatch"]        = "Geen zoekeresultaat."
msg["search.membersince"]    = "lid sinds "

# Profile page
msg["profile.exercisescreated"]     = "Oefeningen aangemaakt"
msg["profile.exercisessolved"]      = "Oefeningen opgelost"
#msg["profile.accept"]     = "AANVAARD VRIENDSCHAPSVERZOEK"
#msg["profile.reject"]     = "WIJS VRIENDSCHAPSVERZOEK AF"
#msg["profile.cancel"]     = "ANNULEER VRIENDSCHAPSVERZOEK"
msg["profile.add"]        = "VOLGEN"
msg["profile.remove"]     = "NIET MEER VOLGEN"

# Create new challenge page
msg["newchallenge.challengeinfo"]     = "Uitdaging informatie"
msg["newchallenge.info"]              = "Dit is de informatie die de gebruikers te zien krijgen bij het verkennen van de uitdagingen."
msg["newchallenge.title"]             = "Uitdaging titel"
msg["newchallenge.difficulty"]        = "Moeilijkheidsgraad"
msg["newchallenge.easy"]              = "Gemakkelijk"
msg["newchallenge.medium"]            = "Gemiddeld"
msg["newchallenge.hard"]              = "Moeilijk"
msg["newchallenge.english"]           = "Engels"
msg["newchallenge.dutch"]             = "Nederlands"
msg["newchallenge.language"]          = "Taal"
msg["newchallenge.proglanguage"]      = "Programmeertaal"
msg["newchallenge.reward"]            = "Beloning"
msg["newchallenge.description"]       = "Beschrijving"
msg["newchallenge.tags"]              = "Tags"
msg["newchallenge.taginfo"]           = "gebruik komma's om meerdere tags te onderscheiden"
msg["newchallenge.makepublic"]        = "Maak uitdaging publiek"
msg["newchallenge.savechallenge"]     = "UITDAGING OPSLAAN"

msg["newchallenge.exercises"]          = "Oefeningen"
msg["newchallenge.exerciseinfo"]       = "Een uitdaging bestaat uit één of meerdere oefeningen. Elke oefening bevat een code-probleem die de gebruiker moet oplossen."
msg["newchallenge.edit"]               = "WIJZIGEN"
msg["newchallenge.delete"]             = "VERWIJDEREN"
msg["newchallenge.createexercise"]     = "OEFENING AANMAKEN"
msg["newchallenge.findexercise"]       = "OEFENING ZOEKEN"

# Create new exercise page
msg["newexercise.title"]     = "Oefeningtitel"

msg["newexercise.markup"]          = "Markering"
msg["newexercise.largeheader"]     = "Grote titel"
msg["newexercise.smallheader"]     = "Kleine titel"
msg["newexercise.highlight"]       = "opvallend"
msg["newexercise.hint"]            = "Hint"

msg["newexercise.codeblock"]       = "Block code"
msg["newexercise.userinput"]       = "Gebruiker's invoer"

msg["newexercise.expectedresult"]   = "Verwacht resultaat"
msg["newexercise.expectregex"]      = "Verwacht regex"
msg["newexercise.evaluationcode"]   = "Evaluatie code"

msg["newexercise.preview"]          = "Voorbeeld"
msg["newexercise.save"]             = "OEFENING OPSLAAN"
msg["newexercise.saveandreturn"]    = "OPSLAAN & TERUGKEREN"
msg["newexercise.update"]           = "OEFENING UPDATEN"
msg["newexercise.updateandreturn"]  = "UPDATEN & TERUGKEREN"

msg["newexercise.sample"] = """Dit is een voorbeeldoefening waarbij de gebruiker "Hello World" moet printen."""

#Challenge completed page
msg["challengecompleted.congratulation"]        = "Gefeliciteerd! U hebt de uitdaging opgelost."
msg["challengecompleted.earning"]               = "beloning"
msg["challengecompleted.exercises"]             = "OEFENINGEN"
msg["challengecompleted.rateinfo"]              = "Wilt u deze uitdaging beoordelen?"
msg["challengecompleted.rate"]                  = "beoordeling"
msg["challengecompleted.abysmal"]               = "Zeer slecht"
msg["challengecompleted.poor"]                  = "Slecht"
msg["challengecompleted.adequate"]              = "Gemiddeld"
msg["challengecompleted.good"]                  = "Goed"
msg["challengecompleted.sublime"]               = "Subliem"

# challengelandingpage
msg["challengelandingpage.overview"]        = "OVERZICHT"
msg["challengelandingpage.statistics"]      = "STATISTIEKEN"
msg["challengelandingpage.startchallenge"]  = "UITDAGING BEGINNEN"
msg["challengelandingpage.editchallenge"]   = "UITDAGING WIJZIGEN"


# challengestatistics page
msg["challengestatistics.nrofsolvers"] = " van de gebruikers hebben deze uitdaging opgelost."
msg["challengestatistics.attempts"] = "Pogingen"
msg["challengestatistics.ratings"] = "Beoordelingen"
msg["challengestatistics.completion"] = "Voltooiing per tijdspspanne"


# listedexercise page
msg["listedexercise.addref"]  = "REF TOEVOEGEN"
msg["listedexercise.addcopy"] = "KOPIE TOEVOEGEN"

# taginfo page
msg["taginfo.title"]         = "Tag info"
msg["taginfo.info1"]         = "Gebruik verschillende tags om je oefening vorm te geven en te specifiëren waar er code geplaatst moet worden."
msg["taginfo.display"]       = "Display tags"
msg["taginfo.text"]          = "TEKST"
msg["taginfo.textinfo1"]     = """Toont "Tekst" als grote titel."""
msg["taginfo.textinfo2"]     = """Toont "Tekst" als kleine titel."""
msg["taginfo.textinfo3"]     = """Accentueert "Tekst" met een kleur."""
msg["taginfo.codetags"]      = "Code tags"
msg["taginfo.code"]          = "Code"
msg["taginfo.codeinfo"]      = """Plaatst "Code" in een "read-only" tekstvak met sintax highlighting."""
msg["taginfo.regex"]         = "Regex"
msg["taginfo.regexinfo"]     = """Plaatst "Code" in een teksteditor, die door de gebruiker aangepast kan worden. De uitkomst van de gebruiker moet overeenkomen met de Regex."""
msg["taginfo.textinfo4"]     = """Er zal  een hint-knop gecreëerd worden. Als je op de knop drukt zal er "Tekst" verschijnen."""


# exercise page
msg["exercise.continue"] = "VOORTGAAN"
msg["exercise.complete"] = "OPLOSSEN"
msg["exercise.challenge"] = "Uitdaging"
msg["exercise.hasnoexercises"] = "bevat geen oefeningen."
