import os
import sys

srcdir = os.path.dirname(os.path.realpath(__file__))
libdir = os.path.join(srcdir, "../lib")
sys.path = [srcdir, libdir] + sys.path
os.chdir(srcdir)

######################################################################

import bottle, main

application = main.app
