# Collection of procedures that are used in the runcode modules for the 
# various programming languages.

import uuid
import time
import subprocess
import os
import inspect

abs_path = os.path.dirname(
            os.path.abspath(inspect.getfile(inspect.currentframe())))
tmp_dir_path = '/'.join(abs_path.split('/')[:-1]) + '/tmp'

def generate_tmp_file_name():
    return 'tmp' + ''.join([c for c in str(uuid.uuid4()) if c != '-'])

def limit_runtime(proc, max_runtime, interval=0.1):
    """Kill process if its running time exceeds max_runtime.
    Return True iff the process terminated itself within the allowed time."""
    mark = time.time()
    while proc.poll() == None:
        time.sleep(interval)
        if time.time() - mark > max_runtime:
            proc.terminate()
            time.sleep(0.2)
            if proc.poll() == None:
                proc.kill()
            return False
    return True

def run_executable(exec_filename, timeout):
    proc = subprocess.Popen(exec_filename,
                            universal_newlines=True,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)

    if limit_runtime(proc, timeout):
        (output, _) = proc.communicate()
        return True, output
    else:
        (output, _) = proc.communicate()
        return False, output + timeout_error

def lines(s):
    """Generate the lines in a string."""
    buf = ''
    for c in s:
        if c == '\n':
            yield buf
            buf = ''
        else:
            buf += c

timeout_error = 'ERROR: program exceeded maximum allowed running time. ' + \
                'Check your code for infinite loops.\n'
