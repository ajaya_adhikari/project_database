import re

class Code:
    def __init__(self, text, close=False):
        self.text = text
        self.close = close
    def getHtml(self):
        if self.close:
            return "</textarea>"
        return "<textarea>"

class Hint:
    def __init__(self, elements, close=False):
        self.elements = elements
        self.close = close
    def getHtml(self):
        if self.close:
            return "</span>"
        return "<span class=\"hint\">"
class Text:
    def __init__(self, text):
        self.text = text
    def getHtml(self):
        return self.text

def convert(text):
    # Remove scripts
    pattern = "<\s*script\s*>.*\<\s*/script\s*\>"
    regex = re.compile(pattern, re.IGNORECASE|re.DOTALL)
    text = regex.sub("",text)

    scanner=re.Scanner([
      (r"\s+",          lambda scanner,token:("WHITESPACE", Text(token))),
      (r"\[code\]",     lambda scanner,token:("codeOpen", Code(token))),
      (r"\[/code\]",    lambda scanner,token:("codeClose", Code(token,True))),
      (r"\[hint\]",     lambda scanner,token:("hintOpen", Hint(token))),
      (r"\[/hint\]",    lambda scanner,token:("hintClose", Hint(token,True))),
      (r"[^\s]*",       lambda scanner,token:("text", Text(token))),
    ], re.IGNORECASE)
    results, rest = scanner.scan(text)
    html = ""
    for code, object in results:
        html += object.getHtml()
    return html
