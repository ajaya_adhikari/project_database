from bottle import route, template
from database import db
from datetime import datetime
from lang import translate
import math

# Slight hackery
def setbrowsedata(challenges):
    ut = db.users_table()
    date_now = datetime.now()

    for challenge in challenges:
        challenge.author_id = ut.get_by_id(challenge.author_id).name
        date_ago = date_now - challenge.time
        seconds = date_ago.total_seconds()
        days, remainder  = divmod(seconds, 86400)
        hours, remainder = divmod(remainder, 3600)
        minutes, seconds = divmod(remainder, 60)

        time_list = []
        time = ""

        if days >= 2:
            time = "%s %s " % (int(days), translate("browse.days"),)
        elif days >= 1:
            time = "%s %s " % (int(days), translate("browse.day"))
        else:
            time = " "
        time_list.append(time)

        if hours >= 2:
            time = "%s %s " % (int(hours), translate("browse.hours"))
        elif hours >= 1:
            time = "%s %s " % (int(hours), translate("browse.hour"))
        else:
            time = " "
        time_list.append(time)

        if minutes >= 2:
            time = "%s %s " % (int(minutes), translate("browse.minutes"))
        elif minutes >= 1:
            time = "%s %s " % (int(minutes), translate("browse.minute"))
        else:
            time += " "
        time_list.append(time)

        if seconds >= 2:
            time = "%s %s" % (int(seconds), translate('browse.seconds'))
        elif seconds >= 1:
            time = "%s %s" % (int(seconds), translate('browse.second'))
        else:
            time = " "
        time_list.append(time)

        if len(time_list) > 1:
            time_list.insert(len(time_list) - 1,"%s " % (translate("browse.and"), ))
        challenge.time = "".join(time_list)
        challenge.difficulty = [translate("browse.easy"),translate("browse.medium"),translate("browse.hard")][challenge.difficulty-1]
        if challenge.language == "en":
            challenge.language = translate("browse.english")
        elif challenge.language == "nl":
            challenge.language = translate("browse.dutch")

        # print 'avg_rating:', challenge.avg_rating
        # print temper_rating(challenge)
        print challenge.tempered_rating
        challenge.tempered_rating = '{0:.1f}'.format(challenge.tempered_rating)

        tt = db.tag_table()
        challenge.tags = tt.get_by_series(challenge)

def temper_rating(challenge, base_average=3.0, dummy_voter_count=10):
    """Tempers a rating by pretending a number of voters has
    given average scores."""
    if challenge.avg_rating == None:
        challenge.avg_rating = 0
    tempered_rating = \
        (challenge.avg_rating * float(challenge.rating_count) \
                + base_average * float(dummy_voter_count)) \
        / float(challenge.rating_count + dummy_voter_count)
    return tempered_rating
