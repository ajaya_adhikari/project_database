import subprocess
import multiprocessing
import time
import os
import inspect
import sys
import shutil
import re
from runcodeutils import *

def evaluate_output(user_code, regex, timeout=3):
    """Run user_code as Python code and check whether its output on stdout 
    matches the supplied regex."""

    # Write solution module to file.
    user_code_module_name = generate_tmp_file_name()
    user_code_file_name = tmp_dir_path + '/' + user_code_module_name + '.py'
    user_code_file = open(user_code_file_name, 'w+')
    user_code_file.write(user_code)
    user_code_file.close()

    # Run user code and collect output.
    (success,result) = run_python_file(user_code_file_name, timeout)

    # Clean up file.
    os.remove(user_code_file_name)

    # Check output against provided regex.
    if success and re.match(regex, result):
        return True,result
    else:
        return False,result

# To be appended to the evaluate module. Writes report to stdout.
report_eval_result = """
print('T' if evaluate() else 'F', end = '')
"""

def run_evaluation_code(user_code, evaluation_code, timeout=3):
    """Run user_code as Python code and have its definitions stored 
    in a dict. That dict is then passed to evaluation_code, allowing it to 
    run and test the objects defined in user_code.

    The user_code is expected to define a number of Python objects. All 
    side-effects are currently ignored.
    
    The evaluation_code string is expected to contain an 'evaluate' function.
    The author of the evaluation code should treat the code written by the user
    as a Python module named 'solution'.
    
    evaluate is expected to return (success,feedback), where success is a Bool
    indicating whether the test was successful, and feedback is a string.
    """

    # Write solution module to file.
    user_code_module_name = generate_tmp_file_name()
    user_code_file_name = tmp_dir_path + '/' + user_code_module_name + '.py'
    user_code_file = open(user_code_file_name, 'w+')
    user_code_file.write(user_code)
    user_code_file.close()

    # Write evaluation module to file.
    evaluation_code_module_name = generate_tmp_file_name()
    evaluation_code_file_name = tmp_dir_path + '/' + evaluation_code_module_name + '.py'
    evaluation_code_file = open(evaluation_code_file_name, 'w+')
    evaluation_code_file.write('import {} as solution\n'.format(user_code_module_name))
    evaluation_code_file.write(evaluation_code)
    evaluation_code_file.write(report_eval_result)
    evaluation_code_file.close()

    # Run evaluator.
    errorfree, output = run_python_file(evaluation_code_file_name, timeout)

    # Interpret result.
    result, correct = output[:-1], (output[-1] == 'T')
    result = ("Your solution is correct!\n\n" if correct \
              else "Your solution contains an error.\n\n") + result

    # Clean up.
    os.remove(user_code_file_name)
    os.remove(evaluation_code_file_name)

    return correct, result

def run_python_file(filename, timeout):
    """Run a Python file as a subprocess and return (success,output)."""
    # Launch subprocess.
    proc = subprocess.Popen(['python3', '-B', filename],
                            universal_newlines=True,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)


    # Time-out if necessary.
    if limit_runtime(proc, timeout):
        # Program ran succesfully, return output.
        (output,_) = proc.communicate()
        return True, output
    else:
        # Program failed to terminate properly, return an error message.
        (output,_) = proc.communicate()
        return False, output + timeout_error
