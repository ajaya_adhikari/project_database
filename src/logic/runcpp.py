import subprocess
import multiprocessing
import time
import uuid
import os
import inspect
import sys
import shutil
import re
from runcodeutils import *

def evaluate_output(user_code, regex, timeout=3):
    """Run user_code as C++ code and check whether its output on stdout 
    matches the supplied regex."""

    # Write solution code to file, wrapped in its own namespace.
    base_filename = tmp_dir_path + '/' + generate_tmp_file_name()
    filename = base_filename + '.cpp'
    cpp_file = open(filename, 'w+')
    cpp_file.write(user_code)
    cpp_file.close()

    # Compile C++ code.
    compile_time_errors = compile_cpp(filename, base_filename, timeout)

    # Only continue execution if there were no compile-time errors.
    if len(compile_time_errors) == 0:
        errorfree_execution, output = run_executable(base_filename, timeout)
        os.remove(base_filename)
        success = errorfree_execution and bool(re.match(regex, output))
    else:
        success = False
        output = compile_time_errors

    # Clean up and return results.
    os.remove(filename)
    return success, output

def compile_cpp(cpp_filename, output_filename, timeout):
    """Compiles a C++ file and writes the binary to disk. Returns compiler 
    stderr output or an error indicating that compilation time exceeded
    the allowed time."""

    # Launch g++, suppressing all warnings.
    proc = subprocess.Popen(['g++', '-w' , '-o' + output_filename, cpp_filename], 
                            universal_newlines=True,
                            stderr=subprocess.PIPE)

    # Limit compilation time.
    if limit_runtime(proc, timeout):
        # Compilation terminated within the allowed time frame.
        # Collect and return compile-time error output.
        (_, errors) = proc.communicate()
        return errors
    else:
        # Compilation did not terminate within allowed time, delete executable 
        #  and return error message.
        try:
            os.remove(base_filename)
        except:
            pass
        return 'ERROR: program compilation time exceeded maximum ' + \
               'allowed running time.'

