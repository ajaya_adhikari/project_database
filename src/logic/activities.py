from lang import translate

def activitiestotext(activities):
    """activities is a list
    that contains the 5 most recent activities of a user or his friends
    one activity is of the form
        [description, name, time, logo] for a certain user
        [description, name, time, logo, user] where 'user' stands for user's friends
    this form is translated to
        [text, logo]
    so that it can be used in a view.

    Briefly: we translate key words into plain text"""
    textlist = []
    for activity in activities:
        text = ()
        if activity[0] == "profile":
            if len(activity) == 4:
                text = (translate("loggedinhome.ra.profile3") % (activity[3], ), "hammer")
            else:
                text = (translate("loggedinhome.ra.profile2"), "hammer")
        elif activity[0] == "challenge":
            if len(activity) == 4:
                text = (translate("loggedinhome.ra.challenge3") % (activity[3], activity[1]), "hammer")
            else:
                text = (translate("loggedinhome.ra.challenge2") % (activity[1], ), "hammer")
        elif activity[0] == "exercise":
            if len(activity) == 4:
                text = (translate("loggedinhome.ra.exercise3") % (activity[3], activity[1]), "hammer")
            else:
                text = (translate("loggedinhome.ra.exercise2") % (activity[1], ), "hammer")
        elif activity[0] == "solution":
            if len(activity) == 4:
                text = (translate("loggedinhome.ra.solution3") % (activity[3], activity[1]), "ruby_small")
            else:
                text = (translate("loggedinhome.ra.solution2") % (activity[1], ), "ruby_small")
        elif activity[0] == "rate":
            if len(activity) == 4:
                text = (translate("loggedinhome.ra.rate3") % (activity[3], activity[1]), "ruby_small")
            else:
                text = (translate("loggedinhome.ra.rate2") % (activity[1], ), "ruby_small")
        elif activity[0] == "friend":
            if len(activity) == 4:
                text = (translate("loggedinhome.ra.friend3") % (activity[3], activity[1]), "person_gray")
            else:
                text = (translate("loggedinhome.ra.friend2") % (activity[1], ), "person_gray")
        elif activity[0] == "":
            if len(activity) == 4:
                pass
            else:
                pass
        textlist.append(text)
    return textlist
