import os
import threading
from bottle import route, template

@route("/debuginfo")
def debuginfo():
    tid   = threading.current_thread().ident
    tname = threading.current_thread().name
    pid   = os.getpid()
    return template(
            "tid: {{tid}}, tname: {{tname}}, pid: {{pid}}",
            tid=tid,
            tname=tname,
            pid=pid)
