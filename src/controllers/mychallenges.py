from bottle import route, template, request
from database import db
from datetime import datetime
from logic.browseformat import setbrowsedata
import math

# TODO change to other template or merge with the browse_page controller
@route( "/challenge/"
        "<lang:re:en|nl>/"
        "<prog:re:all|python|cpp|rust>/"
        "<difficulty:re:any|easy|medium|hard>/"
        "<sort_by:re:date|title|popularity>/"
        "<direction:re:asc|desc>/"
        "<page:int>")
def challenge_page(lang,prog,difficulty,sort_by,direction,page):
    if not request.session.get("logged_in"):
        redirect('/login')

    # Amount of challenges listed per page
    per_page = 5

    est = db.exercise_series_table()

    # Sort the challenge list in a particular fashion
    order = True
    if direction == "desc":
        order = False
    sort = []
    if sort_by == "date":
        order = not order # We store a date, show user a date difference
        sort.append(('time',order))
    elif sort_by == "title":
        sort.append(('name',order))
    elif sort_by == "popularity":
        sort.append(('times_made',order))

    # Choose which challenge are displayed
    select = [('language',lang)]
    select.append(('author_id', request.session.get("user_id")))
    if prog != "all":
        select.append(('programming_language',prog))
    difficulty_int = 1
    if difficulty != "any":
        if difficulty == "easy":
             difficulty_int = 1
        elif difficulty == "medium":
             difficulty_int = 2
        elif difficulty == "hard":
             difficulty_int = 3
        select.append(('difficulty',difficulty_int))

    # Calculate how many pages are required
    amount = est.get_filtered_amount(select)
    pages =  int(math.ceil(float(amount)/float(per_page)))

    if page < 1:
        page = 1
    elif page != 1 and page > pages:
        page = pages

    challenges = est.get_sorted_slice(sort,
                                      select,
                                      per_page,per_page*(page-1))
    setbrowsedata(challenges)
    return template("browse",
                    challenges=challenges,
                    pages=pages,
                    currentPage=page,
                    prog=prog,
                    lang=lang,
                    difficulty=difficulty,
                    sort_by=sort_by,
                    order=direction,
                    mine_only=True)

@route("/challenge")
def challenge_simple():
    return challenge_page("en","all","any","date","asc",1)
