from bottle import route, template, request, response, redirect
from database import db
from lang import translate
import re


@route("/signup")
def signup():
    return template("signup", 
            completed_home_challenge=request.session.get("completed_home_challenge", False))


@route("/signup", method="POST")
def signup():
    username = request.forms.get("username")
    password = request.forms.get("password")
    passwordconfirmation = request.forms.get("passwordconfirmation")

    errormsg = ""
    if re.match("^[A-Za-z]+[A-Za-z0-9]*$", username) == None:
        errormsg = translate("signup.error1")
    elif len(username) < 3:
        errormsg = translate("signup.error2")
    elif len(username) > 15:
        errormsg = translate("signup.error3")
    elif password != passwordconfirmation:
        errormsg = translate("signup.error4")
    elif len(password) < 5:
        errormsg= translate("signup.error5")
    if errormsg != "":
        return template(
            "signup",
            errormsg=errormsg)

    ut = db.users_table()
    if ut.get_by_name(username) == None:
        user = ut.insert(username,password)
        request.session["logged_in"] = True
        request.session["user_id"]   = user.id
        request.session["user_name"] = user.name
        redirect("/")
    else:
        return template(
            "signup",
            errormsg="Username already exists.")
