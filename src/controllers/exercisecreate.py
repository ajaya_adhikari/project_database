from bottle import route, template, request, response, redirect
from database import db
import parseInput


# No checks, anyone can call this
@route("/challenge/create/<challenge_id:int>/exercise")
def create_exercise(challenge_id):
    if not request.session.get("logged_in"):
        redirect('/login')
    return template("exercisecreate",
                    challenge_id=challenge_id,
                    error_string="",
                    text_field="")


# No checks, anyone can call this
@route("/challenge/create/<challenge_id:int>/exercise", method="POST")
def create_exercise_post(challenge_id):
    if not request.session.get("logged_in"):
        redirect('/login')
    title = request.forms.get("title")
    tb = request.forms.get("text-base")

    # Check if valid, as not to allow invalid to be saved
    preview = parseInput.getHtml(tb)
    html = preview[1]
    if not preview[0]:
        return template("exercisecreate",
                        challenge_id=challenge_id,
                        error_string=preview[1],
                        text_field=tb,
                        title=title)

    # User wants to preview
    if(request.forms.get('preview') is not None):
        challenge = db.exercise_series_table().get_by_id(challenge_id)
        challenge_title = challenge.name
        author_name = db.users_table().get_by_id(challenge.author_id).name
        html = html.replace("{{content}}","")
        html = html.replace("{{feed_back}}","")
        html = html.replace("{{challenge_id}}","")
        html = html.replace("{{exercise_id}}","")
        return template("exercise",
                        preview=html,
                        challenge_exercises=[],
                        exercise_title = title,
                        challenge_title=challenge_title,
                        author_name=author_name)

    # TODO check current user, series and insert to db if doesn't exist
    author_id = db.exercise_series_table().get_by_id(challenge_id).author_id
    et = db.exercises_table()
    inserted_exercise = et.insert(author_id, title, tb)
    eist = db.exercises_in_series_table()
    eist.insert_exercise_in_series(challenge_id, inserted_exercise.id)
    if(request.forms.get('return') is not None):
        redirect("/challenge/create/{}".format(challenge_id))
    else: # save
        exercise_num = eist.get_num(challenge_id, inserted_exercise)
        redirect("/challenge/create/{}/exercise/{}".format(challenge_id,
                                                           exercise_num))


# No checks, anyone can call this
@route("/challenge/create/<challenge_id:int>/exercise/<exercise_num:int>")
def update_exercise(challenge_id, exercise_num):
    if not request.session.get("logged_in"):
        redirect('/login')
    est = db.exercise_series_table()
    challenge = est.get_by_id(challenge_id)
    et = db.exercises_table()
    exercise = et.get_by_series_and_num(challenge_id,exercise_num)
    return template("exercisecreate",
                    challenge_id=challenge_id,
                    error_string="",
                    text_field="",
                    exercise=exercise,
                    exercise_num=exercise_num)


# No checks, anyone can call this
@route("/challenge/create/<challenge_id:int>/exercise/<exercise_num:int>", method="POST")
def update_exercise_post(challenge_id, exercise_num):
    if not request.session.get("logged_in"):
        redirect('/login')
    title = request.forms.get("title")
    tb = request.forms.get("text-base")
    et = db.exercises_table()
    exercise = et.get_by_series_and_num(challenge_id,exercise_num)

    # Check if valid, as not to allow invalid to be saved
    preview = parseInput.getHtml(tb)
    html = preview[1]
    if not preview[0]:
        return template("exercisecreate",
                        challenge_id=challenge_id,
                        error_string=preview[1],
                        text_field=tb,
                        exercise=exercise,
                        exercise_num=exercise_num)

    # User wants to preview
    if(request.forms.get('preview') is not None):

        challenge = db.exercise_series_table().get_by_id(challenge_id)
        challenge_title = challenge.name
        author_name = db.users_table().get_by_id(challenge.author_id).name
        html = html.replace("{{content}}","")
        html = html.replace("{{feed_back}}","")
        html = html.replace("{{challenge_id}}","")
        html = html.replace("{{exercise_id}}","")
        return template("exercise", preview=html, challenge_exercises=[],\
                     exercise_title = title, challenge_title=challenge_title, author_name=author_name)

    et = db.exercises_table()
    exercise = et.get_by_series_and_num(challenge_id,exercise_num)
    exercise.title = title
    exercise.contents = tb
    et.update_entry(exercise)
    if(request.forms.get('return') is not None):
        redirect("/challenge/create/{}".format(challenge_id))
    else: # update
        redirect("/challenge/create/{}/exercise/{}".format(challenge_id,exercise_num))


# No checks, anyone can call this
@route("/challenge/create/<challenge_id:int>/exercise/<exercise_num:int>/delete")
def delete_exercise(challenge_id, exercise_num):
    if not request.session.get("logged_in"):
        redirect('/login')
    eist = db.exercises_in_series_table()
    eist.remove_exercise_from_series_by_index(challenge_id, exercise_num)
    redirect("/challenge/create/{}".format(challenge_id))


@route("/challenge/taginfo")
def tag_info():
    return template("taginfo")
