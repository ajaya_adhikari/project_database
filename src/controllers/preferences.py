from bottle import route, template, request, redirect
from database import db

@route("/preferences/<key>/<value>")
def preferences(key, value):
    """Stores a key-value pair in the session's 'preferences' dictionary."""
    request.session["preferences"][key] = value
    ref = request.headers.get("Referer")
    redirect(ref or "/")

# All preferences are stored in the session object. This means they'll be gone 
# should the session expire. Probably not worth the trouble to make separate
# database-backed preferences for logged in users
