from bottle import route, template, request, response, redirect
from database import db
import parseInput

@route("/challenge/<challenge_id:int>/<exercise_id:int>")
def view_exercise(challenge_id, exercise_id):
    session = request.session
    if not session.get("logged_in"):
        redirect('/login')
    eist = db.exercises_in_series_table()
    challenge_exercises = eist.get_exercises_by_series_id(challenge_id)

    challenge = db.exercise_series_table().get_by_id(challenge_id)
    challenge_title = challenge.name
    author_name = db.users_table().get_by_id(challenge.author_id).name

    # If there are no exercises in the challenge show a warning
    if not challenge_exercises:
        return template("exercise",
                        challenge_exercises=challenge_exercises,
                        preview=None,
                        list_exercises = "",
                        exercise_title = "",
                        challenge_title=challenge_title,
                        author_name=author_name,
                        challenge_id=challenge_id,
                        num=exercise_id)

    exercise = challenge_exercises[exercise_id - 1]

    # Insert empty solution if none is present.
    st = db.solutions_table()
    user_id = session.get('user_id')
    if not st.check_exists(exercise, user_id):
        st.insert_empty(exercise, user_id)

    # Check if user has completed this exercise
    st = db.solutions_table()
    solved_exercise = st.has_solved(session.get("user_id"),exercise)
    # Check if user has completed all exercises of challenge
    solved_challenge = False
    # Don't check all exercise if this one isn't completed yet
    if solved_exercise:
        solved_challenge = st.user_has_solved_series(session.get("user_id"),
                                                     challenge_id)
    #parse exercise
    html = parseInput.getHtml(exercise.contents)[1]
    st = db.solutions_table()
    exercise_table = db.exercises_in_series_table()
    exercise       = exercise_table.get_exercise_by_series_id_and_num(challenge_id, exercise_id)

    #Get examplecode form html
    index1 = html.find("{{content}}")
    html = html.replace("{{content}}","",1)
    index2 = html.find("{{content}}")
    example_code = html[index1:index2]
    html = html[:index1] + html[index2:]

    #check whether the user already made the execise
    if st.check_exists(exercise,user_id):
        user_code = st.get_solution(exercise,user_id)
        if user_code != "":
            #The content of the text-area is the user's solution
            example_code = user_code

    html = html.replace("{{challenge_id}}", str(challenge_id))
    html = html.replace("{{exercise_id}}", str(exercise_id))
    html = html.replace("{{content}}",example_code)
    html = html.replace("{{feed_back}}","")

    return template("exercise",
                    preview=html,
                    challenge_exercises=challenge_exercises,
                    exercise_title = exercise.title,
                    challenge_title=challenge_title,
                    author_name=author_name,
                    challenge_id=challenge_id,
                    num=exercise_id,
                    solved_challenge=solved_challenge,
                    solved_exercise=solved_exercise)
