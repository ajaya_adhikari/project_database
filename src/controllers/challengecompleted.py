from bottle import route, template, request, redirect
from database import db

@route("/challenge/<challenge_id:int>/completed")
def challenge_completed(challenge_id):
    if not request.session.get("logged_in"):
        redirect('/login')

    user_id = request.session.get("user_id")
    eist = db.exercises_in_series_table()
    st = db.solutions_table()

    est = db.exercise_series_table()
    challenge = est.get_by_id(challenge_id)
    challenge_title = challenge.name
    exercises = eist.get_exercises_in_series(challenge_id)
    titles = [ex.title for ex in exercises]
    attempt_counts = [st.get_attempt_count(ex, user_id) for ex in exercises]
    return template("challengecompleted",
                    challenge_id=challenge_id,
                    attempt_data = zip(titles, attempt_counts),
                    challenge_title=challenge_title)

@route("/challenge/<challenge_id:int>/completed", method="POST")
def challenge_completed(challenge_id):
    if not request.session.get("logged_in"):
        redirect('/login')

    rating = request.forms.get('rating')
    user_id = request.session.get('user_id')
    rt = db.ratings_table()
    if rt.has_rated(challenge_id, user_id):
        rt.update(challenge_id, user_id, rating)
    else:
        rt.insert(challenge_id, user_id, rating)
    redirect('/browse')
