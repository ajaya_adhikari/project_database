from bottle import route, template, request, response, redirect
from database import db

@route("/challenge/create")
def challenge_create():
    if not request.session.get("logged_in"):
        redirect('/login')
    return template("challengecreate", series=None, exercises=None)

@route("/challenge/create/<challenge_id:int>")
def challenge_create_id(challenge_id):
    if not request.session.get("logged_in"):
        redirect('/login')
    est = db.exercise_series_table()
    tt = db.tag_table()
    series = est.get_by_id(challenge_id)
    series.tags = tt.get_by_series(series)
    et = db.exercises_table()
    exercises = et.get_by_series_id(challenge_id)

    return template("challengecreate", series=series, exercises=exercises)

@route("/challenge/create/<challenge_id:int>", method="POST")
def challenge_create_id(challenge_id):
    if not request.session.get("logged_in"):
        redirect('/login')
    est = db.exercise_series_table()
    tt = db.tag_table()
    series = est.get_by_id(challenge_id)
    series.tags = tt.get_by_series(series)

    # User wants to add exercise
    if(request.forms.get('add') is not None):
        redirect("/challenge/create/{}/exercise".format(inserted.id))

    # User wants to save the edited challenge
    if(request.forms.get('save') is not None):
        # Check if challenge has at least one exercise
        series.name = request.forms.get("title")
        new_difficulty = request.forms.get("difficulty")
        if new_difficulty == "easy":
            series.difficulty = 1
        elif new_difficulty == "medium":
            series.difficulty = 2
        elif new_difficulty == "hard":
            series.difficulty = 3
        series.language = request.forms.get("language")
        series.planguage = request.forms.get("planguage")
        reward = request.forms.get("reward") # Not used now
        series.description = request.forms.get("description")
        series.visible = False
        if request.forms.get("public") is not None:
            eist = db.exercises_in_series_table()
            counted = eist.count_exercises_in_series(series)
            if counted > 0:
                series.visible = True
            else:
                # TODO return a real page
                return "Add at least one exercise before publishing!"

        est.update_entry(series)

        # Update tags.
        tagstring = request.forms.get("tags") # Will need to updates these too
        tags = [tag.upper() for tag in [part.strip() for part in tagstring.split(',')] if tag]
        for tag in tags:
            if tag not in series.tags:
                tt.insert(series, tag)
        for tag in series.tags:
            if tag not in tags:
                tt.delete(series, tag)
        series.tags = tags

    redirect("/challenge/create/{}".format(challenge_id))

@route("/challenge/create", method="POST")
def challenge_create_post():
    if not request.session.get("logged_in"):
        redirect('/login')
    title = request.forms.get("title")
    difficulty = request.forms.get("difficulty")
    language = request.forms.get("language")
    planguage = request.forms.get("planguage")
    reward = request.forms.get("reward") # Not used now
    description = request.forms.get("description")
    tags = request.forms.get("tags") # Will need to split these
    public = request.forms.get("public")

    if difficulty == "easy":
        difficulty = 1
    elif difficulty == "medium":
        difficulty = 2
    elif difficulty == "hard":
        difficulty = 3

    visible = False

    ut = db.users_table()
    user_id = request.session.get("user_id")
    user = ut.get_by_id(user_id)
    est = db.exercise_series_table()
    if not est.is_unique_author_name_pair(user, title):
        # TODO return a real page
        return "Your exercise series should have unique names."

    # check if this stuff already exists, add tags, use visible boolean
    # Title, User, Description, Difficulty (1-3), Subject, Programming language, Language, Public, Tags
    inserted = est.insert(title, user, description, difficulty, planguage, language, visible)

    tt = db.tag_table()
    for tag in [part.strip() for part in tags.split(',')]:
        if tag:
            tt.insert(inserted,tag.upper())

    redirect("/challenge/create/{}".format(inserted.id))


# TODO add ability to delete a challenge

@route("/challenge/<src_challenge_id:int>/copy/<dest_challenge_id:int>/ref/<exercise_id:int>")
def challenge_add_exercise_reference(dest_challenge_id, src_challenge_id, exercise_id):
    eist = db.exercises_in_series_table()
    eist.insert_exercise_in_series(dest_challenge_id, exercise_id)
    redirect("/challenge/create/{}".format(dest_challenge_id))

@route("/challenge/<src_challenge_id:int>/copy/<dest_challenge_id:int>/copy/<exercise_id:int>")
def challenge_add_exercise_copy(dest_challenge_id, src_challenge_id, exercise_id):
    eist = db.exercises_in_series_table()
    eist.insert_exercise_copy_in_series(dest_challenge_id, exercise_id)
    redirect("/challenge/create/{}".format(dest_challenge_id))
