from logic import runpython, runcpp
import json
from bottle import route, template, request
from database import db
from logic.browseformat import setbrowsedata
from logic.activities import activitiestotext
from recommendation import recommend_series

@route("/")
def home():
    if request.session.get("logged_in"):
        name = request.session.get("user_name")
        userid = request.session.get("user_id")
        credits = 123456#hardcoded !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        est = db.exercise_series_table()
        ut = db.users_table()
        ft = db.friends_table()
        st = db.exercise_series_table()

        recentActivities = ut.get_own_recent_activities(userid)
        friendActivities = ut.get_friends_recent_activities(userid)
        recommend_ids = recommend_series(userid, 5)
        recommendedseries = []
        for x in range(len(recommend_ids)):
            recommendedseries.append( st.get_by_id(recommend_ids[x]) )

        recentActivitiestext = activitiestotext(recentActivities)
        friendActivitiestext = activitiestotext(friendActivities)

        for rs in recommendedseries:
            rs.tempered_rating = st.get_tempered_rating(rs)
        setbrowsedata(recommendedseries)

        return template("loggedinhome",
                    name = name,
                    credits = credits,
                    recentActivities = recentActivitiestext,
                    friendActivities = friendActivitiestext,
                    recommendation = recommendedseries)
    return template("home")


@route("/home/eval", method="POST")
def home_eval():
    # Read form values
    user_input  = request.forms.get("user_input")
    language    = request.forms.get("language")

    # Evaluate code
    regex       = "^18$"
    eval_result = runpython.evaluate_output(user_input, regex)

    # Mark exercise as successfully completed
    if eval_result[0]:
        session = request.session
        session["completed_home_challenge"] = True

    # Return evaluation results
    return json.dumps({
        "success": eval_result[0],
        "output":  eval_result[1],
    })
