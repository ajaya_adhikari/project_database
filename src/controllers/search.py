from bottle import route, template, request
from database import db
from logic.browseformat import setbrowsedata

@route("/search", method="POST")
def search():
    query = request.forms.get("query")
    est = db.exercise_series_table()
    ut  = db.users_table()
    challenges = est.search(query)
    users      = ut.search(query)

    for c in challenges:
        c.tempered_rating = est.get_tempered_rating(c)
    setbrowsedata(challenges)
    return template("search",
                    query=query,
                    challenges=challenges,
                    users=users)

# TODO do this nicely
@route("/search/<query>")
def search(query):
    est = db.exercise_series_table()
    ut  = db.users_table()
    challenges = est.search(query)
    users      = ut.search(query)

    for c in challenges:
        c.tempered_rating = est.get_tempered_rating(c)
    setbrowsedata(challenges)
    return template("search",
                    query=query,
                    challenges=challenges,
                    users=users)
