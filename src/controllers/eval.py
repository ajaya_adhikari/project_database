from bottle import route, request
from database import db
import json
import parseInput
from logic import runpython, runcpp

eval_output_functions = \
    {'python'   : runpython.evaluate_output,
     'cpp'      : runcpp.evaluate_output}

run_eval_code_functions = \
    {'python' : runpython.run_evaluation_code}

@route("/challenge/eval", method="POST")
def eval():
    # Read form values
    user_input   = request.forms.get("user_input")
    language     = request.forms.get("language")
    challenge_id = request.forms.get("challenge_id")
    exercise_id  = request.forms.get("exercise_id") # actually num, not id. but we should change to id in the future as exercises can exist independently.

    # Evaluate code
    exercise_table          = db.exercises_in_series_table()
    exercise                = exercise_table.get_exercise_by_series_id_and_num(int(challenge_id), int(exercise_id))
    programming_language    = db.exercise_series_table().get_by_id(challenge_id).programming_language


    regex                   = parseInput.getRegex(exercise.contents).strip()
    if len(regex) > 0:
        eval_result = eval_output_functions[programming_language](user_input, regex)

    else:
        eval_code = parseInput.getEvaluationCode(exercise.contents)
        eval_result = run_eval_code_functions[programming_language](user_input, eval_code)


    # Store code & mark exercise as complete if correct
    st = db.solutions_table()
    session = request.session
    past_success = st.register_attempt(exercise, session['user_id'], user_input, eval_result[0])

    # Return evaluation results
    return json.dumps({
        "success":       eval_result[0],  # this attempt
        "past_success":  past_success,    # any previous attempts
        "output":        eval_result[1],
    })
