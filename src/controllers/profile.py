from bottle import route, template, request
from database import db

@route("/profile/"
        "<profileid:int>")
def profile(profileid):
    et = db.exercises_table()
    ut = db.users_table()
    ft = db.friends_table()
    st = db.solutions_table()

    profile = ut.get_by_id(profileid)
    userid = request.session.get("user_id")
    exercisesCreated = et.get_nr_by_author(profile)
    exercisesSolved = st.get_total_nr_exercises_solved_by_user(profile)

    button = ""
    if userid != profileid:
        boolfriends = ft.is_subscribed_to(userid, profileid)
        if boolfriends:
            button = "removefriend"
        else:
            button = "addfriend"

    return template("profile", button=button, userid=userid, profilename=profile.name, profileid=profile.id, createdchallenges=exercisesCreated, solvedchallenges=exercisesSolved)

@route("/profile/addfriend/"
        "<sender:int>/"
        "<receiver:int>")
def addfriend(sender, receiver):
    ft = db.friends_table()
    ft.insert(sender,receiver)

    return profile(receiver)

@route("/profile/removefriend/"
        "<id1:int>/"
        "<id2:int>")
def removefriend(id1, id2):
    ft = db.friends_table()
    ft.delete(id1,id2)

    return profile(id2)
