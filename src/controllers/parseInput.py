tags =     ["[code_block]", "[/code_block]",
    "[code_inline]", "[/code_inline]",\
    "[user_input, number_of_lines:", "[user_input]","[evaluation_code]", "[/evaluation_code]","[regex]","[/regex]", "[/user_input]",\
    "[hint]", "[/hint]",\
    "[highlight]", "[/highlight]",\
    "[large_header]", "[/large_header]",\
    "[small_header]", "[/small_header]"]


largestTagSize = len(max(tags, key=lambda x: len(x)))

class CodeBlock:
    def __init__(self, text,num):
        self.text = text
        self.num = num
    def __str__(self):
        return " (CodeBlock " + self.text + ") "
    def html(self):
        numberOfNewlines = len(self.text.split("\n"))

        return "<div class=\"textareaa\"><textarea id=\"text"+ str(self.num) + "\">" + self.text + \
                "\n</textarea> </div>" + \
                "\n<script>" + \
                "\ncode_m = CodeMirror.fromTextArea($$(\"#text" + str(self.num) + "\"),"\
                "{readOnly: \"nocursor\", mode: \"python\", lineNumbers: true});"\
                "code_m.setSize(\"100%\","+ str((numberOfNewlines-1)*22 + 75) + ");"\
                "\n</script\n>"


class CodeInline:
    def __init__(self, code,num):
        self.code = code
        self.num = num
    def __str__(self):
        return " (CodeInline " + self.code + ") "
    def html(self):
        numberOfNewLines = len(self.code.split("\n"))
        return "<textarea id=\"text"+ str(self.num) + "\">\n" + self.code + \
                "\n</textarea>"\
                "\n<script>"\
                "\ncode_m = CodeMirror.fromTextArea($$(\"#text" + str(self.num) + "\"),"\
                "{readOnly: \"nocursor\", mode: \"python\", lineNumbers: true});"\
                "code_m.setSize(\"100%\","+ str((numberOfNewLines-1)*20 + 75) + ");"\
                "\n</script>"


class UserInput:
    def __init__(self, numberOfLines, num):
        self.numberOfLines = numberOfLines
        self.num = num
        self.has_evaluation_code = False
        self.has_regex = False
        self.example_code = ""
        self.evaluation_code = ""
        self.regex = ""

    def addEvaluationCode(self,evaluation_code):
        self.evaluation_code = evaluation_code
        self.has_evaluation_code = True

    def addRegex(self, regex):
        self.regex = regex
        self.has_regex = True

    def addExampleCode(self,example_code):
        self.example_code = example_code

    def __str__(self):
        return " (UserInput (numberOfLines " + str(self.numberOfLines) + " ) " + "(evaluation_code " + self.evaluation_code + ")\
         + (example_code" + self.example_code + ")) "

    def html(self):

        return "<div class=\"textareaa userinput\"><form action=\"javascript: evaluate_challenge_exercise()\">\n"\
                "<textarea id='user_input' name=\"user_input\">{{content}}" + self.example_code + "{{content}}</textarea>\n"\
                "<input type='hidden' id='language' value='python'/>"\
                "<input type='hidden' id='challenge_id' value='{{challenge_id}}'/>"\
                "<input type='hidden' id='exercise_id' value='{{exercise_id}}'/>"\
                "<input class=\"submit-button pure-button pure-button-primary\"style=\"margin-top: 1em;\" type=\"submit\" value=\"Submit (Ctrl-Enter)\" id=\"submit\"/></div>"\
                "<script>\n"\
                "code_m = CodeMirror.fromTextArea($$('#user_input'), {mode: \"python\", lineNumbers: true });\n"\
                "code_m.setSize(\"100%\","+ str((self.numberOfLines-1)*20 + 75) + ");"\
                "</script>\n"\
                "</form>\n{{feed_back}}"\
                "<div id='result' class='userinput-result userinput-correct'></div>"\


class Hint:
    def __init__(self, elements):
        self.elements = elements

    def __str__(self):
        stringg = "(Hint "
        for element in self.elements:
            stringg += str(element)
        return stringg + ")"
    def html(self):
        stringg = "<hint id=\"hintid\" style=\"display:none;\">" #change id for every instance
        for element in self.elements:
            if type(element) is str:
                stringg += element
            else:
                stringg += element.html()
        stringg += "</hint>"
        stringg += "<a id=\"hintbutton\">Show hint!</a>" #change id for every instance
        stringg += "<script type=\"text/javascript\">$(function() {var button = $(\"#hintbutton\");var hint = $(\"#hintid\");button.onClick(function() {hint.show();});});</script>"
        return stringg


class Highlight:
    def __init__(self, text):
        self.text = text
    def __str__(self):
        return " (Highlight " + self.text + ") "
    def html(self):
        return " <span class='mark'>" + self.text + "</span>\n"


class LargeHeader:
    def __init__(self, text):
        self.text = text
    def __str__(self):
        return " (LargeHeader " + self.text + ") "
    def html(self):
        return "<div class=\"exercise-header-large\">" + self.text + "</div>\n"

class SmallHeader:
    def __init__(self, text):
        self.text = text
    def __str__(self):
        return " (SmallHeader " + self.text + ") "
    def html(self):
        return "<div class=\"exercise-header-small\">"  + self.text + "</div>\n"

def tokenize(stringInput):
    index = 0
    tokensList = []
    text = ""
    while index < len(stringInput):
        posTag = ""
        a = True
        for i in range(index, min(index+largestTagSize , len(stringInput))):
            posTag += stringInput[i]

            if posTag in tags:
                if  text and text[0] == " ":
                    text = text[1:]
                if text and text[len(text)-1] == " ":
                    text = text[:len(text)-1]
                if text:
                    tokensList.append(text)

                tokensList.append(posTag)
                index += len(posTag)
                text = ""
                a = False
                break


        if a:
            text += stringInput[index]
            if index == len(stringInput)-1:
                if  text and text[0] == " ":
                    text = text[1:]
                if text and text[len(text)-1] == " ":
                    text = text[:len(text)-1]
                if text:
                    tokensList.append(text)
            index += 1

    return tokensList

def trim(stringg):
    """Trim whiteSpaces, newLines and tabs at the begin and end of the string"""
    copy = stringg
    while len(copy) != 0:
        if copy[0] in ["\n", "\r", "\t" , " "]:

            copy = copy[1:]

        else:
            break

    while len(copy) != 0:
        if copy[len(copy)-1] in ["\n", "\r", "\t" , " "]:
            copy = copy[:len(copy)-1]
        else:
            break
    return copy



def helpFuction( listTokens, index, num):
    if listTokens[index] == "[highlight]":
        if listTokens[(index+1)%len(listTokens)] in tags or listTokens[(index+2)%len(listTokens)] != "[/highlight]":
            return [False, "It should be: [highlight] TEXT [/highlight]"]

        return [True, Highlight(listTokens[index+1]), num]

    elif listTokens[index] == "[large_header]":
        if listTokens[(index+1)%len(listTokens)] in tags or listTokens[(index+2)%len(listTokens)] != "[/large_header]":
            return [False, "It should be: [large_header] TEXT [/large_header]"]

        return [True, LargeHeader(listTokens[index+1]),num]

    elif listTokens[index] == "[small_header]":
        if listTokens[(index+1)%len(listTokens)] in tags or listTokens[(index+2)%len(listTokens)] != "[/small_header]":
            return [False, "It should be: [small_header] TEXT [/small_header]"]

        return [True, SmallHeader(listTokens[index+1]), num]

    elif listTokens[index] == "[code_block]":
        if listTokens[(index+1)%len(listTokens)] in tags or listTokens[(index+2)%len(listTokens)] != "[/code_block]":
            return [False, "It should be: [code_block] CODE [/code_block]"]

        return [True, CodeBlock(listTokens[index+1],num), num+1]

    elif listTokens[index] == "[code_inline]":
        if listTokens[(index+1)%len(listTokens)] in tags or listTokens[(index+2)%len(listTokens)] != "[/code_inline]":
            return [False, "It should be: [code_inline] CODE [/code_inline]"]

        return [True, CodeInline(listTokens[index+1],num), num+1]



def getObjects(stringInput):
    """Convert the raw stringInput into list of objects according to the tags in the stringInput"""
    listTokens = tokenize(stringInput)
    i = 0
    while i <len(listTokens):
        listTokens[i] = trim(listTokens[i])
        if listTokens[i] == "":
            del listTokens[i]
        i+=1

    objects = [True]                                # the first element says if the given tokens are valid
    index = 0
    num = 0
    numberOfUserInput = 0

    while index < len(listTokens):
        if listTokens[index] not in tags:
            objects.append(listTokens[index])
            index += 1
        else:

            if listTokens[index] in ["[/code_block]","[/code_inline]", "[/evaluation_code]", "[/user_input]", "[/hint]",\
                    "[/highlight]", "[/large_header]","[/small_header]"]:
                return [False, listTokens[index] + " should precede by it's corresponding tag"]

            elif listTokens[index] == "[evaluation_code]" or listTokens[index] == "[regex]":
                return [False,  " [evaluation_code] or [regex] tags should be between user_input tags"]

            elif listTokens[index] in ["[user_input, number_of_lines:","[user_input]"] :

                numberOfLines = 12
                if listTokens[index] == "[user_input, number_of_lines:":
                    try:
                        if listTokens[index+1][len(listTokens[index+1])-1] != "]":
                            raise
                        numberOfLines = int(listTokens[index+1][:len(listTokens[index+1])-1])
                    except:
                        return [False, "It should be: [user_input, number_of_lines: NUMBER] [evaluation_code] CODE [/evaluation_code] | "\
                                "[regex] REGEX [/regex] [/user_input]"]
                    index += 2
                else:
                    index += 1
                userInput = UserInput(numberOfLines,num)
                num += 1

                if listTokens[index % len(listTokens)] not in ["[evaluation_code]","[regex]"]:
                    userInput.addExampleCode(listTokens[index])
                    index += 1

                if listTokens[index %len(listTokens)] == "[evaluation_code]":
                    if listTokens[(index+1)%len(listTokens)] in tags or listTokens[(index+2)%len(listTokens)] != "[/evaluation_code]":
                        return [False, "It should be: [evaluation_code] CODE [/evaluation_code]"]
                    userInput.addEvaluationCode(listTokens[index+1])
                    index += 3
                elif listTokens[index %len(listTokens)] == "[regex]":
                    if listTokens[(index+1)%len(listTokens)] in tags or listTokens[(index+2)%len(listTokens)] != "[/regex]":
                        return [False, "It should be: [regex] CODE [/regex]"]
                    userInput.addRegex(listTokens[index+1])
                    index += 3
                else:
                    return [False, "There should be one [evaluation_code] CODE [/evaluation_code] or [regex] REGEX [/regex] tags"\
                            " between user_input tags"]

                if listTokens[index %len(listTokens)] != "[/user_input]":
                    return [False, "It should be: [user_input, number_of_lines: NUMBER] [evaluation_code] CODE [/evaluation_code] | "\
                            "[regex] REGEX [/regex] [/user_input]"]
                objects.append(userInput)
                numberOfUserInput += 1
                index += 1

            elif listTokens[index] == "[hint]":

                if listTokens[(index+1)%len(listTokens)] == "[/hint]":
                    return [False, "The content of [hint] [/hint] is empty"]
                index += 1
                hintObjects = []
                while listTokens[index] != "[/hint]" and index < len(listTokens):
                    if listTokens[index] not in tags:
                        hintObjects.append(listTokens[index])
                        index += 1
                        continue

                    listObject = helpFuction(listTokens,index,num)
                    if not listObject[0]:
                        return listObject
                    hintObjects.append(listObject[1])
                    num = listObject[2]
                    index += 3
                if listTokens[index] != "[/hint]":
                    return [False, "the [hint] block should end with a [/hint] tag"]

                index += 1
                objects.append(Hint(hintObjects))

            else:
                listObject = helpFuction(listTokens,index,num)
                if not listObject[0]:
                    return listObject
                objects.append(listObject[1])
                num = listObject[2]
                index += 3
    if numberOfUserInput != 1:
        return [False, "There should be one user_input"]
    return objects


a =  "io [code_block] for int i in range(5):\n\tprint(i)  [/code_block]\
[code_inline] for  [/code_inline]\
[user_input, number_of_lines: 12]\
[evaluation_code] CODE [/evaluation_code]\
[/user_input]\
[hint] highlight [highlight] TEXT [/highlight] [small_header] smallHeader [/small_header] [/hint]\
[highlight] highlight [/highlight] lol\
[large_header] largeHeader [/large_header]\
[small_header] smallHeader [/small_header] lol"

# NOTE oliver: quick hack to support highlighting in a paragraph.
# We should probably rethink the parsing a bit to support user-supplied paragraphs
# (eg. by leaving a blank line in between pieces of text.)
def getHtml(inputString):
    objects = getObjects(inputString)
    if objects[0] is False:
        return (False, objects[1])
    htmlString = ""
    open_paragraph = False
    for element in objects[1:]:
        if not isinstance(element, (Highlight, str)) and open_paragraph:
            htmlString += "</p>"
            open_paragraph = False
        if type(element) is str:
            if not open_paragraph:
                htmlString += "<p>"
                open_paragraph = True
            htmlString += element
        else:
            htmlString += element.html()
    if open_paragraph:
        htmlString += "</p>"
    return objects[0],htmlString

def getRegex(inputString):
    index1 = inputString.find("[regex]") + 7
    index2 = inputString.find("[/regex]",index1)
    if index1 == -1 or index2 == -1:
        return ""
    return inputString[index1:index2]

def getEvaluationCode(inputString):
    index1 = inputString.find("[evaluation_code]") + 17
    index2 = inputString.find("[/evaluation_code]",index1)
    if index1 == -1 or index2 == -1:
        return ""
    return inputString[index1:index2]
