from __future__ import division # Float division in case we run python2 instead of python3
from bottle import route, template, request, response
from collections import namedtuple
from database import db
import random
import datetime

@route("/challenge/<challenge_id:int>/overview")
def challenge_landing_page(challenge_id, copy_challenge_id=None):
    eist = db.exercises_in_series_table()
    exercises = eist.get_exercises_by_series_id(challenge_id)
    est = db.exercise_series_table()
    challenge = est.get_by_id(challenge_id)
    can_edit = False
    if challenge.author_id == request.session.get("user_id"):
        can_edit = True
    author_name = db.users_table().get_by_id(challenge.author_id).name
    return template("challengeoverview",
                    author_name=author_name,
                    exercises=exercises,
                    challenge=challenge,
                    can_edit=can_edit,
                    copy_challenge_id=copy_challenge_id)

@route("/challenge/<challenge_id:int>/statistics")
def challenge_landing_page_statistics(challenge_id):
    eist = db.exercises_in_series_table()
    exercises = eist.get_exercises_by_series_id(challenge_id)
    est = db.exercise_series_table()
    rt = db.ratings_table()
    st = db.solutions_table()
    challenge = est.get_by_id(challenge_id)

    attempts =  st.count_users_attempted_to_solve_series(challenge)
    if attempts == 0:
        attempts = 1
    solved =  st.count_users_solved_series(challenge)
    print attempts
    print solved
    percentage_solved = "{:.1%}".format(solved/attempts)
    print percentage_solved
    # Chart data
    # BarChart for average attempts
    average_attempts_data = []
    DataPoint = namedtuple("DataPoint", "label value")
    for i, exercise in enumerate(exercises):
        average_attempts_data.append(DataPoint(exercises[i].title,st.avg_attempts_per_exercise(exercise)))

    # BarChart for ratings
    rating_data = []
    DataPoint = namedtuple("DataPoint", "label value")

    votes_per_score = rt.votes_per_score(challenge_id)
    for i in range(5):
        rating_data.append(DataPoint(["Abysmal","Poor","Adequate","Good","Sublime"][i],votes_per_score[i]))

    # LineGraph
    DataPoint = namedtuple("DataPoint", "label value")
    timestamps = st.series_solved_over_time(challenge_id)
    completion_data = [DataPoint(str(label),int(value)) for (label,value) in compute_solved_over_time_graph(timestamps)]

    # Can the user edit the challenge
    can_edit = False
    if challenge.author_id == request.session.get("user_id"):
        can_edit = True

    author_name = db.users_table().get_by_id(challenge.author_id).name
    return template("challengestatistics",
                    challenge=challenge,
                    author_name=author_name,
                    can_edit=can_edit,
                    percentage_solved=percentage_solved,
                    rating_data=rating_data,
                    completion_data=completion_data,
                    average_attempts_data=average_attempts_data)

@route("/challenge/<challenge_id:int>")
def challenge_landing_page_overview(challenge_id):
    return challenge_landing_page(challenge_id)

@route("/challenge/<challenge_id:int>/copy/<copy_challenge_id:int>")
def challenge_landing_page_overview(challenge_id,copy_challenge_id):
    return challenge_landing_page(challenge_id, copy_challenge_id)

def span(pred, seq):
    for i in range(len(seq)):
        if not pred(seq[i]):
            return (seq[:i], seq[i:])
    return (seq, [])

def compute_solved_over_time_graph(timestamps, max_label_count=15):
    if len(timestamps) == 0:
        return []

    # Calculate the time span of the graph.
    now = datetime.datetime.now()
    first = timestamps[0]
    avg_interval = (now - first) // len(timestamps)
    starting_point = first - avg_interval
    timespan = now - starting_point

    # Decide on the time stamps that will divide the x-axis.
    label_count = min(max_label_count, 1+len(timestamps)+1, timespan.days+1)
    label_interval = timespan // (label_count - 1)
    labels = [starting_point + (i * label_interval) \
              for i in range(label_count)]

    # Count the number of completions in every interval.
    counts = [0 for i in range(len(labels))]
    for i in range(1,len(labels)):
        before, after = span(lambda ts: ts <= labels[i], timestamps)
        counts[i] = len(before)
        timestamps = after

    # Convert labels to strings, drop precision beyond days.
    labels = [l[:l.index(' ')] for l in map(str,labels)]

    return zip(labels, counts)
