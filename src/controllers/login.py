from bottle import route, template, request, response, redirect
from database import db
from lang import translate

@route("/login")
def login():
    return template("login")

@route("/login", method="POST")
def login():
    username = request.forms.get("username")
    password = request.forms.get("password")
    origin   = request.forms.get("origin")

    ut = db.users_table()
    user = ut.get_by_name(username)

    # user exists and passwords match
    if user and user.password == password:
        session = request.session
        session["logged_in"] = True
        session["user_id"]   = user.id
        session["user_name"] = user.name
        redirect("/")
    # user tried to login from the home page, redirect to signup
    elif origin == "home":
        redirect("/signup")
    # user was not found
    elif not user:
        return template("login", errormsg = translate("login.error.username"))
    # password was incorrect
    else:
        return template("login", errormsg = translate("login.error.password"))
