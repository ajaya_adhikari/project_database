from bottle import error, route, template

@error(404)
def error404(err):
    """ Page not found error. """
    return "<h1>404 Not found</h1><h4>You've met with a terrible fate, haven't you?</h4>"

#@error(500)
#def error500(err):
#    """ Internal server error. Typical reason is an uncaught exception. """
#    return "<p>Internal server error</p>"
