from database import db
import math

neighbours_per_series = 100
max_rating = 5
min_rating = 1

def recommend_series(user_id, number_series):
    """Return k recommended series_id's for the given user. k=number_series"""

    ratings_table = db.ratings_table()
    series_neighbours_table = db.series_neighbours_table()

    #Get a dictionary(series_id's -> score) that the given user has rated
    dict_rated_series = ratings_table.get_rated_series(user_id)
    rated_series_ids = set(dict_rated_series.keys())

    #similar series of the series rated by the given user
    similar_series_rated = set( reduce(lambda x,y: x+y, \
                            map(lambda x : series_neighbours_table.get_neighbours(x).keys(), rated_series_ids), []) ) - rated_series_ids


    #now predict the rating of each series in similar_series_rated
    predictions = []
    for serie_id in similar_series_rated:
        #Get a dictionary(neighbours_id -> similarity) of similar series
        dict_similar_series = series_neighbours_table.get_neighbours(serie_id)
        similar_series_ids = set( dict_similar_series.keys() )

        #intersect silmilar_series_ids and rated_series_ids
        common_series = similar_series_ids.intersection(rated_series_ids)

        numerator = sum(map(lambda series_id: dict_rated_series[series_id]* dict_similar_series[series_id], common_series))
        denominator = sum( map(lambda series_id: dict_similar_series[series_id], common_series) )
        predicted_rating = numerator/denominator

        predictions.append([serie_id,predicted_rating])

    predictions.sort(key =lambda x: predicted_rating)
    predictions.reverse()

    if len(predictions) >= number_series:
        return [x[0] for x in predictions[:number_series]]

    #else append other series
    exercise_series_table = db.exercise_series_table()
    all_series_ids = exercise_series_table.get_all_series_ids()
    recommendation = [x[0] for x in predictions] + all_series_ids

    #remove duplicates from recommendation
    recommendation_no_dup = []
    [recommendation_no_dup.append(i) for i in recommendation if not recommendation_no_dup.count(i)]

    return recommendation_no_dup[0 : min(number_series , len(recommendation_no_dup)) ]



def set_series_neighbours():
    """Insert the k most similar neighbours for each series in the database. k = neighbours_per_series"""
    series_ids = db.exercise_series_table().get_all_series_ids()
    ratings_table = db.ratings_table()
    series_neighbour_table = db.series_neighbours_table()

    #Delete all rows of SeriesNeighbours
    series_neighbour_table.delete_all_rows()

    for series_id in series_ids:
        #similarities wil contain tuples of (neighbour,similarity)
        similarities = []
        for neighbour in series_ids:
            if neighbour == series_id:
                continue

            #get ratings of series_id and neighbour that has been rated by common users
            ratings = ratings_table.get_common_user_ratings(series_id,neighbour)
            rating1 = map(lambda x : x[0] ,ratings)         #list of rating of series_id
            rating2 = map(lambda x : x[1] ,ratings)         #list of rating of neighbour

            similarity = get_similarity(rating1, rating2)
            if similarity != 0:
                similarities.append((neighbour,similarity))

        similarities.sort(key = lambda x: x[1])

        for index in range(min(neighbours_per_series,len(similarities))):
            series_neighbour_table.insert(series_id, similarities[index][0], similarities[index][1])



def get_similarity(rating1, rating2):
    """Calculate the similarity between rating1 and rating2
    bij using euclidean_distance. The similarity is between 0 and 1"""

    if len(rating1) == 0:
        return 0

    euclidean_distance = math.sqrt(sum( map(lambda x,y: (x-y)**2, rating1, rating2)))
    maximum_distance = math.sqrt( len(rating1)* ((max_rating-min_rating)**2) )

    similarity = 1 - euclidean_distance/maximum_distance

    return similarity
