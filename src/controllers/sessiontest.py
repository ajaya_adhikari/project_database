from bottle import route, request

@route("/sessiontest")
def sessiontest():
    s = request.session
    s["visit_count"] = s.get("visit_count", 0) + 1
    return "You have visited this page %s time(s)" % s["visit_count"]
