from bottle import route, template, request, redirect

@route("/logout")
def logout():
    request.session.invalidate()
    redirect("/")
