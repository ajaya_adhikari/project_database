from bottle import route, template, request
from database import db
from datetime import datetime
from logic.browseformat import setbrowsedata
import math

# Use regex to match the sorting and filtering from url
@route( "/browse/"
        "<lang:re:en|nl>/"
        "<prog:re:all|python|cpp|rust>/"
        "<difficulty:re:any|easy|medium|hard>/"
        "<sort_by:re:date|title|popularity>/"
        "<direction:re:asc|desc>/"
        "<page:int>")
def browse_page(lang,
                prog,
                difficulty,
                sort_by,
                direction,
                page,
                challenge_id=None):
    # Amount of challenges listed per page
    per_page = 5

    # Get the series table
    est = db.exercise_series_table()

    # Sort the challenge list in a particular fashion
    order = True
    if direction == "desc":
        order = False
    sort = []
    if sort_by == "date":
        order = not order # We store a date and show user a date difference
        sort.append(('time',order))
    elif sort_by == "title":
        sort.append(('name',order))
    elif sort_by == "popularity":
        sort.append(('tempered_rating',order))

    # Choose which challenges are displayed
    select = [('visible',True)]
    select.append(('language',lang))

    # Display only the chosen programming language
    if prog != "all":
        select.append(('programming_language',prog))

    # Convert difficulty string to integer
    difficulty_int = 1
    if difficulty != "any":
        if difficulty == "easy":
             difficulty_int = 1
        elif difficulty == "medium":
             difficulty_int = 2
        elif difficulty == "hard":
             difficulty_int = 3
        select.append(('difficulty',difficulty_int))

    # Calculate how many pages are required
    amount = est.get_filtered_amount(select)
    pages =  int(math.ceil(float(amount)/float(per_page)))

    # Don't allow negative or out of reach pages
    if page < 1:
        page = 1
    elif page != 1 and page > pages:
        page = pages

    # Get the challenges
    challenges = est.get_sorted_slice(sort,
                                      select,
                                      per_page,per_page*(page-1))

    # Edit the data in challenges to human readable text
    setbrowsedata(challenges)

    return template("browse",
                    challenges=challenges,
                    pages=pages,
                    currentPage=page,
                    prog=prog,
                    lang=lang,
                    difficulty=difficulty,
                    sort_by=sort_by,
                    order=direction,
                    challenge_id=challenge_id)

@route("/browse")
def browse_simple():
    return browse_page("en","all","any","date","asc",1)

# Browse exercises to add to your own challenge
@route( "/browse/"
        "<challenge_id:int>/"
        "<lang:re:en|nl>/"
        "<prog:re:all|python|cpp|rust>/"
        "<difficulty:re:any|easy|medium|hard>/"
        "<sort_by:re:date|title|popularity>/"
        "<direction:re:asc|desc>/"
        "<page:int>")
def browse_page_challenge(challenge_id,lang,prog,difficulty,sort_by,direction,page):
    # ref = request.headers.get('Referer') # probably won't need this
    return browse_page(lang,prog,difficulty,sort_by,direction,page, challenge_id)

# Browse exercises to add to your own challenge
@route( "/browse/<challenge_id:int>")
def browse_page_challenge_simple(challenge_id):
    est = db.exercise_series_table()
    prog_lang = est.get_by_id(challenge_id).programming_language
    return browse_page_challenge(challenge_id,"en",prog_lang,"any","date","asc",1)
