class Solution:
    def __init__(self, exercise_id, user_id, solution, correct, 
                 attempt_count, time):
        self.exercise_id = exercise_id
        self.user_id = user_id
        self.solution = solution
        self.correct = correct
        self.attempt_count = attempt_count
        self.time = time
