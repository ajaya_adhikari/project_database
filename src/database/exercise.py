import exercise_series

class Exercise:
    def __init__(self, id_, author_id, title, contents, time):
        self.id = id_
        self.author_id = author_id
        self.title = title
        self.contents = contents
        self.time = time
