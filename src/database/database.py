import psycopg2
import tables
import time

class Database:
    """Database manager class for the Panjandrum project."""
    def __init__(self, dbname, user, password):
        self.conn = psycopg2.connect("dbname={} user={} password={}".format(dbname,user,password))

    def users_table(self):
        return tables.UsersTable(self.conn.cursor())

    def exercises_table(self):
        return tables.ExerciseTable(self.conn.cursor())

    def exercise_series_table(self):
        return tables.ExerciseSeriesTable(self.conn.cursor())

    def exercises_in_series_table(self):
        return tables.ExercisesInSeriesTable(self.conn.cursor())

    def tag_table(self):
        return tables.TagTable(self.conn.cursor())

    def solutions_table(self):
        return tables.SolutionsTable(self.conn.cursor())

    def ratings_table(self):
        return tables.RatingsTable(self.conn.cursor())

    def friends_table(self):
        return tables.FriendsTable(self.conn.cursor())

# not used anymore because we use subscribers
#    def friendrequests_table(self):
#        return tables.FriendRequestsTable(self.conn.cursor())

    def series_neighbours_table(self):
        return tables.SeriesNeighbours(self.conn.cursor())
