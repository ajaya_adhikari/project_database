import exercise

class ExerciseSeries:
    def __init__(self, id_, name, author_id, description, difficulty,
                 programming_language, language, visible,
                 times_made=0, avg_rating=0, rating_count=0, time='now',
                 tempered_rating=0, credits=0):
        self.id = id_
        self.name = name
        self.author_id = author_id
        self.description = description
        self.difficulty = difficulty
        self.programming_language = programming_language
        self.language = language
        self.time = time
        self.visible = visible
        self.times_made = times_made
        self.avg_rating = avg_rating
        self.rating_count = rating_count
        self.tempered_rating = tempered_rating
        self.tags = []
        self.credits = credits
