import database
import user
import exercise_series
import exercise
import friends
import time
import datetime
import random

class Table:
    def __init__(self, cur):
        self.cur = cur

    def execute(self, q, replacements=[],
                commit=True, rollback_on_error=True):
        """Execute an SQL query."""
        try:
            self.cur.execute(q, replacements)
            if commit:
                self.cur.connection.commit()
            try:
                result = self.cur.fetchall()
            except:
                result = None
        except database.psycopg2.Error as e:
            if rollback_on_error:
                print('Error encountered, rolling back...')
                self.cur.connection.rollback()
            raise e
        return result


class UsersTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_by_id(self, userid):
        """Returns single User or None."""
        q = """SELECT *
            FROM Users
            WHERE id = %s;"""

        result = list_to_maybe(self.execute(q, (userid,)))
        if result == None:
            return None
        else:
            return user.User(*result)

    def get_by_name(self, username):
        """Returns single User or None."""
        q = """SELECT *
            FROM Users
            WHERE name = %s;"""

        result = list_to_maybe(self.execute(q, (username,)))
        if result == None:
            return None
        else:
            return user.User(*result)

    def get_friends_recent_activities(self, userid):
        """Get the recent activities of the friends of a user"""
        q = """SELECT description, name, time, superquery.user
            FROM
                ((SELECT 'profile' AS description, '' AS name, u.time AS time, u.name AS user
                    FROM Friends as f, users as u
                    WHERE f.id1 = %s and f.id2 = u.id
                    ORDER BY u.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'challenge' AS description, ExerciseSeries.name AS name, ExerciseSeries.time AS time, u.name AS user
                    FROM ExerciseSeries, Users as u
                    WHERE u.id in
                                (SELECT f.id2
                                FROM friends as f
                                WHERE f.id1 = %s)
                            and ExerciseSeries.author_id = u.id
                    ORDER BY ExerciseSeries.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'exercise' AS description, Exercises.title AS name, Exercises.time AS time, u.name AS user
                    FROM ExerciseSeries, Exercises, ExercisesInSeries, Users as u
                    WHERE u.id in (SELECT f.id2
                            FROM friends as f
                            Where f.id1 = %s)
                        and ExerciseSeries.author_id = u.id and ExercisesInSeries.series_id = ExerciseSeries.id
                    ORDER BY Exercises.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'solution' AS description, Exercises.title AS name, Solutions.time AS time, u.name AS user
                    FROM Exercises, Solutions, Users as u
                    WHERE u.id in
                                (SELECT f.id2
                                FROM friends as f
                                WHERE f.id1 = %s)
                        and Solutions.user_id = u.id and Solutions.exercise_id = Exercises.id and Solutions.correct = TRUE
                    ORDER BY Solutions.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'rate' AS description, ExerciseSeries.name AS name, Ratings.time AS time, u.name AS user
                    FROM ExerciseSeries, Ratings, Users as u
                    WHERE u.id in (SELECT f.id2
                            FROM friends as f
                            WHERE f.id1 = %s)
                        and Ratings.user_id = u.id and Ratings.series_id = ExerciseSeries.id
                    ORDER BY Ratings.time DESC
                    LIMIT 5)

                UNION

                (SELECT DISTINCT 'friend' AS description, u1.name AS name, f.time AS time, u2.name AS user
                        FROM users as u1, friends as f, users as u2
                        WHERE f.id1 in (
                                SELECT f2.id2
                                FROM Friends as f2
                                WHERE f2.id1 = %s)
                            and f.id2 = u1.id and f.id1 = u2.id
                        LIMIT 5)
                ) AS superquery
            ORDER BY time DESC
            LIMIT 5;
            """
        result = self.execute(q, (userid,userid,userid,userid,userid,userid) )
        print result
        return result

    def get_own_recent_activities(self, userid):
        """Get your most recent activities of:
          user
          exerciseSeries
          exercises
          solutions
          rating
          friends
          groups - nog niet in query
          groupmember - nog niet in query"""
        q = """SELECT description, name, time
            FROM
                ((SELECT 'profile' AS description, '' AS name, Users.time AS time
                    FROM Users
                    WHERE Users.id = %s
                    ORDER BY Users.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'challenge' AS description, ExerciseSeries.name AS name, ExerciseSeries.time AS time
                    FROM ExerciseSeries, Users
                    WHERE Users.id = %s and ExerciseSeries.author_id = Users.id
                    ORDER BY ExerciseSeries.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'exercise' AS description, Exercises.title AS name, Exercises.time AS time
                    FROM ExerciseSeries, Exercises, ExercisesInSeries, Users
                    WHERE Users.id = %s and ExerciseSeries.author_id = Users.id and ExercisesInSeries.series_id = ExerciseSeries.id
                    ORDER BY Exercises.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'solution' AS description, Exercises.title AS name, Solutions.time AS time
                    FROM Exercises, Solutions, Users
                    WHERE Users.id = %s and Solutions.user_id = Users.id and Solutions.exercise_id = Exercises.id and Solutions.correct = TRUE
                    ORDER BY Solutions.time DESC
                    LIMIT 5)

                UNION

                (SELECT 'rate' AS description, ExerciseSeries.name AS name, Ratings.time AS time
                    FROM ExerciseSeries, Ratings, Users
                    WHERE Users.id = %s and Ratings.user_id = Users.id and Ratings.series_id = ExerciseSeries.id
                    ORDER BY Ratings.time DESC
                    LIMIT 5)

                UNION

                (SELECT DISTINCT 'friend' AS description, Users.name AS name, Friends.time AS time
                        FROM Users, Friends
                        Where (Users, Friends) in
                            (SELECT u2, Friends
                            FROM Friends, Users AS u1, Users AS u2
                            WHERE %s = u1.id and u1.id = Friends.id1 and u2.id = Friends.id2
                            ORDER BY Friends.time DESC
                            LIMIT 5)

                )) AS superquery
            ORDER BY time DESC
            LIMIT 5;
            """
        #groups & groupmembers nog niet ge-implementeerd!!!!!!!!
        result = self.execute(q, (userid,userid,userid,userid,userid,userid) )
        return result

    def insert(self, name, password, time='now', credits=0):
        q = """INSERT INTO Users (name, password, time, credits)
            VALUES (%s, %s, %s, %s) RETURNING id, time, credits;"""
        [(id_,time, credits)] = self.execute(q, (name, password, time, credits))
        return user.User(id_, name, password, time, credits)

    def delete(self, user):
        q = """DELETE FROM Users WHERE id = %s;"""
        self.execute(q, (get_id(user),))

    def update_entry(self, user):
        q = """UPDATE Users SET password = %s
            WHERE id = %s;"""
        return self.execute(q, (user.password, get_id(user)))

    def search(self, query):
        q = """SELECT *
               FROM Users
               WHERE lower(name) LIKE %s
               LIMIT 5;
            """
        result = self.execute(q, ['%' + query.lower() + '%'])
        return [user.User(*r) for r in result]


class ExerciseTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_by_id(self, id_):
        """Return single Exercise or None."""
        q = """SELECT *
            FROM Exercises
            WHERE id = %s;"""
        result = self.execute(q, (id_,))
        if result == []:
            return None
        else:
            result = result[0]
            return exercise.Exercise(*result)

    def get_by_series_and_num(self, series, num):
        eist = ExercisesInSeriesTable(self.cur)
        return eist.get_exercise_by_series_id_and_num(series, num)

    def get_by_series(self, series):
        """Return the list of all exercises associated with a series."""
        eist = ExercisesInSeriesTable(self.cur)
        return eist.get_exercises_in_series(series)

    def get_by_series_id(self, series_id):
        """Return the list of all exercises associated with a series."""
        eist = ExercisesInSeriesTable(self.cur)
        return eist.get_exercises_by_series_id(series_id)

    def get_by_author(self, user):
        q = """SELECT *
            FROM Exercises E
            WHERE author_id = %s;"""
        result = self.execute(q, (get_id(user),))
        return [exercise.Exercise(*r) for r in result]

    def get_nr_by_author(self, user):
        """Returns the number of exercises created by a certain user."""
        q = """SELECT COUNT(*)
            FROM Exercises
            WHERE Exercises.author_id = %s"""
        result = self.execute(q, (get_id(user), ))
        return int(result[0][0])

    def insert(self, author, title, contents):
        """Insert a new exercise table entry."""
        author_id = get_id(author)
        q = """INSERT INTO Exercises (author_id, title, contents)
            VALUES (%s, %s, %s) RETURNING id, time;"""
        [(id_, time)] = self.execute(q, (author_id, title, contents))
        return exercise.Exercise(id_, author_id, title, contents, time)

    def copy_contents(self, exercise, new_author=None, new_title=None):
        """Insert an identical (except for the id) copy of an exercise.
        If new_author or new_title are None, the respective value of the old
        exercise is used."""
        ex = self.get_by_id(get_id(exercise))
        if new_author == None:
            new_author = ex.author
        if new_title == None:
            new_title = ex.title
        return self.insert(get_id(new_author), new_title, ex.contents)

    def delete(self, ex):
        q = """DELETE FROM Exercises
            WHERE id = %s;"""
        self.execute(q, (get_id(ex),))

    def update_entry(self, ex):
        q = """UPDATE Exercises SET title = %s, contents = %s
            WHERE id = %s;"""
        self.execute(q, (ex.title, ex.contents, get_id(ex)))

class ExerciseSeriesTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_all_series_ids(self):
        q = """SELECT id FROM ExerciseSeries;"""
        return [x[0] for x in self.execute(q)]

    def get_amount(self):
        q = """SELECT count(*) FROM ExerciseSeries;"""
        result = self.execute(q)
        if result == []:
            return None
        else:
            return result[0][0]

    def get_filtered_amount(self, filters):
        where_clause = filters_to_where_clause(filters)
        filter_replacements = filters_to_replacements(filters)
        q = """SELECT COUNT(*) FROM ExerciseSeries
            """ + where_clause + ';'
        result = self.execute(q, filter_replacements)
        return result[0][0]

    def get_sorted_slice(self, sorting_instructions, filters,
                         limit, offset, base_average=3.0, dummy_voter_count=10):
        """Returns a slice of the list of ExerciseSeries sorted
        lexicographically according to the sorting_instructions parameter,
        which is of the form [(column_name,ascending),...], where
        column_name is a string that specifies the name of a column and
        ascending is a boolean (True -> sort ascending; False ->
        sort descending). The results are filtered according to the
        filters parameter which is of the form [(column_name, value),...].
        Parameters should not contain user input.
        sorting_instructions must contain at least one tuple.
        """
        def instruction_to_string(ins):
            column_name, ascending = ins
            if ascending:
                return column_name + ' ASC'
            else:
                return column_name + ' DESC'

        order_by_clause = 'ORDER BY ' + \
                          reduce(lambda s1,s2: s1 + ', ' + s2,
                                 map(instruction_to_string,
                                     sorting_instructions)) + '\n'
        where_clause = filters_to_where_clause(filters)
        filter_replacements = filters_to_replacements(filters)

        q = """SELECT *, COALESCE( (SELECT (SUM(R.score)+(%s*%s)) / (COUNT(R.user_id)+%s)
                                    FROM Ratings R WHERE id = R.series_id
                                   ), %s )
                         AS tempered_rating
            FROM ExerciseSeries
            """ + where_clause + order_by_clause + \
            """LIMIT %s OFFSET %s;"""
        result = self.execute(q, [base_average, dummy_voter_count,
                                  dummy_voter_count, base_average] + \
                                  filter_replacements + [limit, offset])
        return [exercise_series.ExerciseSeries(*r) for r in result]

    def get_tempered_rating(self, series, base_average=3.0, dummy_voter_count=10):
        q = """SELECT COALESCE((SUM(score)+(%s*%s)) / (COUNT(user_id)+%s), %s)
               FROM Ratings WHERE series_id = %s;"""
        [(tempered_rating,)] = self.execute(q, [base_average, dummy_voter_count,
                                            dummy_voter_count, base_average,
                                            get_id(series)])
        return tempered_rating

    def get_from_to_sorted_by_name(self, limit, offset, ascending=True):
        q = """SELECT * FROM ExerciseSeries
            ORDER BY name %s LIMIT %s OFFSET %s;"""
        sort = "ASC"
        if not ascending:
            sort = "DESC"
        result = self.execute(q, (sort, limit, offset))
        return [exercise_series.ExerciseSeries(*r) for r in result]

    def get_all(self):
        q = """SELECT * FROM ExerciseSeries;"""
        result = self.execute(q)
        return [exercise_series.ExerciseSeries(*r) for r in result]

    def get_by_id(self, id_):
        q = """SELECT * FROM ExerciseSeries WHERE id = %s;"""
        result = self.execute(q,(id_,))
        if result == []:
            return None
        else:
            result = result[0]
            return exercise_series.ExerciseSeries(*result)

    def get_by_author_and_name(self, user, name):
        """Return single ExerciseSeries or None."""
        q = """SELECT * FROM ExerciseSeries
            WHERE author_id = %s AND name = %s;"""
        result = self.execute(q,(get_id(user), name))
        if result == []:
            return None
        else:
            result = result[0]
            return exercise_series.ExerciseSeries(*result)

    def is_unique_author_name_pair(self, user, name):
        q = """SELECT * FROM ExerciseSeries
            WHERE author_id = %s AND name = %s;"""
        result = self.execute(q, (get_id(user), name))
        if result:
            return False
        else:
            return True

    def get_by_author(self, user):
        """Return the list of all ExerciseSeries created by the
        specified author."""
        q = """SELECT * FROM ExerciseSeries WHERE author_id = %s;"""
        result = self.execute(q, (get_id(user),))
        return [exercise_series.ExerciseSeries(*r) for r in result]

    def search(self, query):
        q = """SELECT DISTINCT S.*
               FROM ExerciseSeries S
               LEFT OUTER JOIN Users U ON S.author_id = U.id
               LEFT OUTER JOIN Tags  T ON S.id = T.series_id
               WHERE
                   S.visible = True
                   AND (lower(S.name) LIKE %s
                       OR lower(U.name) LIKE %s
                       OR lower(S.language) LIKE %s
                       OR lower(S.programming_language) LIKE %s
                       OR lower(T.tag) LIKE %s
                       )
               LIMIT 5;
            """
        result = self.execute(q, 5*['%' + query.lower() + '%'])
        return [exercise_series.ExerciseSeries(*r) for r in result]

    def insert(self, name, user, description, difficulty,
               programming_language, language, visible, time='now', credits=0):
        """Insert new entry into table and return assigned id."""
        q = """INSERT INTO ExerciseSeries
            (name, author_id, description, difficulty,
             programming_language, language, visible,
             times_made, avg_rating, rating_count, time, credits)
            VALUES (%s,%s,%s,%s,%s,%s,%s,0,0,0,%s,%s)
            RETURNING id;"""
        [(id_,)] = self.execute(q, (name,get_id(user),description,difficulty,
                                    programming_language, language,
                                    visible, time, credits))
        return exercise_series.ExerciseSeries(id_,name,get_id(user),description,
                                              difficulty,
                                              programming_language,
                                              language,visible,time=time, credits=credits)

    def delete(self, series):
        """Delete an exercise series and all exercises associated with it."""
        q = """DELETE FROM ExerciseSeries
            WHERE id = %s;"""
        self.execute(q, (get_id(series),))

    def update_rating(self, series):
        """Updates the avg_rating and rating_count values of a single
        series, based on the values currently stored in the ratings table."""
        rt = RatingsTable(self.cur)
        new_rating, new_count = rt.compute_rating(series)
        q = """UPDATE ExerciseSeries SET avg_rating = %s, rating_count = %s
            WHERE id = %s;"""
        self.execute(q, (new_rating, new_count, get_id(series)))

    def update_entry(self, series):
        q = """UPDATE ExerciseSeries
            SET name=%s,description=%s,difficulty=%s,
                programming_language=%s,language=%s,time=%s,visible=%s,
                times_made=%s,avg_rating=%s,
                rating_count=%s
            WHERE id=%s;"""
        self.execute(q, (series.name, series.description, series.difficulty,
                         series.programming_language,
                         series.language, series.time, series.visible,
                         series.times_made,
                         series.avg_rating, series.rating_count, get_id(series)))

class ExercisesInSeriesTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_exercises_in_series(self, series):
        return self.get_exercises_by_series_id(get_id(series))

    def get_exercises_by_series_id(self, series_id):
        q = """SELECT E.*
            FROM Exercises E, ExercisesInSeries S
            WHERE S.series_id = %s AND S.exercise_id = E.id
            ORDER BY S.num ASC;"""
        result = self.execute(q, (series_id,))
        return [exercise.Exercise(*r) for r in result]

    def get_exercise_by_series_id_and_num(self, series, num):
        if type(series) is not int:
            series = get_id(series)

        q = """SELECT E.*
            FROM Exercises E, ExercisesInSeries S
            WHERE S.series_id = %s AND S.exercise_id = E.id
                AND S.num = %s;"""
        result = self.execute(q, (get_id(series), num))
        if result:
            return exercise.Exercise(*result[0])
        else:
            return None

    def get_num(self, series, exercise):
        q = """SELECT num FROM ExercisesInSeries
            WHERE series_id = %s AND exercise_id = %s;"""
        [(num,)] = self.execute(q, (get_id(series), get_id(exercise)))
        return num

    def count_exercises_in_series(self, series):
        q = """SELECT COUNT(*) FROM ExercisesInSeries
            WHERE series_id = %s;"""
        [(count,)] = self.execute(q, (get_id(series),))
        return count

    def get_series_containing_exercise(self, exercise):
        q = """SELECT S.*
            FROM ExerciseSeries S, ExercisesInSeries EiS
            WHERE EiS.exercise_id = %s AND EiS.series_id = S.id;"""
        result = self.execute(q, (get_id(exercise),))
        return [exercise_series.ExerciseSeries(*r) for r in result]

    def insert_exercise_in_series(self, series_id, exercise_id, num=None):
        """Create a reference between a series and an exercise. If num is not
        specified, the exercise is appended as the last exercise in the series.
        Otherwise, it is inserted at the num'th position and the other
        exercises are shifted accordingly.

        Returns num, ie the index in the exercise series (1-indexed).
        """
        if num: # Shift all exercises after the inserted exercise to the right.
            q = """UPDATE ExercisesInSeries SET num = num + 1
                WHERE series_id = %s AND num >= %s;"""
            self.execute(q, (series_id, num), commit=False)
        else:
            # Append exercise as the last exercise in the series.
            q = """SELECT COUNT(*) FROM ExercisesInSeries
                WHERE series_id = %s;"""
            [(exercise_count,)] = self.execute(q, (series_id,), commit=False)
            num = exercise_count + 1

        # Create a reference between the series and the exercise.
        q = """INSERT INTO ExercisesInSeries
            VALUES (%s, %s, %s);"""
        self.execute(q, (series_id, exercise_id, num), commit=True)
        return num

    def insert_exercise_copy_in_series(self, series_id, exercise_id):
        et = ExerciseTable(self.cur)
        est = ExerciseSeriesTable(self.cur)
        series_author_id = est.get_by_id(series_id).author_id

        new_ex = et.copy_contents(exercise_id, new_author=series_author_id)
        return self.insert_exercise_in_series(series_id, new_ex.id)

    def remove_exercise_from_series(self, series_id, exercise_id):
        q = """DELETE FROM ExercisesInSeries
            WHERE series_id = %s AND exercise_id = %s RETURNING num;"""
        [(num,)] = self.execute(q, (series_id, exercise_id), commit=False)
        q = """UPDATE ExercisesInSeries SET num = num - 1
            WHERE series_id = %s AND num > %s;"""
        self.execute(q, (series_id, num), commit=True)
        if self.count_exercise_references(exercise_id) == 0:
            ExerciseTable(self.cur).delete(exercise_id)

    def remove_exercise_from_series_by_index(self, series_id, index):
        q = """DELETE FROM ExercisesInSeries
            WHERE series_id = %s AND num = %s
            RETURNING exercise_id;"""
        [(exercise_id,)] = self.execute(q, (series_id, index), commit=False)
        q = """UPDATE ExercisesInSeries SET num = num - 1
            WHERE series_id = %s AND num > %s;"""
        self.execute(q, (series_id, index), commit=True)
        if self.count_exercise_references(exercise_id) == 0:
            ExerciseTable(self.cur).delete(exercise_id)

    def count_exercise_references(self, exercise):
        q = """SELECT COUNT(*) FROM ExercisesInSeries
            WHERE exercise_id = %s;"""
        [(refcount,)] = self.execute(q, (get_id(exercise),))
        return refcount

class TagTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_by_series(self, exs):
        q = """SELECT tag FROM Tags WHERE series_id = %s;"""
        result = self.execute(q, (get_id(exs),))
        return [r for (r,) in result]

    def delete(self, exs, tag):
        q = """DELETE FROM Tags
            WHERE series_id = %s AND tag = %s;"""
        self.execute(q, (get_id(exs), tag))

    def insert(self, exs, tag):
        q = """INSERT INTO Tags VALUES (%s,%s);"""
        self.execute(q, (get_id(exs), tag))

class SolutionsTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_solution(self, exercise, user):
        q = """SELECT solution
            FROM Solutions
            WHERE exercise_id = %s AND user_id = %s;"""
        solution = self.execute(q, (get_id(exercise), get_id(user)))
        return solution[0][0]

    def get_attempt_count(self, exercise, user):
        q = """SELECT attempt_count
            FROM Solutions
            WHERE exercise_id = %s AND user_id = %s;"""
        solution = self.execute(q, (get_id(exercise), get_id(user)))
        return solution[0][0]

    def insert_empty(self, exercise, user):
        q = """INSERT INTO Solutions
                (exercise_id,user_id,solution,correct,attempt_count,time)
            VALUES (%s, %s, '', FALSE, 0, 'now');"""
        self.execute(q, (get_id(exercise), get_id(user)))

    def check_exists(self, exercise, user):
        q = """SELECT * FROM Solutions
            WHERE exercise_id = %s AND user_id = %s;"""
        return bool(self.execute(q, (get_id(exercise), get_id(user))))

    def register_attempt(self, exercise, user, solution, correct,time='now'):

        # Prevent a correct solution from being
        # overwritten by an incorrect one.
        q = """SELECT correct FROM Solutions
            WHERE exercise_id = %s AND user_id = %s;"""
        [(was_correct,)] = self.execute(q, (get_id(exercise), get_id(user)))

        q = """UPDATE Solutions
            SET solution = %s, correct = %s, time = %s {}
            WHERE exercise_id = %s AND user_id = %s;"""
        # Only increment attempt counter when the exercise hasn't been
        # correctly solved before.
        if was_correct:
            q = q.format("")
        else:
            q = q.format(", attempt_count = attempt_count + 1")

        # Update solutions table entry.
        self.execute(q, (solution, correct or was_correct,time, get_id(exercise), get_id(user)))

        # Update the times_made variable in the exercise series
        # table if necessary.
        if not was_correct and correct:
            # Check whether all exercises in the series this exercise occurs in
            # were solved correctly by this user.
            eist = ExercisesInSeriesTable(self.cur)
            for series in eist.get_series_containing_exercise(exercise):
                if self.user_has_solved_series(user, series):
                    q = """UPDATE ExerciseSeries SET times_made = times_made + 1
                        WHERE id = %s;"""
                    self.execute(q, (get_id(series),))
        return was_correct

    def get_by_exercise_and_user(self, exercise, user):
        q = """SELECT * FROM Solutions
            WHERE exercise_id = %s AND user_id = %s;"""
        result = self.execute(q, (get_id(exercise), get_id(user)))
        return [solution.Solution(*r) for r in result]

    def user_has_solved_series(self, user, series):
        # Count the number of correctly solved exercises.
        q = """SELECT COUNT(*)
            FROM Solutions, ExercisesInSeries
            WHERE ExercisesInSeries.series_id = %s
                AND ExercisesInSeries.exercise_id = Solutions.exercise_id
                AND Solutions.user_id = %s
                AND correct = TRUE;"""
        [(solved_count,)] = self.execute(q, (get_id(series), get_id(user)))

        # Get total number of exercises in series.
        eist = ExercisesInSeriesTable(self.cur)
        total_exercises = len(eist.get_exercises_by_series_id(get_id(series)))

        return solved_count == total_exercises

    def avg_attempts_per_exercise(self, exercise):
        q = """SELECT AVG(attempt_count)
            FROM Solutions WHERE exercise_id = %s;"""
        [(avg_attempt_count,)] = self.execute(q, (get_id(exercise),))
        return avg_attempt_count

    def count_users_attempted_to_solve_series(self, series):
        """Return the number of users who attempted to solve a series,
        regardless of whether they were successful. An attempt is defined as
        having submitted at least one solution (possibly incorrect) to at least
        one exercise in the series."""
        q = """SELECT COUNT(DISTINCT user_id)
            FROM ExercisesInSeries NATURAL JOIN Solutions
            WHERE series_id = %s AND attempt_count > 0;"""
        [(attempts,)] = self.execute(q, (get_id(series),))
        return attempts

    def count_users_solved_series(self, series):
        """Return the number of users who successfully solved series."""
        q = """SELECT COUNT(*)
            FROM (SELECT user_id, COUNT(exercise_id) as c, MAX(Solutions.time) as t
                  FROM Solutions NATURAL JOIN ExercisesInSeries
                  WHERE series_id = %s GROUP BY user_id) as ExercisesMade
            WHERE ExercisesMade.c = (SELECT COUNT(exercise_id)
                                     FROM ExercisesInSeries
                                     WHERE series_id = %s);"""
        [(count,)] = self.execute(q, 2*[get_id(series)])
        return count


    def has_solved(self, user, exercise):
        q = """SELECT * FROM Solutions
            WHERE user_id = %s AND exercise_id = %s AND correct = TRUE;"""
        return bool(self.execute(q, (get_id(user), get_id(exercise))))

    def series_solved_over_time(self, series):
        q = """SELECT ExercisesMade.t
            FROM (SELECT user_id, COUNT(exercise_id) as c, MAX(Solutions.time) as t
                  FROM Solutions NATURAL JOIN ExercisesInSeries
                  WHERE series_id = %s AND correct = TRUE
                  GROUP BY user_id
                  ) as ExercisesMade
            WHERE ExercisesMade.c = (SELECT COUNT(exercise_id)
                                     FROM ExercisesInSeries
                                     WHERE series_id = %s)
            ORDER BY ExercisesMade.t ASC;"""
        result = self.execute(q, (get_id(series), get_id(series)))
        return [time for (time,) in result]

    def get_total_nr_exercises_solved_by_user(self, user):
        """Returns the number of exercises correctly solved by a certain user."""
        q = """SELECT COUNT(*)
            FROM Solutions
            WHERE Solutions.user_id = %s and Solutions.correct = TRUE"""
        result = self.execute(q, (get_id(user), ))
        return int(result[0][0])

class RatingsTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_rated_series(self,user):
        """Get a dictionary(series_id's -> score) that the given user has rated"""
        q = """SELECT series_id, score
                FROM Ratings
                WHERE user_id = %s;"""
        result = self.execute( q, (get_id(user),) )

        return {x[0]: x[1] for x in result}


    def update_series(self, series):
        est = ExerciseSeriesTable(self.cur)
        est.update_rating(series)

    def insert(self, series, user, score):
        q = """INSERT INTO Ratings (series_id, user_id, score)
            VALUES (%s, %s, %s);"""
        self.execute(q, (get_id(series), get_id(user), score))
        self.update_series(series)

    def insert2(self, series, user, score):
        q = """INSERT INTO Ratings (series_id, user_id, score,time)
            VALUES (%s, %s, %s,%s);"""

        now = datetime.datetime.now()
        delta = datetime.timedelta(random.randrange(10,20))
        self.execute(q, (get_id(series), get_id(user), score, str(now-delta)))


        st = SolutionsTable(self.cur)
        exercies = ExercisesInSeriesTable(self.cur).get_exercises_by_series_id(get_id(series))

        for exercise in exercies:
            if not st.check_exists(exercise, user):
                st.insert_empty(exercise, user)
                random_num = random.randrange(0,5)
                for i in range(random_num):
                    st.register_attempt(exercise, user, "", False,str(datetime.datetime.now() - datetime.timedelta(random.randrange(0,10))))
                st.register_attempt(exercise, user, "", True, str(datetime.datetime.now() - datetime.timedelta(random.randrange(0,10))))

        self.update_series(series)

    def update(self, series, user, score):
        q = """UPDATE Ratings SET score = %s
            WHERE series_id = %s AND user_id = %s;"""
        self.execute(q, (score, get_id(series), get_id(user)))
        self.update_series(series)

    def has_rated(self, series, user):
        q = """SELECT * FROM Ratings
            WHERE series_id = %s AND user_id = %s;"""
        return bool(self.execute(q, (get_id(series), get_id(user))))

    def votes_per_score(self, series):
        """Return [c1,c2,c3,c4,c5] where cX represents the number of users
        who have rated the series with X points."""

        q = """SELECT score, COUNT(*) FROM Ratings
            WHERE series_id = %s GROUP BY score;"""
        tuples = self.execute(q, (get_id(series),))
        result = 5 * [0]
        for (score, count) in tuples:
            result[int(score) - 1] = int(count)
        return result

    def compute_rating(self, series):
        """Return the avg_rating and rating_count attributes of
        the provided series by computing the arithmetic mean of all ratings.
        avg_rating is None if count is 0."""
        q = """SELECT AVG(score), COUNT(score)
            FROM Ratings R, Users U
            WHERE R.series_id = %s
                AND R.user_id = U.id;"""
        [(avg_rating, count)] = self.execute(q, (get_id(series),))
        return avg_rating, count

    def get_common_user_ratings(self, series_id, neighbours_id):
        """Return a list of ratings of series_id and neighbours_id that has been rated by a common user"""
        q = """SELECT R1.score, R2.score
            FROM Ratings R1, Ratings R2
            WHERE   R1.series_id = %s AND
                    R2.series_id = %s AND
                    R1.user_id = R2.user_id;"""
        return self.execute(q,(series_id,neighbours_id))


class FriendsTable(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def get_subscribees(self, user1):
        """Returns all the users (ids) who are followed by user1."""
        q = """SELECT id2
                FROM Friends
                WHERE id1 = %s
            """
        return self.execute(q , (get_id(user1), ))

    def is_subscribed_to(self, user1, user2):
        q = """SELECT *
                FROM Friends
                WHERE id1 = %s and id2 =%s
            """
        result = self.execute(q , (get_id(user1), get_id(user2)))
        if len(result) == 1:
            return True
        return False


    def insert(self, user1, user2):
        #commented because not needed with subsribers
        #if get_id(user1) > get_id(user2):
            #swap because id1 is always smaller than id2 in the DB
        #    user1, user2 = user2, user1

        q = """INSERT INTO Friends (id1, id2)
            VALUES (%s, %s);"""
        self.execute(q, (get_id(user1), get_id(user2)))

    def delete(self, user1, user2):
        #commented because not needed with subsribers
        #if get_id(user1) > get_id(user2):
            #swap because id1 is always smaller than id2 in the DB
        #    user1, user2 = user2, user1

        q = """DELETE FROM Friends
            WHERE id1 = %s AND id2 = %s;"""
        self.execute(q, (get_id(user1), get_id(user2)))

class SeriesNeighbours(Table):
    def __init__(self, cur):
        Table.__init__(self, cur)

    def insert(self, series_id, neighbours_id, similarity):
        q = """INSERT INTO SeriesNeighbours (series_id, neighbours_id, similarity)
                VALUES (%s, %s, %s);"""
        self.execute(q, (series_id,neighbours_id, similarity))
    def get_all(self):
        q = """SELECT * FROM SeriesNeighbours;"""
        return self.execute(q)

    def get_neighbours(self, series):
        """Get a dictionary(neighbours_id -> similarity) of series_id"""
        q = """SELECT neighbours_id, similarity
                FROM SeriesNeighbours
                WHERE series_id = %s;"""
        result = self.execute(q, (get_id(series),))

        return {x[0]: x[1] for x in result}

    def delete_all_rows(self):
        q = """TRUNCATE TABLE SeriesNeighbours;"""
        self.execute(q)

################################################################################
# CONVENIENCE FUNCTIONS
def list_to_maybe(l):
    """Transform a list containing 0 or 1 elements into respectively
    None or the single element."""
    assert(len(l) < 2)
    if (len(l) == 0):
        return None
    else:
        return l[0]

def get_timestamp():
    return 'now'
    #return database.psycopg2.TimestampFromTicks(time.time())

def filters_to_where_clause(filters):
    if len(filters) > 0:
        filter_placeholders = [column_name + ' = %s'
                               for (column_name, _) in filters]
        return 'WHERE ' + \
               reduce(lambda s1,s2: s1 + ' AND ' + s2,
                      filter_placeholders) + '\n'
    else:
        return ''

def filters_to_replacements(filters):
    return [value for (_,value) in filters]

def get_id(obj):
    if type(obj) == int:
        return obj
    else:
        return obj.id
