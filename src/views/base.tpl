% from lang import translate
% from bottle import request
%
% language = request.session["preferences"].get("language", "en")

<!doctype html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Panjandrum</title>

    <link rel="shortcut icon" type="image/png" href="/static/img/logo_medium.png">
    <link href='http://fonts.googleapis.com/css?family=Rosario' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/static/css/pure-min.css">
    <link rel="stylesheet" href="/static/css/grids-responsive-min.css">
    <link rel="stylesheet" href="/static/css/codemirror.css">
    <link rel="stylesheet" href="/static/css/syntaxthemes/solarized.css">
    <link rel="stylesheet" href="/static/css/panjandrum.css">

    % if defined("charts"):
    <script type="text/javascript" src="/static/js/chart-min.js"></script>
    % end
    <script type="text/javascript" src="/static/js/minified-legacyie-src.js"></script>
    <script type="text/javascript" src="/static/js/codemirror-vim-min.js"></script>
    <script type="text/javascript" src="/static/js/panjandrum.js"></script>
  </head>

  <body>

    <div class="wrapper">

    <div class="header">
      <div class="header-content pure-g">

        <div class="header-logo-area pure-u-8-24">
          <a class="noeffect" href="/"><img src="/static/img/header_logo.png" /></a>
        </div>

        <div class="header-nav-area pure-u-9-24">
          <form action="/search" method="POST">
            <span style="vertical-align: middle;"><label for="txt-search"><img src="/static/img/icon_search.png" /></label><input id="txt-search" name="query" type="text" placeholder="{{translate("header.search")}}" required></span><span class="header-nav-area-link"><a href="/browse">{{translate("header.browse")}}</a></span>
          </form>
        </div>

        % if request.session.get("logged_in"):
        <div class="header-loggedin-area pure-u-7-24">
          <div class="loggedin-name">
            <a href="/profile/{{request.session.get("user_id")}}">
              <img src="/static/img/icon_person.png">
              {{request.session.get("user_name")}}
            </a>
          </div>
          <p class="loggedin-msg">
            {{translate("header.loggedin")}}<a href="/logout" class="underline">{{translate("header.logout")}}</a>.
          </p>
        </div>
        % else:
        <div class="header-login-area pure-u-7-24">
          <a href="/login" class="button-login">{{translate("header.login")}}</a>
          <span class="button-signup-bg"><a href="/signup" class="button-signup">{{translate("header.signup")}}</a></span>
        </div>
        % end

      </div>
    </div>

    <div class="middle">
        {{!base}}
    </div>

    <div class="footer">
      <div class="footer-content">
        <p class="language">{{translate("footer.language")}}</p>
        <p class="languages">
          <a href="/preferences/language/en" class="{{"language-selected" if language=="en" else ""}}">English</a> | <a href="/preferences/language/nl" class="{{"language-selected" if language=="nl" else ""}}">Nederlands</a>
        </p>
        <p class="copyright">
          <img style="margin-top: 20px;" src="/static/img/logo_medium.png" /><br/>
          Copyright &#169; 2015<br/>
          Panjandrum&#8482; Inc.
        </p>
      </div>
    </div>

  </div>

  </body>

</html>
