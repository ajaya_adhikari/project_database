%from lang import translate
<!doctype html>
<html>

  <head>
    <title>{{translate("taginfo.title")}}</title>
    <link href='http://fonts.googleapis.com/css?family=Rosario' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/static/css/panjandrum.css">
  </head>

  <body class="taginfobody">

    <p class="taginfo">{{translate("taginfo.info1")}}</p>

    <div class="tagsectioncontainer">
    <h1 class="tagsection">{{translate("taginfo.display")}}</h1> <hr/>
    <ul class="taglist">
      <li>
        <h2 class="tagheader">[large_header] {{translate("taginfo.text")}} [/large_header]</h2>
        <p class="taginfo">{{translate("taginfo.textinfo1")}}</p>
      </li>
      <li>
        <h2 class="tagheader">[small_header] {{translate("taginfo.text")}} [/small_header]</h2>
        <p class="taginfo">{{translate("taginfo.textinfo2")}}</p>
      </li>
      <li>
        <h2 class="tagheader">[highlight] {{translate("taginfo.text")}} [/highlight]</h2>
        <p class="taginfo">{{translate("taginfo.textinfo3")}}</p>
      </li>
    </ul>
    </div>

    <div class="tagsectioncontainer">
    <h1 class="tagsection">{{translate("taginfo.codetags")}}</h1> <hr/>
    <ul class="taglist">
      <li>
        <h2 class="tagheader">[code_block] {{translate("taginfo.code")}} [/code_block]</h2>
        <p class="taginfo">{{translate("taginfo.codeinfo")}}</p>
      </li>
      <li>
        <h2 class="tagheader">[user_input] <br/>Code <br/>[regex] {{translate("taginfo.regex")}} [/regex]<br/>[/user_input]</h2>
        <p class="taginfo">{{translate("taginfo.regexinfo")}}</p>
      </li>
      <li>
        <h2 class="tagheader">[hint] {{translate("taginfo.text")}} [/hint]</h2>
        <p class="taginfo">{{translate("taginfo.textinfo4")}}</p>
      </li>
    </ul>
    </div>
  </body>

</html>
