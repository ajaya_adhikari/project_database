% from lang import translate
% setdefault("charts", False)
% rebase("baseframe", charts=charts)
% setdefault("can_edit", False)
% setdefault("overview", "")
% setdefault("statistics", "")

<div style="text-align: center;">
  <h1 class="challenge-title" style="text-align: center; margin-bottom: 0;">{{challenge.name}}</h1>
  <h2 class="author-name" style="text-align: center; margin-top: 0;">{{author_name}}</h2>

  <p style="text-align: left; margin-bottom: 2em;">{{challenge.description}}</p>

  <div class="selector">
    <a href="/challenge/{{challenge.id}}/overview" class="first {{overview}}">
      {{translate("challengelandingpage.overview")}}
    </a>
    <a href="/challenge/{{challenge.id}}/statistics" class="last {{statistics}}">
      {{translate("challengelandingpage.statistics")}}
    </a>
  </div>

  <div class="challenges-container-top"></div>
  {{!base}}
  <div class="challenges-container-bottom"></div>
  <div style="margin-top: 3em;"></div>
  <button onclick="window.location.href='/challenge/{{challenge.id}}/1'" class="large-button pure-button pure-button-primary">{{translate("challengelandingpage.startchallenge")}}</button>
  % if can_edit:
  <button onclick="window.location.href='/challenge/create/{{challenge.id}}'" class="large-button pure-button pure-button-primary">{{translate("challengelandingpage.editchallenge")}}</button>
  % end
</div>
