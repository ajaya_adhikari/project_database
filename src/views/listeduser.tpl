% from lang import translate
% name              = user.name
% id                = user.id
% # registration_date = user.registration_date # TODO
% coins             = [1,0,128]

<div class="user-container-entry">
  <div class="pure-g">
    <div class="user-col-title pure-u-18-24">
      <div class="title"><a href="profile/{{id}}">{{name}}</a></div>
      <div class="detail">{{translate("search.membersince")}} march 2015<!--TODO--></div>
    </div>
    <div class="user-col-earnings pure-u-6-24">
      % if coins[0]:
        <span class="gold">{{coins[0]}}</span>
      % end
      % if coins[0] or coins[1]:
        <span class="silver">{{coins[1]}}</span>
      % end
      <span class="bronze">{{coins[2]}}</span>
    </div>
  </div>
</div>
