% from lang import translate
% rebase("baseframe")
% rev_order = "asc"
% if order == rev_order:
%   rev_order = "desc"
% end
% url = "browse"
% if defined("mine_only"):
%   url = "challenge"
% end
% setdefault("challenge_id",None)
% if challenge_id:
%   url = "{}/{}".format(url, challenge_id)
% end


<div style="text-align: center;">
  <div style="width: 400px; margin: 25px auto;">
    <div class="pure-g">
      <!--
      % if defined("mine_only"):
        <div class="pure-g">
          <div class="pure-u-1">
            <h4 class="challenge-title" style="text-align: center;">{{translate("browse.mychallenges")}}</h4>
          </div>
        </div>
      % end
      -->
      % if not challenge_id:
        <div class="pure-u-6-24 planguage
          % if prog=="all":
            planguage_selected
          % end
          ">
          <a href="/{{url}}/{{lang}}/all/{{difficulty}}/{{sort_by}}/{{order}}/{{currentPage}}" style="border-bottom: none;">
          <img src="/static/img/lang_any_big.png" />
          <h3>{{translate("browse.showall")}}</h3>
          </a>
        </div>
        <div class="pure-u-6-24 planguage
          % if prog=="python":
            planguage_selected
          % end
          ">
          <a href="/{{url}}/{{lang}}/python/{{difficulty}}/{{sort_by}}/{{order}}/{{currentPage}}" style="border-bottom: none;">
          <img src="/static/img/lang_python_big.png" />
          <h3>Python</h3>
          </a>
        </div>
        <div class="pure-u-6-24 planguage
          % if prog=="cpp":
            planguage_selected
          % end
          ">
          <a href="/{{url}}/{{lang}}/cpp/{{difficulty}}/{{sort_by}}/{{order}}/{{currentPage}}" style="border-bottom: none;">
          <img src="/static/img/lang_cpp_big.png" />
          <h3>C++</h3>
          </a>
        </div>
        <div class="pure-u-6-24 planguage
          % if prog=="rust":
            planguage_selected
          % end
          ">
          <a href="/{{url}}/{{lang}}/rust/{{difficulty}}/{{sort_by}}/{{order}}/{{currentPage}}" style="border-bottom: none;">
          <img src="/static/img/lang_rust_big.png" />
          <h3>Rust</h3>
          </a>
        </div>
      % end
    </div>
  </div>
  <div style="margin: 10px auto 50px;">
    <form class="pure-form">
      <select id="difficulty">
        <option value="any" link="/{{url}}/{{lang}}/{{prog}}/any/{{sort_by}}/{{order}}/{{currentPage}}"
          % if difficulty=="any":
            selected
          % end
        >
        {{translate("browse.anydifficulty")}}
        </option>
        <option value="easy" link="/{{url}}/{{lang}}/{{prog}}/easy/{{sort_by}}/{{order}}/{{currentPage}}"
          % if difficulty=="easy":
            selected
          % end
        >
        {{translate("browse.easy")}}
        </option>
        <option value="medium" link="/{{url}}/{{lang}}/{{prog}}/medium/{{sort_by}}/{{order}}/{{currentPage}}"
          % if difficulty=="medium":
            selected
          % end
        >
        {{translate("browse.medium")}}
        </option>
        <option value="hard" link="/{{url}}/{{lang}}/{{prog}}/hard/{{sort_by}}/{{order}}/{{currentPage}}"
          % if difficulty=="hard":
            selected
          % end
        >
        {{translate("browse.hard")}}
        </option>
      </select>
      <script>
        document.getElementById('difficulty').onchange = function() {
            window.location.href = this.children[this.selectedIndex].getAttribute('link');
        }
      </script>
      <select id="language">
        <option value="en" link="/{{url}}/en/{{prog}}/{{difficulty}}/{{sort_by}}/{{order}}/{{currentPage}}"
          % if lang=="en":
            selected
          % end
        >
        {{translate("browse.english")}}
        </option>
        <option value="nl" link="/{{url}}/nl/{{prog}}/{{difficulty}}/{{sort_by}}/{{order}}/{{currentPage}}"
          % if lang=="nl":
            selected
          % end
        >
        {{translate("browse.dutch")}}
        </option>
      </select>
      <script>
        document.getElementById('language').onchange = function() {
            window.location.href = this.children[this.selectedIndex].getAttribute('link');
        }
      </script>
    </form>
  </div>
  <div class="selector">
    % if sort_by=="date":
      <a href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/date/{{rev_order}}/{{currentPage}}" class="first current
      % if order == "asc":
        ascending
      % else:
        descending
      % end
    % else:
    <a href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/date/asc/{{currentPage}}" class="first
    % end
    ">
    {{translate("browse.date")}}
    </a>

    % if sort_by=="title":
      <a href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/title/{{rev_order}}/{{currentPage}}" class="current
      % if order == "asc":
        ascending
      % else:
        descending
      % end
    % else:
      <a href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/title/asc/{{currentPage}}"
    % end
    ">
    {{translate("browse.title")}}
    </a>

    % if sort_by=="popularity":
      <a href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/popularity/{{rev_order}}/{{currentPage}}" class="last current
      % if order == "asc":
        ascending
      % else:
        descending
      % end
    % else:
      <a href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/popularity/asc/{{currentPage}}" class="last
    % end
    ">
    {{translate("browse.popularity")}}
    </a>
  </div>
</div>
<div class="challenges-container-top"></div>
% if challenges:
  % for challenge in challenges:
    % include('listedchallenge.tpl', challenge=challenge, challenge_id=challenge_id)
  % end
% end
<div class="challenges-container-bottom"></div>
<div style="text-align: center;">
  % prev = max(1, currentPage-1)
  % next = min(pages, currentPage+1)
  % if pages > 1:
  <ul class="my-paginator">
    <li><a class="my-button prev" href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/{{sort_by}}/{{order}}/{{prev}}">&#171;</a></li>
    % for page in range(1, pages+1):
      % if currentPage == page:
        <li><a class="my-button current" href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/{{sort_by}}/{{order}}/{{page}}">{{page}}</a></li>
      % else:
        <li><a class="my-button" href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/{{sort_by}}/{{order}}/{{page}}">{{page}}</a></li>
      % end
    % end
    <li><a class="my-button next" href="/{{url}}/{{lang}}/{{prog}}/{{difficulty}}/{{sort_by}}/{{order}}/{{next}}">&#187;</a></li>
  </ul>
  % end
</div>
