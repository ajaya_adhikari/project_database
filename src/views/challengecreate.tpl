% from lang import translate
% rebase("baseframe")
% from bottle import request
% description = ""
% button_text = "SAVE"
% extra_url = ""
% if series:
%   description = series.description
%   difficulty_selected = series.difficulty
%   language_selected = series.language
%   programming_language_selected = series.programming_language
%   button_text = "UPDATE"
%   extra_url = "/" + str(series.id)
% end


<div class="pure-g">
  <div class="pure-u-1">
    <h1 class="red-title" style="margin: 10px 0 0;">{{translate("newchallenge.challengeinfo")}}</h1>
    <p style="font-size: 0.9em; margin: 0">{{translate("newchallenge.info")}}</p>
    <hr>
    <form class="pure-form pure-form-stacked" action="/challenge/create{{extra_url}}" method="POST">
      <fieldset>
        <div class="pure-g">
          <div class="pure-u-1 pure-u-md-1">
            <label for="title">{{translate("newchallenge.title")}}</label>
            <input type="text" id="title" name="title" placeholder={{translate("newchallenge.title")}}
            % if series:
                  value = "{{series.name}}"
            % end
            class="pure-u-1" required autofocus>
          </div>
          <div class="pure-u-1 pure-u-md-6-24">
            <label for="difficulty">{{translate("newchallenge.difficulty")}}</label>
            <select id="difficulty" name="difficulty" class="pure-u-23-24">
              <option value="easy"
              % if series and difficulty_selected == 1:
                   selected
              % end
              >{{translate("newchallenge.easy")}}</option>
              <option value="medium"
              % if series and difficulty_selected == 2:
                   selected
              % end
              >{{translate("newchallenge.medium")}}</option>
              <option value="hard"
              % if series and difficulty_selected == 3:
                   selected
              % end
              >{{translate("newchallenge.hard")}}</option>
            </select>
          </div>
          <div class="pure-u-1 pure-u-md-6-24">
            <label for="language">{{translate("newchallenge.language")}}</label>
            <select id="language" name="language" class="pure-u-23-24">
              <option value="en"
              % if series and language_selected == "en":
                    selected
              % end
              >{{translate("newchallenge.english")}}</option>
              <option value="nl"
              % if series and language_selected == 'nl':
                    selected
              % end
              >{{translate("newchallenge.dutch")}}</option>
            </select>
          </div>
          <div class="pure-u-1 pure-u-md-6-24">
            <label for="planguage">{{translate("newchallenge.proglanguage")}}</label>
            <select id="planguage" name="planguage" class="pure-u-23-24">
              <option value="python"
              % if series and programming_language_selected == 'python':
                    selected
              % end
              >Python</option>
              <option value="cpp"
              % if series and programming_language_selected == 'cpp':
                    selected
              % end
              >C++</option>
              <option value="rust"
              % if series and programming_language_selected == 'rust':
                    selected
              % end
              >Rust</option>
            </select>
          </div>
          <div class="pure-u-1 pure-u-md-6-24">
            <label for="reward">{{translate("newchallenge.reward")}}</label>
            <select id="reward" name="reward" class="pure-u-1">
              <option value="0">0</option>
              <option value="25" selected>25</option>
              <option value="50">50</option>
              <option value="75">75</option>
              <option value="100">100</option>
            </select>
          </div>
          <div class="pure-u-1 pure-u-md-1">
            <label for="description">{{translate("newchallenge.description")}}</label>
            <textarea id="description" name="description" class="pure-u-1" required>{{description}}</textarea>
          </div>
          <div class="pure-u-1 pure-u-md-1">
            <label for="tags">{{translate("newchallenge.tags")}} <span style="color: #aaaaaa; font-size: 0.8em;">{{translate("newchallenge.taginfo")}}<span></label>
            <input type="text" id="tags" name="tags"
            % if series:
            %     tags = ', '.join(series.tags)
                  value = "{{tags}}"
            % end
            class="pure-u-1"></textarea>
          </div>
        </div>

        <label for="public" class="pure-checkbox">
          <p><input id="public" name="public" type="checkbox"
            % if not series:
                disabled
            % elif series.visible:
                checked
            % end
            > {{translate("newchallenge.makepublic")}}</input></p>
        </label>
        <button type="submit" name="save" class="large-button pure-button pure-button-primary">{{translate("newchallenge.savechallenge")}}</button>
      </fieldset>
    </form>
  </div>
</div>

% if series:
<div class="pure-g">
  <div class="pure-u-1">
    <h1 class="red-title" style="margin: 20px 0 0;">{{translate("newchallenge.exercises")}}</h1>
    <p style="font-size: 0.9em; margin: 0">{{translate("newchallenge.exerciseinfo")}}</p>
    <hr>
    % for i in range(len(exercises)):
    %   num = i+1
    <div class="pure-g" style="margin-bottom: 4px;">
      <div class="pure-u-13-24">
        {{exercises[i].title}}
      </div>
      <div class="pure-u-11-24" style="text-align: right;">
        % disabled = ""
        % if not exercises[i].author_id == request.session.get('user_id'):
        %   disabled = "disabled"
        % end
        <!-- TODO maybe change to copy instead of disabling -->
        <button onclick="window.location.href='/challenge/create/{{series.id}}/exercise/{{num}}'" class="edit-button pure-button pure-button-primary" {{disabled}}>{{translate("newchallenge.edit")}}</button>
        <button onclick="window.location.href='/challenge/create/{{series.id}}/exercise/{{num}}/delete'" class="delete-button pure-button pure-button-primary">{{translate("newchallenge.delete")}}</button>
      </div>
    </div>
    % end
    <button onclick="window.location.href='/challenge/create/{{series.id}}/exercise'" class="large-button pure-button pure-button-primary">{{translate("newchallenge.createexercise")}}</button>
    <button onclick="window.location.href='/browse/{{series.id}}'" class="large-button pure-button pure-button-primary">{{translate("newchallenge.findexercise")}}</button>
  </div>
</div>
% end
