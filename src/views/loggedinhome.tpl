% from lang import translate
% rebase("baseframe")

<!-- user info -->
<div class="pure-g">
  <h1 class="pure-u-12-24" style="text-align: left;">
    {{name}}
    <span style="font-size: 16px;">
    % if credits > 1000000:
    <span class="gold">{{credits/1000000}}</span>
    %end
    % if credits > 1000:
    <span class="silver">{{(credits/1000)%1000}}</span>
    %end
    <span class="bronze">{{credits%1000}}</span>
    </span>
  </h1>
  <div class="pure-u-12-24" style="text-align: right; margin-top: 22px;">
    <button onclick="window.location.href='/challenge'" class="medium-button pure-button pure-button-primary">{{translate("loggedinhome.browsemychallenges")}}</button>
    <button onclick="window.location.href='/challenge/create'" class="medium-button pure-button pure-button-primary">{{translate("loggedinhome.createnewchallenge")}}</button>
  </div>
</div>

<!-- recent and friend activity-->
<div class="pure-g" style="margin-top: 20px;">
  <!-- linker helft -->
  <div class="pure-u-12-24">
    <div style="margin-right: 5px;">
      <!-- title: recent activity-->
      <h1 class="red-title" style="text-align: left;">{{translate("loggedinhome.recentactivity")}}</h1>
      <!-- box -->
      <div class="pure-g activity-container">
        % if recentActivities:
        %   for activity in recentActivities[:]:
              <!-- logo -->
              <div class="pure-u-3-24 activity-image" style="text-align: left;">
                <img src="/static/img/icon_{{activity[1]}}.png" />
              </div>
              <!-- text -->
              <div class="pure-u-21-24" style="text-align: left;">
                <p>{{activity[0]}}</p>
              </div>
        %   end
        % end
      </div>
    </div>
  </div>

  <!-- rechter helft-->
  <div class="pure-u-12-24">
    <div style="margin-left: 5px;">
    <!--title-->
    <h1 class="red-title" style="text-align: left;">{{translate("loggedinhome.friendactivity")}}</h1>
    <!-- box -->
    <div class="pure-g activity-container">
      <!-- text -->
      <div class="pure-u-1" style="text-align: left;">
        % if friendActivities:
        %   for activity in friendActivities[:]:
              <!-- logo -->
              <!--
              <div class="pure-u-3-24 activity-image" style="text-align: left;">
                <img src="/static/img/icon_{{activity[1]}}.png" />
              </div>
              -->
              <!-- text -->
              <div class="pure-u-21-24" style="text-align: left;">
                <p>{{activity[0]}}</p>
              </div>
        %   end
        % end
      </div>
    </div>
    </div>
  </div>
</div>

<!-- new challenges-->
<h1 class="red-title" style="margin-top: 35px;">{{translate("loggedinhome.recommendedchallenges")}}</h1>
<div class="challenges-container-top"></div>
  % if recommendation:
  %   for challenge in recommendation:
  %     include('listedchallenge.tpl', challenge=challenge)
  %     end
  % end
<div class="challenges-container-bottom"></div>

<!-- Browse all button-->
<div style="text-align: center; margin:2.2em;">
  <a href="/browse">
    <button class="large-button pure-button pure-button-primary" style="width: 320px;">{{translate("loggedinhome.browseallchallenges")}}</button>
  </a>
</div>
