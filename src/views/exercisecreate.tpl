% from lang import translate
% rebase("baseframe")
% setdefault("title", "")
% button_text1 = translate("newexercise.save")
% button_text2 = translate("newexercise.saveandreturn")
% if defined("exercise"):
%   title = exercise.title
%   if not text_field:
%     text_field = exercise.contents
%   end
%   button_text1 = translate("newexercise.update")
%   button_text2 = translate("newexercise.updateandreturn")
% end
% extra_url = ""
% if defined("exercise_num"):
%   extra_url = "/" + str(exercise_num)
% end
% if defined("error_string"):
%  pass
% end

<!-- TODO report errors-->
<div class="pure-form pure-g">
  <div class="pure-u-5-24" style="margin-top: 7em;">
    <div style="text-align: left;">
      <span style="font-size: 15px; font-weight:bold;">TAGS</span> <a class="help" onclick="window.open('/challenge/taginfo', '_blank', 'width=400,height=600,scrollbars=yes');">?</a>
    </div>
    <div class="pure-u-1 textinsert">
      <p>{{translate("newexercise.markup")}}</p>
      <a id="large_header" class="pure-u-1 first" onclick="javascript:format(this.id)">{{translate("newexercise.largeheader")}}</a>
      <a id="small_header" class="pure-u-1"       onclick="javascript:format(this.id)">{{translate("newexercise.smallheader")}}</a>
      <a id="highlight"    class="pure-u-1"       onclick="javascript:format(this.id)">{{translate("newexercise.highlight")}}</a>
      <a id="hint"         class="pure-u-1 last"  onclick="javascript:format(this.id)">{{translate("newexercise.hint")}}</a>

      <p>{{translate("newexercise.codeblock")}}</p>
      <a id="code_block"      class="pure-u-1 first" onclick="javascript:format(this.id)">{{translate("newexercise.codeblock")}}</a>
      <a id="user_input"      class="pure-u-1 last"  onclick="javascript:format(this.id)">{{translate("newexercise.userinput")}}</a>

      <p>{{translate("newexercise.expectedresult")}}</p>
      <a id="regex"           class="pure-u-1 first" onclick="javascript:format(this.id)">{{translate("newexercise.expectregex")}}</a>
      <a id="evaluation_code" class="pure-u-1 last"  onclick="javascript:format(this.id)">{{translate("newexercise.evaluationcode")}}</a>
    </div>
  </div>
  <div class="pure-u-1-24">
  </div>
  <div class="pure-u-18-24">
    <form class="pure-form pure-form-stacked" action="/challenge/create/{{challenge_id}}/exercise{{extra_url}}" method="POST">
      <div class="pure-g"  style="margin-bottom: 1em;">
        <div class="pure-u-1">
          <h2 style="text-align: left; margin-left:0;">{{translate("newexercise.title")}}</h2>
          <input type="text" name="title" placeholder={{translate("newexercise.title")}} class="pure-input-1" value="{{title}}" required autofocus>
        </div>
      </div>
<div class="error_box">
{{error_string}}
</div>

% if text_field:
<textarea id="text-base" name="text-base">{{text_field}}</textarea>
% else:
<textarea id="text-base" name="text-base">{{translate("newexercise.sample")}}

[user_input]
[regex]Hello World[/regex]
[/user_input]
</textarea>
% end
      <script>
        cm_editor = CodeMirror.fromTextArea($$("#text-base"), {
          mode: "exercise",
        });
		cm_editor.setSize("100%", 500);
        function format(text, link)
        {
          centertext = cm_editor.getSelection();
          cm_editor.replaceSelection("[" + text + "]" + centertext + "[/" + text + "]");
          cm_editor.focus();
        }
      </script>
      <div class="pure-g" style="margin: 1em 0 0 0;">
        <div class="pure-u-1">
          <button type="submit" name="preview" class="medium-button pure-button pure-button-primary">{{translate("newexercise.preview")}}</button>
          <button type="submit" name="save" class="medium-button pure-button pure-button-primary">{{button_text1}}</button>
          <button type="submit" name="return" class="medium-button pure-button pure-button-primary">{{button_text2}}</button>
        </div>
      </div>
    </form>
  </div>
</div>
