% from lang import translate
% rebase("baseframe")
% error=False

<div style="text-align: center;">

% if completed_home_challenge:
  <h1>{{translate("signup.rewardtitle")}}</h1>
  <h2>{{translate("signup.rewardinfo1")}}<span class="bronze">{{translate("signup.rewardcoins")}}</span>{{translate("signup.rewardinfo2")}}</h2>
% else:
  <h1>{{translate("signup.title")}}</h1>
  <h2>{{translate("signup.subtitle")}}</h2>
% end

    <form class="pure-form pure-form-stacked signup-form" action="/signup" method="POST">
        <input type="text"     name="username" placeholder={{translate("signup.username")}} class="pure-input-1" style="text-align: center;" required autofocus>
        <fieldset class="pure-group">
          <input type="password" name="password" placeholder={{translate("signup.password")}} class="pure-input-1" style="text-align: center;" required>
          <input type="password" name="passwordconfirmation" placeholder={{translate("signup.confirmation")}}  style="width: 100%; text-align: center;" required>
        </fieldset>
        <button type="submit" class="large-button pure-button pure-input-1 pure-button-primary" style="margin: 0;">{{translate("signup.signup")}}</button>
    </form>

  %if 'errormsg' in vars():
    <div class="form-error" style="width: 100%; text-align: center;">
      {{errormsg}}
    </div>
  %end
  <p>{{translate("signup.logininfo")}} <a class="underline" href="login">{{translate("signup.login")}}</a></p>
</div>
