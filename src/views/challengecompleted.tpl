% from __future__ import division
% from lang import translate
% rebase("baseframe")

<div style="text-align: center;">
  <p>{{translate("challengecompleted.congratulation")}}</p>
  <h1 class="challenge-title" style="text-align: center; margin:0;">{{challenge_title}}</h1>
  <p>{{translate("challengecompleted.earning")}} <span class="bronze">100</span></p>
  <div style="margin-top: 2em;">
    <div class="challenges-container-top"></div>
    <div class="pure-g challenges-container-entry">
      <div class="pure-u-1">
    % for (index, (title, attempt_count)) in enumerate(attempt_data):
    <div class="pure-g">
      <div style="text-align: left; padding: 0.2em 0;" class="pure-u-6-24">
        {{title}}
      </div>
      <div class="pure-u-18-24">
        % if attempt_count == 1:
           <div class="bar full pure-u-1" style="width: 100%"></div>
        % else:
          % width = 100/attempt_count
          <div class="pure-g">
          % for i in range(attempt_count):
            % if i == 0:
            <div class="bar grey first pure-u-1" style="width: {{width}}%"></div>
            % elif i == attempt_count-1:
            <div class="bar last pure-u-1" style="width: {{width}}%"></div>
            % else:
            <div class="bar grey pure-u-1" style="width: {{width}}%"></div>
            % end
          % end
          </div>
        % end
      </div>
    </div>
    % end
      </div>
    </div>
    <div class="challenges-container-bottom"></div>
  </div>
  <div style="text-align: center; margin: 2em 0;">
    <p>{{translate("challengecompleted.rateinfo")}}</p>
    <form class="pure-form" action="/challenge/{{challenge_id}}/completed" method="POST">
      <select id="rating" name="rating">
        <option value="1">1 - {{translate("challengecompleted.abysmal")}}</option>
        <option value="2">2 - {{translate("challengecompleted.poor")}}</option>
        <option value="3">3 - {{translate("challengecompleted.adequate")}}</option>
        <option value="4">4 - {{translate("challengecompleted.good")}}</option>
        <option value="5" selected>5 - {{translate("challengecompleted.sublime")}}</option>
      </select>
      <button type="submit" style="height: 40px;" class="large-button pure-button pure-button-primary">
        RATE
      </button>
    </form>
  </div>
</div>
<button class="large-button pure-button pure-button-primary" id="sharebuttonfacebook">Share</button>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '813570728691513',
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script>
$("#sharebuttonfacebook").click(function() {
  var msg = "Hi This is my wonderful website";
  FB.ui({method: 'feed',
  name: 'MCQ Nation ', 
  link: 'http://www.mcqnation.com/Trail3/pyramid.php?..dyanmic stuff..' , 
  picture: 'http://www.mcqnation.com/Trail3/fbapp2.png', 
  caption:'CLICK ON THE IMAGE TO GO TO OUR WEBSITE', 
  description: msg , 
  message: 'AAA'});
  return false;
});
</script>




