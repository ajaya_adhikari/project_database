% from lang import translate
% setdefault("copy_challenge_id", None)

<div class="challenges-container-entry">
  <div style="text-align: left;" class="pure-g">
    <div class="challenge-col-title pure-u-16-24">
      <div style="margin-top: 5px;" class="title"><a href="/challenge/{{challenge.id}}/{{num}}">{{exercise.title}}</a></div>
    </div>
    % if copy_challenge_id:
    <div style="text-align: right;" class="pure-u-8-24">
      <button onclick="window.location.href='/challenge/{{challenge.id}}/copy/{{copy_challenge_id}}/ref/{{exercise.id}}'" class="medium-button pure-button pure-button-primary"><img style="vertical-align: middle; margin: -2px 5px 0 0;" src="/static/img/icon_link.png"/>{{translate("listedexercise.addref")}}</button>
      <button onclick="window.location.href='/challenge/{{challenge.id}}/copy/{{copy_challenge_id}}/copy/{{exercise.id}}'" class="medium-button pure-button pure-button-primary"><img style="vertical-align: middle; margin: -2px 5px 0 0;" src="/static/img/icon_copy.png"/>{{translate("listedexercise.addcopy")}}</button>
    </div>
    % end
  </div>
</div>
