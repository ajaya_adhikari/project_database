% from lang import translate
% rebase("base")

<div class="splash">
  <div class="splash-content">
    <div class="splash-title">
      <h1>{{translate("home.splashtitle")}}</h1>
      <h2>{{translate("home.splashsubtitle1")}} <span class="highlight">{{translate("home.splashsubtitle2")}}</span>{{translate("home.splashsubtitle3")}}
        <a class="underline" href="#learn-more">{{translate("home.splashlearnmore")}}</a>.</h2>
    </div>
    <div class="splash-signup">
      <form class="pure-form" action="/login" method="POST">
        <input type="text"     name="username" placeholder="{{translate("home.username")}}" required>
        <input type="password" name="password" placeholder="{{translate("home.password")}}" required>
        <input type="hidden"   name="origin"   value="home">
        <button type="submit" class="button-start-coding pure-button pure-button-primary">{{translate("home.startcoding")}}</button>
      </form>
    </div>
  </div>
</div>

<div class="code-info">

<h1 style="margin: 0.8em 0 1.2em;">{{translate("home.headertry1")}}<b>{{translate("home.headertry2")}}</b>!</h1>

<div class="pure-g">

  <!-- Exercise column -->
  <div class="pure-u-13-24" style="position: relative">
  <div style="position: relative;">

  <form action="javascript: evaluate_home_exercise();">

  <!-- Python -->
  <div id="code-python">
<textarea id="txt-code-python" name="code-python">
{{translate("home.challengeinfo1")}}
def gcd(nrs):
    for div in range(min(nrs), 1, -1):
        if any(n % div == 0 for n in nrs):
            return div
    return 1

{{translate("home.challengeinfo2")}}
numbers = [72, 648, 54, 1782]
print(gcd(numbers))
</textarea>
  </div>

  <!-- C++ -->
  <div id="code-cpp" style="display:none;">
<textarea id="txt-code-cpp"  name="code-cpp">
// Can you fill in the blanks?
for( int i = 1; i < 101; ++i ) {
    if( ___ )
        cout &lt;&lt; "FizzBuzz" &lt;&lt; '\n';
    else if( ___ )
        cout &lt;&lt; "Fizz" &lt;&lt; '\n';
    else if( ___ )
        cout &lt;&lt; "Buzz" &lt;&lt; '\n';
    else
        cout &lt;&lt; i &lt;&lt; '\n';
}
</textarea>
  </div>

  <!-- Rust -->
  <div id="code-rust" style="display:none;">
<textarea id="txt-code-rust" name="code-rust">
// Can you fill in the blanks?
for i in range(1, 101) {
    if ___ {
        println!("FizzBuzz");
    } else if ___ {
        println!("Fizz");
    } else if ___ {
        println!("Buzz");
    } else {
        println!("{}", i);
    }
}
</textarea>
  </div>

  <input class="submit-button pure-button pure-button-primary" style="margin: 1em 0.8em 0;" type="submit" value="Submit (Ctrl-Enter)" id="submit"/>
  </form>
  </div>
  <div id='result' class='userinput-result userinput-correct'></div>
  <div style="position: absolute; right: 19px; top: 15px; z-index: 10;"><span class="bronze">100</span></div>

  </div> <!-- end exercise column -->

  <div class="pure-u-1-24"></div>

  <!-- Lang selections column -->
  <div class="pure-u-10-24 home-lang-selection">
    <div id="lang-select-python" class="pure-g" style="padding-left:80px; margin-bottom: 30px;">
      <div class="pure-u" style="margin-left:-80px;">
         <a href="javascript: showCode('python');" class="noeffect"><img src="/static/img/lang_python_big.png" /></a>
      </div>
      <div class="pure-u-1" style="box-sizing: border-box; padding-left:15px;">
         <a href="javascript: showCode('python');">
           <h2 style="text-align:left; font-weight: bold; margin:0">Python</h2>
         </a>
         <p style="margin: 0;">{{translate("home.challengepython")}}</p>
      </div>
    </div>
    <div id="lang-select-cpp" class="pure-g" style="padding-left:80px; margin-bottom: 30px;">
      <div class="pure-u" style="margin-left:-80px;">
         <a href="javascript: showCode('cpp');" class="noeffect"><img src="/static/img/lang_cpp_big.png" /></a>
      </div>
      <div class="pure-u-1" style="box-sizing: border-box; padding-left:15px;">
         <a href="javascript: showCode('cpp');">
           <h2 style="text-align:left; font-weight: bold; margin:0">C++</h2>
         </a>
         <p style="margin: 0;">{{translate("home.challengecpp")}}</p>
      </div>
    </div>
    <div id="lang-select-rust" class="pure-g" style="padding-left:80px;">
      <div class="pure-u" style="margin-left:-80px;">
         <a href="javascript: showCode('rust');" class="noeffect"><img src="/static/img/lang_rust_big.png" /></a>
      </div>
      <div class="pure-u-1" style="box-sizing: border-box; padding-left:15px;">
         <a href="javascript: showCode('rust');">
           <h2 style="text-align:left; font-weight: bold; margin:0">Rust</h2>
         </a>
         <p style="margin: 0;">{{translate("home.challengerust")}}</p>
      </div>
    </div>
  </div>

  <script>
    function showCode(lang) {
        $("#code-python").hide();
        $("#code-cpp").hide();
        $("#code-rust").hide();
        $("#lang-select-python").set("$opacity", 0.3);
        $("#lang-select-cpp").set("$opacity", 0.3);
        $("#lang-select-rust").set("$opacity", 0.3);
        if( lang == "python" ) {
            $("#code-python").show();
            $("#lang-select-python").set("$opacity", 1);
            if( typeof cm_python == "undefined" ) {
                cm_python = CodeMirror.fromTextArea($$("#txt-code-python"), {
                    mode: "python",
                });
            }
        } else
        if( lang == "cpp" ) {
            $("#code-cpp").show();
            $("#lang-select-cpp").set("$opacity", 1);
            if( typeof cm_cpp == "undefined" ) {
                cm_cpp = CodeMirror.fromTextArea($$("#txt-code-cpp"), {
                    mode: "clike",
                });
            }
        } else
        if( lang == "rust" ) {
            $("#code-rust").show();
            $("#lang-select-rust").set("$opacity", 1);
            if( typeof cm_rust == "undefined" ) {
                cm_rust = CodeMirror.fromTextArea($$("#txt-code-rust"), {
                    mode: "rust",
                });
            }
        }
        active_language = lang;
    }
    showCode("python");
  </script>

</div> <!-- end grid -->

<div style="text-align: center; margin:2.2em;">
  <a href="/browse">
  <button class="large-button pure-button pure-button-primary">{{translate("home.browseall")}}</button>
  </a>
</div>

<div style="border-bottom: 1px dotted #505050; margin: 2em 0;"></div>

<a name="learn-more"></a>
<h1>{{translate("home.headerabout1")}} <b>Panjandrum</b> {{translate("home.headerabout3")}}?</h1>

<p>
{{translate("home.about1")}}
</p>
<p>
{{translate("home.about2")}}
</p>

</div>
