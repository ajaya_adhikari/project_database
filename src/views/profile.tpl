% from lang import translate
% rebase("baseframe")

<!-- user info -->
<div class="pure-g">
  <!-- picture
  <div class="pure-u-6-24 activity-image" style="text-align: left;">
    <img src="/static/img/profilepicture.png" />
  </div>

  whitespace
  <div class="pure-u-2-24 " style="text-align: left;">
  </div>
  -->

  <!-- text -->
  <div class="pure-u-24-24" style="text-align: center; margin-top: 22px;">
    <h1 class="red-title" style="text-align: center; font-weight: bold;">{{profilename}}</h1>
    <p>{{translate("profile.exercisescreated")}}: {{createdchallenges}}</p>
    <p>{{translate("profile.exercisessolved")}}: {{solvedchallenges}}</p>

    % if button == "removefriend":
      <button onclick="window.location.href='/profile/removefriend/{{userid}}/{{profileid}}' " class="medium-button pure-button pure-button-primary" style="margin-top: 55px;">{{translate("profile.remove")}}</button>
    %elif button == "addfriend":
      <button onclick="window.location.href='/profile/addfriend/{{userid}}/{{profileid}}' " class="medium-button pure-button pure-button-primary" style="margin-top: 55px;">{{translate("profile.add")}}</button>
    % end
  </div>
</div>
