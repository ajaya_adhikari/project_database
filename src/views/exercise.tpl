% from lang import translate
% rebase("baseframe")
<div class="pure-g">
  <div class="pure-u-5-24"></div>
  <div class="pure-u-1-24"></div>
  <div class="pure-u-13-24">
    <div class="challenge-title">{{challenge_title}}</div>
    <div class="exercise-title">{{exercise_title}}</div>
  </div>
  <div class="pure-u-5-24">
    <div class="author-name">{{author_name}}</div>
  </div>
</div>
<div class="pure-g">
  <div class="pure-u-5-24" style="margin-top: 0.85em;">
    % for i in range(len(challenge_exercises)):
      % exercise = challenge_exercises[i]
      % if exercise.title == exercise_title:
        <div class="pure-g exercise-list-item exercise-list-selected" style="margin-bottom: 1.3em;">
      % else:
        <div class="pure-g exercise-list-item" style="margin-bottom: 1.3em;">
      % end
        <div class="pure-u-4-24">
          <!-- TODO: only display checkmark if completed -->
          <!--<img src="/static/img/icon_checkmark.png"/>-->
        </div>
        <div class="pure-u-20-24">
          <a class="exercise-list-title" href="/challenge/{{challenge_id}}/{{i+1}}">{{exercise.title}}</a>
        </div>
      </div>
    % end
  </div>
  <div class="pure-u-1-24"></div>
  <div class="pure-u-18-24">

    % if preview:
    {{!preview}}
    % else:
    {{translate("exercise.challenge")}} "{{challenge_title}}" {{translate("exercise.hasnoexercises")}}
    % end
    % if defined("solved_exercise") and defined("solved_challenge"):
      <!-- TODO: style these buttons -->
      <!-- Will end badly if users completes exercises out of order and does last one before another -->
      % if solved_challenge:
        <button id="continue" onclick="window.location.href='/challenge/{{challenge_id}}/completed'" class="large-button pure-button pure-button-primary">{{translate("exercise.complete")}}</button>
      % else:
        % if num < len(challenge_exercises):
          <button id="continue" onclick="window.location.href='/challenge/{{challenge_id}}/{{num+1}}'" class="large-button pure-button pure-button-primary" {{"" if solved_exercise else "disabled"}}>{{translate("exercise.continue")}}</button>
        % else:
          <button id="continue" onclick="window.location.href='/challenge/{{challenge_id}}/{{num}}'" class="large-button pure-button pure-button-primary" {{"" if solved_exercise else "disabled"}}>{{translate("exercise.continue")}}</button>
        % end
      % end
    % end
  </div>
</div>
