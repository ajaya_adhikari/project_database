% from lang import translate
% rebase("challengelandingpage", charts=True)
% statistics = "current"
% setdefault("can_edit", False)

<div class="pure-g challenges-container-entry">
  <div class="pure-u-1">
    <p>{{percentage_solved}} {{translate("challengestatistics.nrofsolvers")}}</p>
  </div>
</div>

<!-- Show some charts -->
<div class="pure-g challenges-container-entry">
  <div class="pure-u-12-24">
    <canvas id="attempt-chart"></canvas>
    <div id="attempt-legend"></div>
  </div>
  <div class="pure-u-12-24">
    <canvas id="rating-chart"></canvas>
    <div id="rating-legend"></div>
  </div>
</div>
<div class="pure-g challenges-container-entry">
  <div class="pure-u-1">
    <canvas id="completion-chart"></canvas>
    <div id="completion-legend"></div>
  </div>
</div>

<!-- Fill chart containers -->
<script  type="text/javascript">

  // Data for the Bar Chart containing ratings
  var averageAttemptsData = {
    labels : [
      % for point in average_attempts_data:
      "{{point.label}}",
      % end
    ],
    datasets : [
      {
        label: "{{translate("challengestatistics.attempts")}}",
        fillColor : "rgba(75,75,75,0.5)",
        strokeColor : "rgba(75,75,75,0.8)",
        highlightFill: "rgba(75,75,75,0.75)",
        highlightStroke: "rgba(75,75,75,1)",
        data : [
          % for point in average_attempts_data:
          {{point.value}},
          % end
        ]
      }
    ]
  }

  // Function to draw the BarChart and its legend
  var drawAttemptBarChart = function(){
    var contextBar = document.getElementById("attempt-chart").getContext("2d");
    var attemptsGraph = new Chart(contextBar).Bar(averageAttemptsData, {
      responsive : true,
      legendTemplate : "<p class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%><%}%></p>"
    });
    document.getElementById("attempt-legend").innerHTML = attemptsGraph.generateLegend();
    window.attemptsBar = attemptsGraph;
  };

  // Data for the Bar Chart containing ratings
  var ratingData = {
    labels : [
      % for point in rating_data:
      "{{point.label}}",
      % end
    ],
    datasets : [
      {
        label: "{{translate("challengestatistics.ratings")}}",
        fillColor : "rgba(75,75,75,0.5)",
        strokeColor : "rgba(75,75,75,0.8)",
        highlightFill: "rgba(75,75,75,0.75)",
        highlightStroke: "rgba(75,75,75,1)",
        data : [
          % for point in rating_data:
          {{point.value}},
          % end
        ]
      }
    ]
  }

  // Function to draw the BarChart and its legend
  var drawRatingBarChart = function(){
    var contextBar = document.getElementById("rating-chart").getContext("2d");
    var ratingGraph = new Chart(contextBar).Bar(ratingData, {
      responsive : true,
      legendTemplate : "<p class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%><%}%></p>"
    });
    document.getElementById("rating-legend").innerHTML = ratingGraph.generateLegend();
    window.ratingBar = ratingGraph;
  };

  // Data for the line chart of completion over time
  var lineChartData = {
    labels : [
      % for point in completion_data:
      "{{point.label}}",
      % end
    ],
    datasets : [
      {
        label: "{{translate("challengestatistics.completion")}}",
        fillColor : "rgba(220,220,220,0.2)",
        strokeColor : "rgba(220,220,220,1)",
        pointColor : "rgba(220,220,220,1)",
        pointStrokeColor : "#fff",
        pointHighlightFill : "#fff",
        pointHighlightStroke : "rgba(220,220,220,1)",
        data : [
          % for point in completion_data:
          {{point.value}},
          % end
        ]
      },
    ]
  }

  // Function to draw the lineChart and its legend
  var drawCompletionLineChart = function(){
    var contextBar = document.getElementById("completion-chart").getContext("2d");
    var completionGraph = new Chart(contextBar).Line(lineChartData, {
      responsive : true,
      bezierCurve : true,
      legendTemplate : "<p class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%><%}%></p>"
    });
    document.getElementById("completion-legend").innerHTML = completionGraph.generateLegend();
    window.completionLine = completionGraph;
  };

  window.onload = function(){
    drawAttemptBarChart();
    drawRatingBarChart();
    drawCompletionLineChart();
  };
</script>
