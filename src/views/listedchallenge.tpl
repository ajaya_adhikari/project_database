% from lang import translate
% codelanguage = challenge.programming_language
% title        = challenge.name
% description  = challenge.description
% user         = challenge.author_id
% time         = challenge.time
% tags         = challenge.tags
% coins        = [0,0,1]
% rating       = challenge.tempered_rating
% difficulty   = challenge.difficulty
% language     = challenge.language
% sid          = challenge.id
% visible      = challenge.visible
% setdefault("challenge_id", None)
% copy_url = ""
% if challenge_id:
%   copy_url = "/copy/{}".format(challenge_id)
% end

<div class="challenges-container-entry">
  <div class="pure-g">
    <div class="challenge-col-icon pure-u-1-24"><img src="/static/img/lang_{{codelanguage}}_big.png"/></div>
    <div class="challenge-col-title pure-u-12-24">
      <div class="title"><a href="/challenge/{{sid}}{{copy_url}}">{{title}}</a></div>
      <div class="detail">{{translate("browse.by")}} {{user}}, {{time}} {{translate("browse.ago")}}
          % for tag in tags:
            , <a class="underline" href="/search/{{tag}}">{{tag}}</a>
          % end
      </div>
    </div>
    <div class="challenge-col-reward pure-u-6-24">
      % if coins[0]:
        <span class="gold">{{coins[0]}}</span>
      % end
      % if coins[1]:
        <span class="silver">{{coins[1]}}</span>
      % end
      % if coins[2]:
        <span class="bronze">{{coins[2]}}</span>
      % end
    </div>
    <div class="challenge-col-info pure-u-5-24">{{rating}} / 5 {{translate("browse.stars")}}<br/><span class="{{difficulty}}">{{difficulty}}</span><br/>{{language}}</div>
  </div>
</div>
