% from lang import translate
% rebase("baseframe")

<p style="text-align: center">
{{translate("search.resultsfor")}} "{{query}}"
</p>

<h1 class="red-title" style="margin-top: 30px;">{{translate("search.challenges")}}</h1>
% if challenges:
<div class="challenges-container-top"></div>
  %   for challenge in challenges:
  %     include('listedchallenge.tpl', challenge=challenge)
  %   end
<div class="challenges-container-bottom"></div>
% else:
  <p>{{translate("search.nomatch")}}</p>
% end

<h1 class="red-title" style="margin-top: 30px;">{{translate("search.users")}}</h1>
% if users:
<div class="user-container-top"></div>
  %   for user in users:
  %     include('listeduser.tpl', user=user)
  %   end
<div class="challenges-container-bottom"></div>
% else:
  <p>{{translate("search.nomatch")}}</p>
% end
