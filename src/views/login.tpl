% from lang import translate
% rebase("baseframe")

<div style="text-align: center;">
  <h1>{{translate("login.title")}}</h1>
  <h2>{{translate("login.info")}}</h2>
    <form class="pure-form pure-form-stacked login-form" action="/login" method="POST">
        <input type="text"     name="username" placeholder={{translate("login.username")}} class="pure-input-1" style="text-align: center; margin: 0.65em 0;" required autofocus>
        <input type="password" name="password" placeholder={{translate("login.password")}}  class="pure-input-1" style="text-align: center; margin: 0.65em 0 0.2em;" required>
        <button type="submit" class="large-button pure-button pure-input-1 pure-button-primary">{{translate("login.login")}}</button>
    </form>
  %if 'errormsg' in vars():
  <div class="form-error" style="width: 100%; text-align: center;">
    {{errormsg}}
  </div>
  %end
  <p>{{translate("login.signupinfo")}}<a class="underline" href="signup">{{translate("login.signup")}}</a></p>
</div>
